
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef TESTDENDOGRAMDRAWINGFUNCTIONS_H_
#define TESTDENDOGRAMDRAWINGFUNCTIONS_H_ 

#include <dendogram/treeDrawingFunctions.h>
#include <functional>


//for formating the output string
#include <sstream>
#include<iomanip>

namespace test {

    const double hDrawnSpacing = 2;
    const double vDrawnSpacing = 1.5;
    const double gDrawingSpacing = 1.2; //spacing between graphs in same cluster
    const double vSpreadSpacing =0.8; // the amount of space taken by all the branches of the same cluster

    //a function just to consistingly format the data wherever it is printed
    inline std::string formatDistance(double distance){
        std::stringstream ss;
        ss << std::fixed << std::setprecision(2) << distance;
        // ss << std::fixed << distance;
        return ss.str();
    }

    template< class Tree, class Renderer, class DrawLeaf>
    void
    drawDendogram(Tree const& tree, 
                  Renderer& renderer, 
                  DrawLeaf const& drawLeaf, 
                  double spacing){

        using Node = typename Tree::node_type;

        auto elemWidth = spacing*hDrawnSpacing;
        auto elemHeight = spacing*vDrawnSpacing;

        auto height=dendogram::maxDepth(tree);

        // layoutNodeLevel(tree,tree.getRoot(), 0, 0, 
        layoutByLevel(tree, 
                     //Draw leaf.
                     [&](Node const& node, size_t posX, size_t level){

                        auto posXS = level * elemWidth;
                        auto posYS = posX  * elemHeight; //laying on the side...
                        auto posXLeaf = elemWidth*height;

                        renderer.setLineThickness(1.5f);
                        renderer.begin(Renderer::DrawingMode::LINES);
                        renderer.vertex(posXS-elemWidth,posYS,0);
                        renderer.vertex(posXLeaf,posYS,0);
                        renderer.end();
                        
                        //render leaf
                        drawLeaf(tree.getLeaf(node),renderer, posXLeaf,posYS);
                     },
                     //Draw node.
                     [&](Node const& node, double posX, size_t level){

                        auto posXS = level * elemWidth;
                        auto posYS = posX  * elemHeight; //laying on the side...
                        renderer.setLineThickness(1.5f);
                        renderer.begin(Renderer::DrawingMode::LINES);
                        renderer.vertex(posXS,posYS,0);
                        renderer.vertex(posXS - elemWidth,posYS,0);
                        renderer.end();

                        //Draw distance text.
                        std::ostringstream stringStream;
                        // stringStream << tree.getData(node);
                        stringStream <<formatDistance(tree.getData(node));
                        renderer.renderTextInPos(posXS, posYS, 0, stringStream.str(),10,-5);

                     },
                     //Draw branch.
                     [&](double posX,  double posOther, size_t level){

                        auto posXS = level * elemWidth;
                        auto posYS = posX  * elemHeight;
                        auto posYO =  posOther * elemHeight; 
                        renderer.setLineThickness(1.5f);
                        renderer.begin(Renderer::DrawingMode::LINES);
                        renderer.vertex(posXS,posYS,0);
                        renderer.vertex(posXS,posYO,0);
                        renderer.end(); 
                     }
                     );
    } 

    //----------------------------------------------------------------------------
    //Clustered
    //----------------------------------------------------------------------------
    
    template< class Tree, class LeafF, class NodeF,class EdgeF, class PosF>
    class ClusterLayout{
    public:
        using Node = typename Tree::node_type;
        using ClusterTestFunction = std::function<bool(Node,double)>;

        ClusterLayout(Tree const& tree,
                      LeafF const& leafFunction,
                      NodeF const& nodeFunction,
                      EdgeF const& edgeFunction,
                      PosF  const& posFunction):
        tree_{tree},
        leafFunction_{leafFunction},
        nodeFunction_{nodeFunction},
        edgeFunction_{edgeFunction},
        posFunction_{posFunction}{}

        //void traverse(double threshold);
        void traverse(ClusterTestFunction const& isStartOfCluster);

    private:
        double traverse(Node node, double parentDistance);
 
    private:
        Tree const& tree_;
        LeafF const& leafFunction_;
        NodeF const& nodeFunction_;
        EdgeF const& edgeFunction_;
        PosF  const& posFunction_;


        size_t leafPos_;
        double clusterPos_; 
        size_t elemsInCluster_; //only used when iterating in clusters (usefull only in leaves)
        //double threshold_;
        ClusterTestFunction isStartOfCluster_;

    };



    template< class Tree, class LeafF, class NodeF,class EdgeF, class PosF>
    void 
    ClusterLayout<Tree,LeafF,NodeF,EdgeF,PosF>::traverse(ClusterTestFunction const& isStartOfCluster){

        //This copies the type passed, so copy constructor should reset all internal values. Be carefull with lambda 
        //captures of local variables, because they won't be reset if changed in function!!
        isStartOfCluster_ = isStartOfCluster;
        leafPos_ = elemsInCluster_= 0;
        clusterPos_ = 0;
        traverse(tree_.getRoot(),tree_.getData(tree_.getRoot())); //start with max distance
    }


    template< class Tree, class LeafF, class NodeF,class EdgeF,class PosF>
    double
    ClusterLayout<Tree,LeafF,NodeF,EdgeF,PosF>::traverse(Node node, double parentDistance){

        double nodePos = 0;
        double distance = tree_.getData(node);

        //Check if it is the beginning of a cluster and reset accordingly.
        //auto startOfCluster=parentDistance > threshold_ && distance <= threshold_;
        auto startOfCluster= isStartOfCluster_(node, parentDistance);
        if(startOfCluster){
            leafPos_ = 0;
            elemsInCluster_ = dendogram::numOfLeaves(tree_,node,0); 
        }

        if(tree_.isNodeLeaf(node)){
            //nodePos = clusterPos_ + leafPos_/elemsInCluster_;
            nodePos = posFunction_(clusterPos_, leafPos_, elemsInCluster_);
            leafFunction_(node,nodePos, parentDistance, clusterPos_, leafPos_);
            ++leafPos_;

        }
        else{
            //this imposes an arbitrary ordering of left and right
            auto aPos=traverse(tree_.getChildA(node), distance);
            auto bPos=traverse(tree_.getChildB(node), distance);
      
            //just a way of calculating the pos
            nodePos = (aPos + bPos)/2.0;

            //do all drawing for node
            nodeFunction_(node,nodePos,parentDistance); 
            edgeFunction_(nodePos, aPos, distance);
            edgeFunction_(nodePos, bPos, distance);
        }

        //Exiting the beginning of a cluster (moving up the hierarchy)
        if(startOfCluster){
            ++clusterPos_;
        }

        return nodePos;
    }

    template< class Tree, class LeafF, class NodeF,class EdgeF, class PosF>
    ClusterLayout<Tree,LeafF,NodeF,EdgeF,PosF>
    createClusterLayout(Tree const& tree,
                        LeafF const& leafFunction,
                        NodeF const& nodeFunction,
                        EdgeF const& edgeFunction,
                        PosF  const& posFunction
                        ){
        return ClusterLayout<Tree,LeafF,NodeF,EdgeF,PosF>(tree,leafFunction,nodeFunction,edgeFunction,posFunction);
    }


    //All leaves in cluster are drawn together
    template<class Tree, class ClusterTestFunction, class Renderer, class DrawLeaf>
    double
    drawDendogramClusters(Tree const& tree,
                          ClusterTestFunction const& clusterFunction,
                          Renderer& renderer,
                          DrawLeaf const& drawLeaf,
                          double spacing,
                          double distanceScale){

        using Node = typename Tree::node_type;

        auto elemHeight = spacing*vDrawnSpacing;
        auto leafClusterSpacing = spacing * gDrawingSpacing; //the horizontal spacing between leafs of same cluster
        auto clusterDistArea = spacing*vSpreadSpacing; //the area to distribute vertically the branching of the same cluster

        //This is used to distribute the leaves (and thus the rest of the dendogram) in space.
        auto posCalculation = [&elemHeight,&clusterDistArea](
            size_t clusterPos,
            size_t leafPos,
            size_t elemsInCluster)->double{

            auto branchOffset = 0;
            if(elemsInCluster >1){
                branchOffset = (static_cast<double>(leafPos)/(elemsInCluster-1))*clusterDistArea - clusterDistArea/2.0;
            }
            return clusterPos*elemHeight + branchOffset;
        };

        //before drawing,use the cluster layout to calculate the span of the resulting tree, so it is 
        //possible to centre the view.
        double span = 0;

        auto leafPosCalculation =  [&](
            Node const& node, //leafDrawing is the only one that is not empty
            double posYS,
            double parentDistance,
            size_t clusterCt,
            size_t leafCt){
                span = posYS > span? posYS : span;
            };

        auto preLayout = createClusterLayout(
            tree,
            leafPosCalculation,
            [](Node const&,double,double){},    //empty NodeF function
            [](double, double, double){},       //empty EdgeF function
            posCalculation);

        preLayout.traverse(clusterFunction); 

        // auto xOrig = tree.getData(tree.getRoot());
        auto xOrig = 0;
        auto yOrig = -span/2.0;
        // auto yOrig = 0;

        // auto margin = spacing/2; //space between tree drawing and leaves
        auto margin = 0; //space between tree drawing and leaves


        auto leafDrawing = [&](Node const& node, 
                               double posYS,
                               double parentDistance,
                               size_t clusterCt,
                               size_t leafCt){

           posYS += yOrig; 

            auto posXS = (xOrig-parentDistance)*distanceScale;
            auto posXL = margin + xOrig*distanceScale;

            renderer.setLineThickness(1.5f);
            renderer.begin(Renderer::DrawingMode::LINES);
            renderer.vertex(posXS,posYS,0);
            renderer.vertex(posXL,posYS,0);
            renderer.end();
            
            //render leaf
            drawLeaf(tree.getLeaf(node),renderer, posXL + leafCt * leafClusterSpacing , yOrig + clusterCt*elemHeight);
        };

        auto nodeDrawing = [&](Node const& node, 
                               double posYS,
                               double parentDistance){

            posYS += yOrig; 

            auto distance = tree.getData(node);
            auto posXP = (xOrig-parentDistance)*distanceScale;  
            auto posXS = (xOrig-distance)*distanceScale;

            renderer.setLineThickness(1.5f);
            renderer.begin(Renderer::DrawingMode::LINES);
            renderer.vertex(posXS,posYS,0);
            renderer.vertex(posXP,posYS,0);
            renderer.end();

            //Draw distance text.
            std::ostringstream stringStream;
            // stringStream << tree.getData(node);
            stringStream <<formatDistance(tree.getData(node));
            renderer.renderTextInPos(posXS, posYS, 0, stringStream.str(),10,-5);

        };

        auto branchDrawing = [&](double posYS,  
                                 double posYO, 
                                 double distance){

            posYS += yOrig; 
            posYO += yOrig;
            auto posXS = (xOrig-distance)*distanceScale;

            renderer.setLineThickness(1.5f);
            renderer.begin(Renderer::DrawingMode::LINES);
            renderer.vertex(posXS,posYS,0);
            renderer.vertex(posXS,posYO,0);
            renderer.end(); 
        };


        auto layout = createClusterLayout(tree,leafDrawing,nodeDrawing,branchDrawing,posCalculation);
        layout.traverse(clusterFunction);

        return span;
    } 

    template< class Tree, class Renderer, class DrawLeaf>
    double
    drawDendogramClustersByDistance(Tree const& tree,
                                    double threshold,
                                    Renderer& renderer,
                                    DrawLeaf const& drawLeaf,
                                    double spacing,
                                    double distanceScale){

        using Node = typename Tree::node_type;

        auto startOfCluster= [&threshold, &tree](Node const& node, double parentDistance)->bool{
            return parentDistance >= threshold && tree.getData(node) <= threshold;
        };

        return drawDendogramClusters(tree,startOfCluster,renderer,drawLeaf,spacing,distanceScale);
    }


    template< class Tree, class Renderer, class DrawLeaf>
    double
    drawDendogramClustersBySize(Tree const& tree,
                                size_t clusterSize,
                                Renderer& renderer,
                                DrawLeaf const& drawLeaf,
                                double spacing,
                                double distanceScale){

        using Node = typename Tree::node_type;

        //For being able to reset it should be a functor

        struct StartOfCluster{
        private:
            Tree const& tree_;
            bool inCluster_;
            size_t numOfLeafsInCluster_;
            size_t leafsInCluster_;
            size_t clusterSize_;

        public:

            StartOfCluster(Tree const& tree, size_t clusterSize):
            tree_{tree},
            clusterSize_{clusterSize},
            numOfLeafsInCluster_{0},
            inCluster_{false},
            leafsInCluster_{0}{}

            //Copy constructor that resets all internal variables.
            //ClusterLayout::traverse copies the type passed, so it resets
            StartOfCluster(StartOfCluster const& other):
            tree_{other.tree_},
            clusterSize_{other.clusterSize_},
            numOfLeafsInCluster_{0},
            inCluster_{false},
            leafsInCluster_{0}{}


            bool operator()(Node const& node, double parentDistance){

                bool isStart = false;
                if(!inCluster_){
                    auto numOfLeafs = dendogram::numOfLeaves(tree_,node,0); 
                    if(numOfLeafs <= clusterSize_){ //initialise
                        numOfLeafsInCluster_ = numOfLeafs;
                        leafsInCluster_ = 0;
                        inCluster_= true;
                        isStart = true;
                    }
                }
                if (tree_.isNodeLeaf(node)){
                    ++leafsInCluster_;
                    if(leafsInCluster_ >= numOfLeafsInCluster_){ //it has included the last leaf,so reset:
                        numOfLeafsInCluster_ = 0;
                        leafsInCluster_ = 0;
                        inCluster_= false;
                    }   
                }
                return isStart;     
            }
        };



        return drawDendogramClusters(tree,StartOfCluster(tree,clusterSize),renderer,drawLeaf,spacing,distanceScale);

    }

}
#endif