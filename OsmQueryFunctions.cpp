
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#include "OsmQueryFunctions.h"

#include <sstream>

namespace test {



    std::string processQueryString(std::string const& queryString){
        std::istringstream stringStream{queryString};
        return processQueryStream( stringStream);   
    }


    std::string processQueryFile(std::string const& pathToQuery){
        std::ifstream fileStream{pathToQuery};
        if(!fileStream.good()){
            throw(std::runtime_error("Failed opening file:" + pathToQuery));
        }
        auto resultQuery = processQueryStream(fileStream);
        return resultQuery;
    }

    size_t getQueryTypePos(std::string const& query, size_t pos){
    //find first occurence of "way", "node", or "relation" query

        std::string types[]={"way", "node","relation"};
        for(auto const& type:types){
            auto qpos = query.find(type,pos);
            if (qpos  != std::string::npos) { //Query type found
                return qpos;
            }
        }
    //if it reaches here it means it could not find a query 
        return query.length();
    }
    //in case that there many queries in the set with different bounds,
    //this could be modified to go through all queries and return the bbox of all queries

    BoundingCoords getBoundingBoxFromSingleQuery(std::string const& query){
        BoundingCoords boundingBox;
        auto pos=getQueryTypePos(query);

        auto bPos=query.find("(",pos)+1;
        auto ePos=query.find(")",bPos);

        auto cPos = query.find(",",bPos);
    //process all numbers between parenthesis followed by a comma
        size_t ct=0;
        while (cPos < ePos){
            boundingBox.set(ct++,query.substr(bPos,cPos-bPos));
       //std::cout<<"bPos: " << bPos << " cPos: "<< cPos <<std::endl;
            bPos = cPos+1;
            cPos = query.find(",",bPos);
        }
    //process from last comma to closing parenthesis
        boundingBox.set(ct,query.substr(bPos,ePos-bPos));
        return boundingBox;
    }

    BoundingCoords getBoundingBoxFromBoundingBoxQuery(std::string const& query){
        BoundingCoords boundingBox;
        auto bboxWord="bbox:";
        auto bPos=query.find(bboxWord);
        if(bPos==std::string::npos){
            return boundingBox; //return unset
        }

        bPos+=std::strlen(bboxWord);
        auto ePos=query.find("]",bPos);

        auto cPos = query.find(",",bPos);
        //process all numbers between parenthesis followed by a comma
        size_t ct=0;
        while (cPos < ePos){
            boundingBox.set(ct++,query.substr(bPos,cPos-bPos));
        //std::cout<<"bPos: " << bPos << " cPos: "<< cPos <<std::endl;
            bPos = cPos+1;
            cPos = query.find(",",bPos);
        }
        //process from last comma to closing parenthesis
        boundingBox.set(ct,query.substr(bPos,ePos-bPos));
        return boundingBox;
    }

    std::string removeBoundingBoxFromBoundingBoxQuery(std::string const& query){

        auto bboxBeginWord  ="[bbox:";
        auto bboxEndWord    ="];";
        auto bPos=query.find(bboxBeginWord);
        if(bPos==std::string::npos){
            return query;
        }
        auto ePos=query.find(bboxEndWord,bPos) + std::strlen(bboxEndWord);
        return query.substr(0,bPos) + query.substr(ePos,query.size());
    }

    std::string addBoundingBoxToQuery(std::string const& query, double south, double west, double north, double east){
        std::ostringstream bboxos;
        bboxos.precision(4);
        bboxos << "[bbox:";
        bboxos << std::fixed << south << ","; //std::fixed to make sure no conversions to scientific notation
        bboxos << std::fixed << west << ",";
        bboxos << std::fixed << north << ",";
        bboxos << std::fixed << east;
        bboxos << "];";
        auto beginWord ="[out:json]";
        auto bPos=query.find(beginWord) + std::strlen(beginWord);
        return query.substr(0,bPos) + bboxos.str() + query.substr(bPos,query.size());
    }
}
