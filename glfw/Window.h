
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLFWWINDOW_H_
#define GLFWWINDOW_H_ 

//This is a class that it mimics the sb6::application in the OpenGL Superbible,
//but it uses templates instead of inheritance (duck typing) and the GLFW framework

// third-party libraries
#include <GL/glew.h>
#include <GLFW/glfw3.h>

// standard C++ libraries
#include <iostream>
#include <string>
#include <stdexcept>
#include <cmath>
#include <memory>

#include <ui/MouseInfo.h>
#include <ui/KeyInfo.h>

#include <thread> //for sleep
#include <chrono>
#include <vector>//for the callback...


namespace glfw {

	//This is the code for having a one window app.
	
	class Window
	{
	public:

		using MouseFunction 	= std::function<void(ui::MouseInfo)>;
		using KeyFunction 		= std::function<void(ui::KeyInfo)>;
		using ScrollFunction 	= std::function<void(int,int)>;

		using RenderFunction 	= std::function<void(double)>;
		using ResizeFunction 	= std::function<void(GLint,GLint)>;
		using CharFunction 		= std::function<void(char32_t,ui::ModifierType)>;

		using DropFunction 		= std::function<void(std::vector<std::string> const& paths)>;


		Window(int width=400, int height=400, std::string const& name="OpenGL Window", bool animated=false);
		virtual ~Window();
		//Window( Window const&)=delete;
		Window& operator = (Window const&)=delete;

        void setTitle(std::string const& title){glfwSetWindowTitle(windowPtr_, title.c_str());}

		void execute();

		bool isAnimated(){return animated_;}
		void setAnimation(bool val){animated_=val;}

		void forceRedraw(); //if the display need to be updated (not in animation mode)

		void mainLoop();
		//this is necessary to handle Emscripten compilation version
		static void mainLoopCallBack(void* windowPtr);

		//transform from screen coordinates to pixels...
		ui::MouseInfo makeMouseInfo( 
		                            double x, 
		                            double y, 
		                            ui::MouseButtonType button, 
		                            ui::MouseActionType action,
		                            ui::ModifierType   mods);


		void setRenderFunction(RenderFunction renderFunction);

		void setResizeFunction(ResizeFunction resizeFunction);
		static void resize(GLFWwindow* windowPtr,int width, int height);

		static double getCurrentTime();

		void setMouseButtonFunction(MouseFunction mouseButtonFunction);
		static void mouseButtonCallBack(GLFWwindow* windowPtr, int button, int action, int mods);

		void setMouseMotionFunction(MouseFunction mouseMoveFunction);
		static void mouseMoveCallBack(GLFWwindow* windowPtr, double x, double y);

		void setKeyFunction(KeyFunction keyFunction);
		static void keyCallBack(GLFWwindow* windowPtr, int key, int scancode, int action, int mods);

		void setScrollFunction(ScrollFunction scrollFunction);
		static void scrollCallBack(GLFWwindow* windowPtr, double xOffset, double yOffset);

		void setCharFunction(CharFunction charFunction);
		static void charModCallBack(GLFWwindow* windowPtr, unsigned int codepoint,int mods);

		void setDropFunction(DropFunction dropFunction);
		static void dropCallBack(GLFWwindow* window, int count, const char** paths);


		
	private:
		
		GLFWwindow* windowPtr_;
		
		RenderFunction renderFunction_;
		ResizeFunction resizeFunction_;
		MouseFunction mouseButtonFunction_;
		MouseFunction mouseMoveFunction_;
		KeyFunction keyFunction_;
		ScrollFunction scrollFunction_;	
		CharFunction charFunction_;
		DropFunction dropFunction_;

		bool animated_;
		std::mutex redrawMutex_;
        bool forceRedraw_;
	};
}


#endif
