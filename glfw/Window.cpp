
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#include "Window.h"	

#ifdef EMSCRIPTEN
#include<emscripten/emscripten.h>
#include<emscripten/html5.h>
#endif

namespace glfw {

	//if width or height are 0 in emscripten, read from html canvas element instead.
	Window::Window( int width, int height, std::string const& name, bool animated):animated_{animated},forceRedraw_{false}
	{

		// initialise GLFW is done in GlfwApplication
	    // open a window with GLFW //these should eventually be parameters for the constructor,
	    // so if creation fails, we can use other parameters instead.

	    float mult=1; //if not EMSCRIPTEN, width and height will be in screen coordinates,
	    				  //but it will create the correct size (size*pixelratio, in retinas case 2)

#ifdef EMSCRIPTEN
		glfwWindowHint(GLFW_DOUBLEBUFFER, GL_TRUE);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
		//this makes sure that it gets width and height in pixels
		mult=emscripten_get_device_pixel_ratio();
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

#else	
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
#endif
		

		//glfwDisable(GLFW_AUTO_POLL_EVENTS);
	    //glfwWindowHint(GLFW_RESIZABLE, GL_FALSE); 
#ifdef EMSCRIPTEN
		if(width == 0 || height == 0){ //if any is 0, read from html element instead!
			double canvasW, canvasH;
			emscripten_get_element_css_size("#canvas", &canvasW, &canvasH);
			width = canvasW;
			height =canvasH;
		}
#endif	    
		windowPtr_ = glfwCreateWindow(width*mult, height*mult, name.c_str(), NULL, NULL);
		if(!windowPtr_){

#ifdef EMSCRIPTEN
			throw std::runtime_error("glfwCreateWindow failed. Can your hardware handle OpenGL 2.0?");
#else	
			throw std::runtime_error("glfwCreateWindow failed. Can your hardware handle OpenGL 3.2?");
#endif
		}

#ifdef EMSCRIPTEN
		//this makes sure that it gets width and height in pixels
		//it needs to be called after glfwCreateWindow, to correct the set values
		emscripten_set_canvas_element_size("#canvas", width*mult, height*mult);
		emscripten_set_element_css_size("#canvas", width, height);
#endif

	    //to be able to retrieve the object associated with the windowPtr_ in the static callbacks...
		glfwSetWindowUserPointer(windowPtr_,this);

	    //glfwSetWindowSizeCallback(windowPtr_,resize); //default resize
	    glfwSetFramebufferSizeCallback(windowPtr_,resize); //this works correctly with Retina displays

	    // GLFW settings
	    //needs to be set before glewInit() call
	    glfwMakeContextCurrent(windowPtr_);

	    
	    // initialise GLEW
	    glewExperimental = GL_TRUE; //stops glew crashing on OSX :-/
	    if(glewInit() != GLEW_OK)
	    	throw std::runtime_error("glewInit failed");


	    // print out some info about the graphics drivers
	    std::cout << "OpenGL version: " << glGetString(GL_VERSION) << std::endl;
	    std::cout << "GLSL version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
	    std::cout << "Vendor: " << glGetString(GL_VENDOR) << std::endl;
	    std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;

#ifdef EMSCRIPTEN
	     // make sure OpenGL API version is available
	    if(!GLEW_VERSION_2_0){
	    	throw std::runtime_error("OpenGL 2.0 API is not available.");
	    }
#else
		if(!GLEW_VERSION_3_2){	
			throw std::runtime_error("OpenGL 3.2 API is not available.");
		}
#endif
	
	    //Now, after all context initialisation, we create the contextData. 
	    //This way we make sure a rendering context is already created and we can use RAII for
	    //OpenGL objects and resources in the contexData. Since ContextData is associated with this window
	    //All its data will be also destroyed when this context is not valid
	    //the recomendations for OpenGL and RAII: 
	    //https://www.opengl.org/wiki/Common_Mistakes#The_Object_Oriented_Language_Problem
	    //implementing the 3rd alternative suggested
	}

	Window::~Window(){	
		glfwDestroyWindow(windowPtr_);
	};



	//Main loop, animated. Alternatives can be written, or be parametricised
	//by eliminating the render call in the while loop (only pool events).

	void Window::mainLoop(){
	    // process pending events
		glfwPollEvents();

		// redrawMutex_.lock();
        // draw one frame 
		if(animated_){
			renderFunction_(getCurrentTime());
			glfwSwapBuffers(windowPtr_);
		}else if(forceRedraw_){ //it should instead use glfwWaitEvents()
			
			renderFunction_(getCurrentTime());
			glfwSwapBuffers(windowPtr_);
			forceRedraw_=false;
		}
		else{
        	//std::this_thread::sleep_for(10ms);
        	//std::this_thread::sleep_for(2s); //C++14
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
			// std::cout<<"x"<<std::endl;
		}
		// redrawMutex_.unlock();



	}

	//this is necessary to handle Emscripten compilation version
	//it needs to have this signature, and thust the extra casting
	void Window::mainLoopCallBack(void* windowPtr){
		Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(reinterpret_cast<GLFWwindow*>(windowPtr)));
		window.mainLoop();
	}


	//start execution
	void  Window::execute(){ 

		// GLFW settings
		glfwMakeContextCurrent(windowPtr_);
	    //call the resize, to send it the correct size values
		int width, height;
		//this works with retina displays too...
		glfwGetFramebufferSize (windowPtr_, &width, &height);
		if(resizeFunction_ != nullptr){
			resizeFunction_(width,height);
		}

        //after update to macOS 10.14 (Mojave) it was necessary to update glfw to version 3.3.
        // version 3.3 does not seem to render outside the main event loop, so this is the way 
        // of forcing a first redraw.
		forceRedraw_=true;

#ifdef EMSCRIPTEN
		std::cout<<"Emscripten has set the main loop"<<std::endl;
		//The background of the canvas needs to be black so alpha blending can work-
		EM_ASM(
			canv = document.getElementById("canvas");
			canv.style.backgroundColor ='#000';
  		);
		emscripten_set_main_loop_arg(&mainLoopCallBack, windowPtr_, 0, true);
#else	
      // run while the window is open
		std::cout<<"regular forever main loop"<<std::endl;
		while (!glfwWindowShouldClose(windowPtr_)){
			mainLoop();
		}
#endif

	}


	void  Window::forceRedraw(){ 
		// redrawMutex_.lock();
        forceRedraw_=true; //this is thread safe...
        // redrawMutex_.unlock();	
    }

    void Window::resize(GLFWwindow* windowPtr,int width, int height){
    	float mult = 1;
 
#ifdef EMSCRIPTEN    
		mult=emscripten_get_device_pixel_ratio();
     	emscripten_set_canvas_element_size("#canvas", width*mult, height*mult);
		emscripten_set_element_css_size("#canvas", width, height);
		glfwMakeContextCurrent(windowPtr);
		glViewport(0,0,width*mult,height*mult);
#endif
		int fbwidth, fbheight;
		glfwGetFramebufferSize(windowPtr, &fbwidth, &fbheight);
		
    	Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(windowPtr));
    	window.resizeFunction_(width*mult,height*mult);
    	window.renderFunction_(getCurrentTime());
    	glfwSwapBuffers(windowPtr);
    }

    double Window::getCurrentTime(){
    	static auto start = std::chrono::system_clock::now();
    	auto now = std::chrono::system_clock::now();
    	auto timeFromStart=std::chrono::duration_cast<std::chrono::milliseconds>(now-start).count();
    	return timeFromStart/1000.0;
	    //or it coud have simple done:
	    //return glfwGetTime();

    }
    void Window::setRenderFunction(RenderFunction renderFunction){
    	renderFunction_=renderFunction;
    }
    void  Window::setResizeFunction(ResizeFunction resizeFunction){
    	resizeFunction_=resizeFunction;
    }

    void Window::setMouseButtonFunction(MouseFunction mouseButtonFunction){
    	mouseButtonFunction_=mouseButtonFunction;
    	glfwSetMouseButtonCallback(windowPtr_,mouseButtonCallBack); 
    }

    void Window::mouseButtonCallBack(GLFWwindow* windowPtr, int button, int action, int mods){


    	if (button>2){
    		throw std::runtime_error("The mouse button received from glfw is "
    			"out of range for the ui::MouseInfo::ButtonType");
    	}

		//This gets complicated if using viewports (the mouse coordinates of 
		//glfwGetCursorPos will be in relation to a viewport, but apparently
		//not the last one set in the rendering cycle necessarily...
		//glViewport() should thus be avoided.

    	double posx,posy;
		glfwGetCursorPos(windowPtr,&posx,&posy); //these are scrren coordinates, not pixels

		Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(windowPtr));

		window.mouseButtonFunction_(window.makeMouseInfo(
			posx, 
			posy,
			static_cast<ui::MouseButtonType>(button),
			static_cast<ui::MouseActionType>(action),
			static_cast<ui::ModifierType>(mods)
			)
		);

		if(!window.isAnimated()){ //if not animated, refresh after event
			window.renderFunction_(getCurrentTime());
			glfwSwapBuffers(windowPtr);
		}
	}

	void Window::setMouseMotionFunction(MouseFunction mouseMoveFunction){
		mouseMoveFunction_=mouseMoveFunction;
		glfwSetCursorPosCallback(windowPtr_,mouseMoveCallBack);
	}

	void Window::mouseMoveCallBack(GLFWwindow* windowPtr, double x, double y){

		Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(windowPtr));

		window.mouseMoveFunction_(window.makeMouseInfo(
			x,
			y, 
			ui::MouseButtonType::UNKNOWN, 
			ui::MouseActionType::MOVED,
			ui::ModifierType::NONE
			)
		);

		if(!window.isAnimated()){ //if not animated, refresh after event
			window.renderFunction_(getCurrentTime());
			glfwSwapBuffers(windowPtr);
		}
	}

	ui::MouseInfo Window::makeMouseInfo( 
		double x, 
		double y, 
		ui::MouseButtonType button, 
		ui::MouseActionType action,
		ui::ModifierType mods){

		//this way we convert it to correct pixels even with retina displays
	

#ifdef EMSCRIPTEN  
		int pxW, pxH;
		emscripten_get_canvas_element_size("#canvas", &pxW, &pxH);

		return{
			static_cast<int>(x),
			static_cast<int>(pxH-y),
			button,
			action,
			mods
		};
#else 
		//This is probably better done by multiplying by pixel resolution? not tested
		int scW, scH; //width and height in screen coordinates
		int pxW, pxH; //width and height in pixels
		glfwGetWindowSize (windowPtr_, &scW, &scH);	
		glfwGetFramebufferSize (windowPtr_, &pxW, &pxH);
		return{
			static_cast<int>(std::floor((x*pxW)/scW)),
			static_cast<int>(std::floor(((scH - y)*pxH)/scH)),
			button,
			action,
			mods
		};
#endif
		

	}

	void Window::setKeyFunction(KeyFunction keyFunction){
		keyFunction_=keyFunction;
		glfwSetKeyCallback(windowPtr_,keyCallBack);	

	}

	void Window::keyCallBack(GLFWwindow* windowPtr, int key, int scancode, int action, int mods){
		Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(windowPtr));
		window.keyFunction_(ui::KeyInfo{
			static_cast<ui::Key>(key),
			static_cast<ui::KeyActionType>(action),
			static_cast<ui::ModifierType>(mods)
		});

		if(!window.isAnimated()){ //if not animated, refresh after event
			window.renderFunction_(getCurrentTime());
			glfwSwapBuffers(windowPtr); 
		}
	}

	void Window::setScrollFunction(ScrollFunction scrollFunction){
		scrollFunction_=scrollFunction;
		glfwSetScrollCallback(windowPtr_,scrollCallBack);	
	}

	void Window::scrollCallBack(GLFWwindow* windowPtr, double xOffset, double yOffset){
		Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(windowPtr));
		window.scrollFunction_(static_cast<int>(xOffset),static_cast<int>(yOffset) );

		if(!window.isAnimated()){ //if not animated, refresh after event
			window.renderFunction_(getCurrentTime());
			glfwSwapBuffers(windowPtr);
		}
	}

	void Window::setCharFunction(CharFunction charFunction){
		charFunction_=charFunction;
		glfwSetCharModsCallback(windowPtr_,charModCallBack);	
	}
	void Window::charModCallBack(GLFWwindow* windowPtr, unsigned int codepoint, int mods){
		Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(windowPtr));
		//an unsigned int is equivalent to char32_t 
		//http://www.glfw.org/docs/latest/group__input.html#gabf24451c7ceb1952bc02b17a0d5c3e5f
		window.charFunction_(codepoint,static_cast<ui::ModifierType>(mods));

		if(!window.isAnimated()){ //if not animated, refresh after event
			window.renderFunction_(getCurrentTime());
			glfwSwapBuffers(windowPtr); 
		}
	}

	void Window::setDropFunction(DropFunction dropFunction){
		dropFunction_=dropFunction;
		glfwSetDropCallback(windowPtr_, dropCallBack);	
	}

	void Window::dropCallBack(GLFWwindow* windowPtr, int count, const char** paths){
		Window& window = *reinterpret_cast<Window*>(glfwGetWindowUserPointer(windowPtr));
		std::vector<std::string> pathsV;
		for(int i=0;i<count;++i){
			pathsV.push_back(paths[i]);
		}
		window.dropFunction_(pathsV);

		if(!window.isAnimated()){ //if not animated, refresh after event
			window.renderFunction_(getCurrentTime());
			glfwSwapBuffers(windowPtr); 
		}
	}

}
