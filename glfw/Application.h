
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#ifndef glfwGLFWAAPLICATION_H_
#define glfwGLFWAAPLICATION_H_ 
//#include <GLFW/glfw3.h> 

namespace glfw{

	//A simple RAII wrapper for the glfw initialisation (with error callbacks) and cleanup.
	class Application
	{
	public:
		Application();

		virtual ~Application();
		Application( Application const&)=delete;
		Application& operator = (Application const&)=delete;
	
	};
}

#endif