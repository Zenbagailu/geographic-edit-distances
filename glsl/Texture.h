
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLSLTEXTUREOBJECT_H_
#define GLSLTEXTUREOBJECT_H_ 


//GL/glew includes all necessary and correct OpenGL headers. 
//If not using glew, change this for the convinient headers
//OpenGL/gl.h in OS X, or GL/gl.h in other operating systems,
//or gl3.h.
#include <GL/glew.h>



namespace glsl {

	class Texture
	{
	public:
		// //since they will keep the same type after first binding, we define them here
		// enum class Type : GLenum {
		// 	_1D=GL_TEXTURE_1D,
		// 	_2D=GL_TEXTURE_2D,
		// 	_3D=GL_TEXTURE_3D,
		// 	_RECTANGLE=GL_TEXTURE_RECTANGLE,
		// 	_1D_ARRAY=GL_TEXTURE_1D_ARRAY,
		// 	_2D_ARRAY=GL_TEXTURE_2D_ARRAY,
		// 	_CUBE_MAP=GL_TEXTURE_CUBE_MAP,
		// 	_CUBE_MAP_ARRAY=GL_TEXTURE_CUBE_MAP_ARRAY,
		// 	_BUFFER=GL_TEXTURE_BUFFER,
		// 	_2D_MULTISAMPLE=GL_TEXTURE_2D_MULTISAMPLE,
		// 	_2D_MULTISAMPLE_ARRAY=GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
		// 	INVALID
		// };

	protected:
		Texture(){};
		template<class PtrType> static PtrType make();	

	};

	template<class PtrType> 
	PtrType Texture::make(){
		//this way we make sure RAII works. At any time, if  an exception is thrown,
		//index_Ptr will call the deleter (the lamda function)
		PtrType indexPtr{new GLuint(0),[](GLuint* iptr){
			glDeleteTextures(1,iptr);
			delete iptr;
		}}; 

		glGenTextures(1,indexPtr.get());
		//glBindTextures(*indexPtr);
		return indexPtr;
	}


}

#endif