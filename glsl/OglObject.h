
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLSLOBJECT_H_
#define GLSLOBJECT_H_ 


//GL/glew includes all necessary and correct OpenGL headers. 
//If not using glew, change this for the convinient headers
//OpenGL/gl.h in OS X, or GL/gl.h in other operating systems,
//or gl3.h.
#include <GL/glew.h>


#include <memory>

namespace glsl {
	/**
	PtrType may not need to be an argument, since std::shared_ptr seems to be the best solution.
	OglObject creates a smart pointer that also has a factory method specific to the resource,
	and is of the type of the resource. Type safety is the basic reason for inheritance, so 
	RT only defines a static method called "make()"" with the creation policy for a OglOPtrType, which
	will most likely include a deleter with any clean up (calls to glDelete*... etc) This implements 
	thus a RAII paradigm. OglObject uses a constructor with variadic templates that will call
	the correct make static method for the specific RT.
	*/

	/*
	After using it for a while, this is really messy, as the variadic constructor is the one all copy constructors
	or in essence any constructor defaults, and the only real functionality it has is to deal with lists of shaders
	for the Program type. The inheritance of the base type also messes up further expansion of the base class functionality,
	as one would do with traditional OOP and runtime inheritance. An alternative is needed...
	*/
	// expose public interfaces, for type checking enums, etc
	template<class RT, template <class> class PtrType = std::shared_ptr> 
	class OglObject final: public RT 
	{
	public:

		typedef PtrType<GLuint> OglOPtrType;

		template<class... Args>OglObject(Args&&... args){
			oglObjectPtr_=RT::template make<OglOPtrType>(args...);
		}


		//these need to be implemented so it is not deduced through the variadic constructor
		OglObject(OglObject& other){
			oglObjectPtr_=other.oglObjectPtr_;
		}

		OglObject(OglObject const& other){
			oglObjectPtr_=other.oglObjectPtr_;
		}

		OglObject(OglObject&& other)=default;

		OglObject& operator=(OglObject&& other)=default;

		OglObject& operator=(OglObject const& other)=default;


		GLuint getIndex() const{return *oglObjectPtr_; }	

	private:
		OglOPtrType oglObjectPtr_;
	};


}

#endif