
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLSLVERTEXBUFFEROBJECT_H_
#define GLSLVERTEXBUFFEROBJECT_H_ 

#include <cstddef> //for size_t

//GL/glew includes all necessary and correct OpenGL headers. 
//If not using glew, change this for the convinient headers
//OpenGL/gl.h in OS X, or GL/gl.h in other operating systems,
//or gl3.h.
#include <GL/glew.h>


//#include <iostream>
//The T parameter is only for type safety. By making it explicit, 
//we make sure that we cannot mix them, for example by interchanging 
//by mistake a buffer of ints, or floats organised in a specific way. 
namespace glsl {


	template<typename T> class VertexBufferObject
	{
	protected:
		VertexBufferObject(){};
		template<class PtrType> static PtrType make(); 
	};

	template<typename T>
	template<class PtrType> 
	PtrType VertexBufferObject<T>::make(){


		//this way we make sure RAII works. At any time, if  an exception is thrown,
		//index_Ptr will call the deleter (the lamda function). If the object has not been
		//created (*iptr is 0) opengl will silently ignored the delete.
		PtrType indexPtr{new GLuint(0),[](GLuint* iptr){
			glDeleteBuffers(1, iptr); //if it is already deleted it will be silently ignored
			delete iptr;
		}}; 

		glGenBuffers(1, indexPtr.get()); 
		return indexPtr;
	}


}

#endif