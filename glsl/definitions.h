
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLSLDEFINITIONS_H_
#define GLSLDEFINITIONS_H_ 

#include <glsl/OglObject.h>
#include <glsl/Shader.h>
#include <glsl/Program.h>
#include <glsl/Texture.h>
#include <glsl/VertexArrayObject.h>
#include <glsl/VertexBufferObject.h>
#include <glsl/FrameBuffer.h>
#include <glsl/RenderBuffer.h>



namespace glsl {
	
	typedef OglObject<VertexArrayObject>            VAO;
	typedef OglObject<VertexBufferObject<GLfloat>>  VBOFloat;
	typedef OglObject<VertexBufferObject<GLuint>> 	VBOIndices;
	typedef OglObject<Program> 						ProgramO;
	typedef OglObject<Shader>						ShaderO;
	typedef OglObject<Texture> 						TextureO;
	typedef OglObject<FrameBuffer> 					FrameBufferO;
	typedef OglObject<RenderBuffer> 				RenderBufferO;

	//inlined functions, so we can define them in the header, no intention to 
	//optimise anything!
	//these functions just adds throws to the function calls
	//http://stackoverflow.com/questions/1759300/when-should-i-write-the-keyword-inline-for-a-function-method/1759575#1759575

	inline GLint getCurrentProgramIndex(){GLint indx; glGetIntegerv(GL_CURRENT_PROGRAM ,&indx); return indx;}

	GLint getAttributeLocation(glsl::ProgramO const& program, std::string const& attrName);
	GLint getUniformLocation(glsl::ProgramO const& program, std::string const& uniformName);
	GLint getAttributeLocationFromCurrentProgram(std::string const& attrName);
	GLint getUniformLocationFromCurrentProgram(std::string const& uniformName);

}
#endif