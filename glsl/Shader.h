
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLSLSHADER_H_
#define GLSLSHADER_H_ 


//#include <iostream>


//GL/glew includes all necessary and correct OpenGL headers. 
//If not using glew, change this for the convinient headers
//OpenGL/gl.h in OS X, or GL/gl.h in other operating systems,
//or gl3.h.
#include <GL/glew.h>


#include <string>

#include <stdexcept>


namespace glsl {

	class Shader
	{
	public:
		//this is just a trick (any Shader type passed to Program should implement it)
		//to make sure that we don't pass by mistake anything that is not a Shader type
		//implementing the same basic functionality here, to the variadic make method of Program
		//since as otherwise as long as it has an getId() method, it will compile.
		struct isShader : std::true_type { }; 
	protected:
		Shader(){};
		template<class PtrType> static PtrType make(){return PtrType{new GLuint(0)};} //called when type declared (default constructor)
		template<class PtrType> static PtrType make(GLenum type, std::string const& shader);		
	};

	template<class PtrType> 
	PtrType Shader::make(GLenum type, std::string const& shader){

		//this way we make sure RAII works. At any time, if  an exception is thrown,
		//index_Ptr will call the deleter
		PtrType indexPtr{new GLuint(0),[](GLuint* iptr){
			glDeleteShader(*iptr);
			delete iptr;
		}}; 

		GLint result = GL_FALSE;
		int infoLogLength;

		// Compile Shader
		*indexPtr = glCreateShader(type);

		if(*indexPtr == 0){
			throw std::runtime_error("glCreateShader failed");
		}
		char const * sourcePointer = shader.c_str();
		glShaderSource(*indexPtr, 1,&sourcePointer , NULL);
		glCompileShader(*indexPtr);

		// Check Shader
		glGetShaderiv(*indexPtr, GL_COMPILE_STATUS, &result);
		if(result == GL_FALSE){

			glGetShaderiv(*indexPtr, GL_INFO_LOG_LENGTH, &infoLogLength);
			const std::unique_ptr<char[]> strInfoLogPtr(new char[infoLogLength+1]);
			glGetShaderInfoLog(*indexPtr, infoLogLength, NULL, strInfoLogPtr.get());
		 // 	throw std::runtime_error(strInfoLogPtr.get());
		 	
			switch(type){
				// case(GL_COMPUTE_SHADER):
				// throw std::runtime_error(std::string(strInfoLogPtr.get())+"in Compute Shader.");
				// break;
				case(GL_VERTEX_SHADER): 
				throw std::runtime_error(std::string(strInfoLogPtr.get())+"in Vertex Shader.");
				break;
				case(GL_TESS_CONTROL_SHADER):
				throw std::runtime_error(std::string(strInfoLogPtr.get())+"in Tess Control Shader.");
				break; 
				case(GL_TESS_EVALUATION_SHADER):
				throw std::runtime_error(std::string(strInfoLogPtr.get())+"in Tess Evaluation Shader.");
				break;
				case(GL_GEOMETRY_SHADER):
				throw std::runtime_error(std::string(strInfoLogPtr.get())+"in Geometry Shader.");
				break;
				case(GL_FRAGMENT_SHADER):
				throw std::runtime_error(std::string(strInfoLogPtr.get())+"in Fragment Shader."); 
				break;
				default:
				throw std::runtime_error(std::string(strInfoLogPtr.get())+"in unidentified shader."); 
			}
			
		}
		//std::cout<<"shader created with number: "<<*indexPtr<<std::endl;

		return indexPtr;
	}
}
#endif