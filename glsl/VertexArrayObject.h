
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLSLVERTEXARRAYOBJECT_H_
#define GLSLVERTEXARRAYOBJECT_H_ 


//GL/glew includes all necessary and correct OpenGL headers. 
//If not using glew, change this for the convinient headers
//OpenGL/gl.h in OS X, or GL/gl.h in other operating systems,
//or gl3.h.
#include <GL/glew.h>



namespace glsl {

	class VertexArrayObject
	{
	protected:
		VertexArrayObject(){};
		template<class PtrType> static PtrType make();	
	};

	template<class PtrType> 
	PtrType VertexArrayObject::make(){

		//this way we make sure RAII works. At any time, if  an exception is thrown,
		//index_Ptr will call the deleter (the lamda function)
		PtrType indexPtr{new GLuint(0),[](GLuint* iptr){
			glDeleteVertexArrays(1,iptr);
			delete iptr;
		}}; 

		glGenVertexArrays(1,indexPtr.get());
		//glBindVertexArray(*indexPtr); //don't do it by default
		return indexPtr;
	}


}

#endif