
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLSLPROGRAM_H_
#define GLSLPROGRAM_H_ 

#include <memory>
#include <stdexcept>


//GL/glew includes all necessary and correct OpenGL headers. 
//If not using glew, change this for the convinient headers
//OpenGL/gl.h in OS X, or GL/gl.h in other operating systems,
//or gl3.h.
#include <GL/glew.h>


#include <type_traits>
//#include<iostream>

namespace glsl {

	struct Program 
	{

	protected:

		/*
		called when type declared (default constructor). It returns a dummy 
		pointer with a GLuint=0, which will be silently ignored by opengl calls
		(glCreate*, glDelete*...) no specific deleting will be necesary, since no 
		gl objects have been allocated, the default works fine. This is necessary 
		here because some gl objects, such as VAOs, may actually need to use the default
		constructor. 
		*/

		Program(){};

		template<class PtrType> 
		static 
		PtrType make(){return PtrType{new GLuint(0)};} 

	    template<class PtrType, class... Shaders> 
		static PtrType 
		make(Shaders ...args); //The only member of Shader is a smart pointer, so copy is perfectly fine

	private:

		template<class Shader, class... Shaders> 
		static void 
		attachShaders(GLuint pIndex, Shader shader, Shaders... args);

		

		template<class Shader, class... Shaders> 
		static void 
		detachShaders(GLuint pIndex,Shader shader, Shaders... args);

		//These need to be declared inline or defined in a separate
		// .cpp file. Since they are not templated functions
		// they will otherwise produce duplicate symbol linkage errors 
		//(They are plain function members of a non templated class).

		inline static void 
		detachShaders(GLuint);

		inline static void 
		attachShaders(GLuint);
	};


	template<class PtrType, class... Shaders> 
	PtrType
	Program::make(Shaders...args){

		//this way we make sure RAII works. At any time, if  an exception is thrown,
		//index_Ptr will call the deleter

		PtrType indexPtr{new GLuint(0),[](GLuint* iptr){
			glDeleteProgram(*iptr);
			delete iptr;
		}}; 

		*indexPtr= glCreateProgram();
		if(*indexPtr == 0){
			throw std::runtime_error("glCreateProgram failed");
		}

		attachShaders(*indexPtr, args...);
		glLinkProgram(*indexPtr);
		detachShaders(*indexPtr, args...);

		 //throw exception if linking failed
		GLint status;
		glGetProgramiv(*indexPtr, GL_LINK_STATUS, &status);
		if (status == GL_FALSE) {
			GLint infoLogLength;
			glGetProgramiv(*indexPtr, GL_INFO_LOG_LENGTH, &infoLogLength);
			//char strInfoLog[1024];
			//runtime sized array C++14 (and C++11). The problem with this
			//is that it is allocated in the stack, and if big enough (though not likely
			//in this case) it may produce a stack overflow.
			const std::unique_ptr<GLchar[]> strInfoLogPtr(new GLchar[infoLogLength+1]);
			glGetProgramInfoLog(*indexPtr, infoLogLength, NULL, strInfoLogPtr.get());
			throw std::runtime_error(strInfoLogPtr.get()); //copy it...
		}
		return indexPtr;
	}


	//static methods
	template<class Shader, class... Shaders>
	void 
	Program::attachShaders(GLuint pIndex, Shader shader, Shaders... args){
		//std::cout<<"attaching shader: "<<shader.getIndex()<<" to program "<<pIndex <<std::endl;
		typename std::remove_reference<Shader>::type::isShader();
		glAttachShader(pIndex, shader.getIndex());
	 	attachShaders(pIndex,std::forward<Shaders>(args)...); //recursive expansion
	 }

	

	template<class Shader, class... Shaders>
	 void 
	 Program::detachShaders(GLuint pIndex, Shader shader, Shaders... args){
	 	glDetachShader(pIndex, shader.getIndex());
	 	detachShaders(pIndex, std::forward<Shaders>(args)...); //recursive expansion
	 }


	 	//terminal
	//if there were no Shaders in the list, it won't attach any...
	 void Program::attachShaders(GLuint){
	 	//std::cout<<"Finished attaching Shaders" << std::endl;
	 }

	 //terminal
	//if there were no Shaders in the list, it won't detach any...
	 void Program::detachShaders(GLuint){
	 	//std::cout<<"Finished detaching Shaders" << std::endl;
	 }

}

#endif