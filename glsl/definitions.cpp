
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "definitions.h"
#include <stdexcept>

namespace glsl {

	GLint getAttributeLocation(glsl::ProgramO const& program, std::string const& attrName){
		GLint attrLocation=glGetAttribLocation(program.getIndex(),attrName.c_str());
		if(attrLocation<0){
			throw(std::runtime_error("error getting attribute: " + attrName));
		}
		return attrLocation;
	}

	GLint getUniformLocation(glsl::ProgramO const& program, std::string const& uniformName){
		GLint uniformLocation=glGetUniformLocation(program.getIndex(),uniformName.c_str());
		if(uniformLocation<0){
			throw(std::runtime_error("error getting uniform: " + uniformName));
		}
		return uniformLocation;
	}

	GLint getAttributeLocationFromCurrentProgram(std::string const& attrName){
		GLint attrLocation=glGetAttribLocation(getCurrentProgramIndex(),attrName.c_str());
		if(attrLocation<0){
			throw(std::runtime_error("error getting attribute: " + attrName));
		}
		return attrLocation;
	}

	GLint getUniformLocationFromCurrentProgram(std::string const& uniformName){
		GLint uniformLocation=glGetUniformLocation(getCurrentProgramIndex(),uniformName.c_str());
		if(uniformLocation<0){
			throw(std::runtime_error("error getting uniform: " + uniformName));
		}
		return uniformLocation;
	}
}