
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "GraphDendogram.h"

namespace test {

    // This is temporary. Possibly they should be read form file, possibly as an argument in main.
    // The way OsmDrawQuery.h processes it now, it will use the setting if it finds the tagname (independently of
    // their value), and it will take alwats the first in the list. For example, in the way below, if a relation has
    // both the tags building and amenity (independently of their value, "yes", "no", "school"...) it will use the settings
    //for building, so the interpertation of the attributs belopw is incomplete, but it works for more cases
    //this should be expanded and developed. 
    AttributeDrawingSettings createDrawingAttributes(){

        AttributeDrawingSettings drawingSettings;

        drawingSettings.push_back({
            {"relation",{{"building","yes"}}},
            {true,false,0.5,{0.6,0.6,0.6,1.0}} //no text
        });

        drawingSettings.push_back({
            {"relation",{{"amenity","school"}}},
            {true,false,0.5,{0.9,0.9,0.9,1.0}} //no text
        });

        drawingSettings.push_back({
            {"relation",{{"amenity","kindergarten"}}},
            {true,false,0.5,{0.9,0.9,0.9,1.0}} //no text
        });


        drawingSettings.push_back({
           {"way",{{"building","yes"}}},
           {true,false,0.5,{0.6,0.6,0.6,1.0}} //no text
        }); 

        drawingSettings.push_back({
            {"way",{{"amenity","school"}}},
            {true,false,0.5,{0.9,0.9,0.9,1.0}} //no text
        });

        drawingSettings.push_back({
            {"way",{{"amenity","kindergarten"}}},
            {true,false,0.5,{0.9,0.9,0.9,1.0}} //no text
        });

        return drawingSettings;
    }

    //------------------------------------------------------------------------------
    // THIS IS FOR BUILDING GRAPHS AROUND FEATURES
    //------------------------------------------------------------------------------

    template<class OsmSubsets> 
    OsmSubsets
    clusterSubsets(OsmSubsets const& subsets, float clusteringDistance){
        //this can be really optimised if necessary.
        using Subset = typename  OsmSubsets::value_type;
        auto dendogramSubSets = dendogram::create(subsets.begin(), subsets.end());
        dendogramSubSets.calculate(
            // dendogram::DistanceMatrix::SHORTEST,
            dendogram::DistanceMatrix::WEIGHTED_AVERAGE,
            [](Subset const& subA, Subset const& subB){
             //cartesian distance between centroids
                auto const& posA = subA.getCentroid();
                auto const& posB = subB.getCentroid();
                auto dx=posB[0]-posA[0];
                auto dy=posB[1]-posA[1];
                return std::sqrt(dx*dx + dy*dy);
            }
        );

        std::vector<Subset> clusteredSubsets;

        dendogramSubSets.getClusters(clusteringDistance, [&clusteredSubsets](Subset leaf, size_t cluster){
            //std::cout<<"cluster: " << cluster;
            if(clusteredSubsets.empty() || clusteredSubsets.size() <= cluster){
                clusteredSubsets.push_back(leaf);
            }
            else{
                clusteredSubsets.back() = unionSubset(clusteredSubsets.back(), leaf);
            }
        });

        //calcuate their new centroids:  
        for(auto& subset: clusteredSubsets){
            subset.calculateCentroid();
        }
        return clusteredSubsets;
    }


    //gives current number of leaves, depending of current state of claculation (in multithreaded form)
    size_t GraphDendogram::numberOfLeaves(){
        size_t s;
        matrixMutex_.lock(); 
        s= distanceMatrix_.size(); 
        matrixMutex_.unlock();
        return s;
    }  

     double GraphDendogram::getMaxDistance(){
        dendogramMutex_.lock();
        double result = dendogram_.ready() ? dendogram_.getData(dendogram_.getRoot()): 0;
        dendogramMutex_.unlock();
        return result;
    }



    void GraphDendogram::setCentreFromQuery(std::string const& entityQuery){

         //we use the bounding box fromt the entity query
        auto bbox = test::getBoundingBoxFromBoundingBoxQuery(entityQuery);
        if(!bbox.isSet()){
            bbox = test::getBoundingBoxFromSingleQuery(entityQuery);
        }
        if(!bbox.isSet()){
            throw(std::runtime_error("Could not find bounding box"));
        }

        // std::cout<< bbox <<std::endl;
        //now we need to get the center of the bounding box...
        centreLon_   =(bbox[BoundingCoords::EASTLONG] + bbox[BoundingCoords::WESTLONG] )/2.0;
        referenceLat_=(bbox[BoundingCoords::SOUTHLAT] + bbox[BoundingCoords::NORTHLAT] )/2.0;

    }

    //This generates subgraphs according tro clustering, but without filtering them depending of validity
    void GraphDendogram::generateSubgraphs(std::string const& entityQuery,double clusteringRadius){
         //get the data
        entityDataPtr_= std::make_shared<test::OsmDataHolder>(entityQuery, referenceLat_, centreLon_);
        //calculate subsets of entities in data, if they cluster
        subsets_ = test::makeSubsetsFromDatataHolder(entityDataPtr_);
        subsets_ = clusterSubsets(subsets_, clusteringRadius);
    }

     void GraphDendogram::prepocessSubgraphs(
        std::string const& entityQuery, 
        double clusteringRadius){

        setCentreFromQuery(entityQuery);
        generateSubgraphs(entityQuery,clusteringRadius);
     }


    void GraphDendogram::addRowToDistanceMatrix(){

        using vertexIt      = typename boost::graph_traits<SPGraph>::vertex_iterator;
        using IteratorPair  = std::pair<vertexIt,vertexIt>;

        using VMapContainer = editdistance::VertexMapContainer<SVertexIntMap, IteratorPair, size_t>;

        //With map.
        editdistance::NodeMapCost costCalculator;

        //we don't need a mutex because it will not be modified from other threads, 
        auto newRowIndex = distanceMatrix_.size(); 

        std::vector<typename DistanceMatrix::IntType> costRow(newRowIndex);

        for( int j = 0; j < newRowIndex; ++j){

            auto matrix  = editdistance::generateMatrix(
                                                        VMapContainer{
                                                            streetNetworks_[newRowIndex].vertexValues,
                                                            boost::vertices(streetNetworks_[newRowIndex].pathGraph),
                                                            boost::num_vertices(streetNetworks_[newRowIndex].pathGraph)
                                                        },
                                                        VMapContainer{
                                                            streetNetworks_[j].vertexValues,
                                                            boost::vertices(streetNetworks_[j].pathGraph),
                                                            boost::num_vertices(streetNetworks_[j].pathGraph)
                                                        },
                                                        costCalculator);

            auto reduced = kuhnmunkres::reducedColumnsCopy(matrix); //The heuristic makes a considerable difference in performance.
            auto result  = kuhnmunkres::calculateKuhnMunkres(reduced);
            auto cost    = kuhnmunkres::assignmentCost(matrix,result);
            
            // auto cost = dist(rng);   //random generate to test:
            costRow[j]=cost;
        }

        //modify matrix (do it separatelly so the mutex is not locked during distance calculations,
        //and the matrix size etc is updated only when done).
        matrixMutex_.lock();
        distanceMatrix_.expand();
        for( int j = 0; j < newRowIndex; ++j){
            distanceMatrix_.set(newRowIndex,j, costRow[j]);
        }
        matrixMutex_.unlock();

    }

    void GraphDendogram::calculateDendogram(){
         //This creates a dendogram of the calculated distances so far.

        //we don't need a mutex because it will not be modified from other threads.
        //Local variables are created in the thread local stack:
        //https://stackoverflow.com/questions/6324994/c-multithread-safe-local-variables

        //Since they are not modifying any of the other variables (streetNetworks_) and distanceMatrix_
        //is accessed through a mutex, it should be safe to call from two differetn threads. streetNetworks_.size()
        //should be an atomic read, so it should also be thread safe.

        std::vector<int> indices(streetNetworks_.size());
        std::iota (std::begin(indices), std::end(indices), 0); // Fill with 0, 1, ..., streetNetworks_.size().

        //Methods for the DistanceMatrix are:
        // AVERAGE,WEIGHTED_AVERAGE, SHORTEST, LONGEST 
        auto dendogram = dendogram::create(std::begin(indices), std::end(indices));
        //dendogram.calculate(dendogram::DistanceMatrix::WEIGHTED_AVERAGE,
        dendogram.calculate(currentClusteringMethod_,
                            [this](int gIndA, int gIndB){
                                //distance between graphs indexed by gIndA and gIndB
                                DistanceMatrix::IntType distance;
                                matrixMutex_.lock();
                                distance = distanceMatrix_.get(gIndA,gIndB);  
                                matrixMutex_.unlock();  
                                return distance;   
                            });

        dendogramMutex_.lock();
        dendogram_=std::move(dendogram);
        dendogramMutex_.unlock();
    }
}