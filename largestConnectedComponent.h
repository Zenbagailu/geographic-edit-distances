
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef TESTLARGESTCONNECTEDCOMPONENT_
#define TESTLARGESTCONNECTEDCOMPONENT_ 
namespace test {
    //this is a function that takes a graph and returns the largest connected component, as a new graph
    template< class Graph> 
    Graph 
    createLargestComponentGraph(Graph& graph){ //boost::copy_graph cannot handle const&, it needs to be an l-value 

        using VertexId = typename boost::graph_traits<Graph>::vertex_descriptor;
        std::map<VertexId, size_t> componentMap;
        boost::associative_property_map<std::map<VertexId, size_t>> cPropertyMap(componentMap);
        auto numOfComponents = boost::connected_components(graph, cPropertyMap);   

        if(numOfComponents <= 1){
            return graph;
        } 

        //find out which one is largest.
        std::map<size_t,size_t> vertexCountMap;
        for (auto const& elem: componentMap){
            vertexCountMap[elem.second]++; //elem.second is the integer "name" given to the component
        }

        size_t largestComponent;
        size_t largestComponentSize=0;
        for (auto const& componentSize: vertexCountMap){
            if(componentSize.second>largestComponentSize){
                largestComponentSize=componentSize.second;
                largestComponent=componentSize.first;
            }
        }
        
        struct OnlyLargest{

            OnlyLargest(){} //needs default constructor
            OnlyLargest(
                        size_t largestComponent, 
                        std::map<VertexId, size_t>& componentMap
                        ):
            largestComponent_{largestComponent},
            componentMapPtr_{&componentMap}{}

            bool operator()(VertexId vId)const{
                auto elemIt = componentMapPtr_->find(vId);
                if (elemIt != componentMapPtr_->end()){
                    return elemIt->second == largestComponent_;
                }
                else{
                    throw (std::runtime_error("Trying to access non existent vertex in map, in FilterIntEqual."));
                }
            }
        private:
            size_t largestComponent_;
            std::map<VertexId, size_t> *componentMapPtr_;
        
        };
        
        Graph largestConnected;
        auto filtered = boost::make_filtered_graph(graph, boost::keep_all(), OnlyLargest(largestComponent,componentMap));
        //we copy the graph, using the filtered graph  directly is too slow
        boost::copy_graph(filtered,largestConnected);
        return largestConnected;
    }
}
#endif