
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <array>
#include <exception>
#include <memory>
#include <cmath>
#include <numeric> //for std::iota 

#include <emscripten.h>

//rendering
#include <glfw/Application.h>
#include <glfw/Window.h>

#include <views/Ortho.h>
#include <views/Screen.h>

//Interaction
#include <ui/MouseInfo.h>
#include <ui/KeyInfo.h>
#include <ui/Mouse2DNavigation.h>
#include <ui/KeyInfo.h>

#include <glaux/functions.h>
#include <glaux/GL2Renderer.h>

#include <glfonts/FontAtlas.h>
#include <glfonts/FreeTypeFontLibrary.h>
#include <cstdlib>

#include <aux/utils.h>

#include "GraphDendogram.h"
#include <dendogram/DistanceMatrix.h>

#include "OsmQueryFunctions.h"

#include "gui.h"


glfonts::Face makeFace(){
	glfonts::FreeTypeFontLibrary flib;
    //the font can be downloaded from: https://fonts.google.com/specimen/IBM+Plex+Mono?preview.text_type=custom
    return flib.makeFaceFromFile("fonts/IBM_Plex_Mono/IBMPlexMono-Regular.ttf");
}

glfonts::FontProgram makeFontProgram(){

	glsl::ProgramO fontProgramO = {
		glsl::ShaderO{GL_VERTEX_SHADER, aux::getStringFromFile("shaders/TextVS.glsl")},
		glsl::ShaderO{GL_FRAGMENT_SHADER,aux::getStringFromFile("shaders/TextFS.glsl")}
	};

	return glfonts::FontProgram{fontProgramO};
}

int main(int argc, char *argv[]) {
    
	try {

		glfw::Application App;
		//multisampling enabled. It needs to be set before the window is created
		//a posibility would be to have it as an option for the constructor

		glfwWindowHint(GLFW_SAMPLES, 4); 

		typedef glfw::Window    Window;  
		//Window window(800,800,"Street Network Clustering"); //not animated

        Window window(0,0,"Street Network Clustering"); //full canvas size
        // Window window(0,0,"Street Network Clustering",true); //full canvas size, animated for testing for concurrency errors

		//now there is a context, so all initialisation 
		//can be done in the constructors.
		views::Ortho view;

        //This needs to be automatised,,,
        view.setZoom(0.15);
        // view.setCentre(8440, 1480,0);

        views::Screen screenView;

		window.setResizeFunction([&view,&screenView](int w, int h){
			view.resize(w,h);  
            screenView.resize(w,h);
		});

		glaux::GL2Renderer renderer(GL_DYNAMIC_DRAW);
		//we use two renderers, so we can turn on and off the text
		glaux::GL2Renderer textRenderer(GL_DYNAMIC_DRAW);

        glaux::GL2Renderer guiRenderer(GL_DYNAMIC_DRAW);

        glaux::GL2Renderer overlayRenderer(GL_DYNAMIC_DRAW);

		auto fontProgram=makeFontProgram();
        fontProgram.setColor({0.0f,0.0f,0.0f,1.0f});
        fontProgram.setDepth(0.0f);

		auto face=makeFace();

        int faceSize10=10;
        face.setSize(faceSize10);
        glfonts::FontAtlas fontAtlas10{fontProgram, face};

		int faceSize12=12;
		face.setSize(faceSize12);
		glfonts::FontAtlas fontAtlas12{fontProgram, face};
        
        face.setSize(22); //no need to refer to size variable anywhere else
        glfonts::FontAtlas fontAtlas22{fontProgram, face};
	   

        //=================================================================================================
        // GUI CREATION AND INITIALISATION
        //=================================================================================================

        gui::UniformGridLayout grid{4,4,{10,10,900,150},10,10,3};

        //GUI is using font of size 22:
        auto guiFontAtlas = fontAtlas22;
        auto fontSize=22;
        face.setSize(fontSize); //now get some font metrics
        auto ascender = face.getAscender();
        auto descender = face.getDescender();
        auto fontHeight = face.getHeight();
        double leftOff=20;

        std::vector<gui::Separator> separators;
        separators.push_back(grid.createHorizontalSeparator(0));
        separators.push_back(grid.createHorizontalSeparator(1));
        separators.push_back(grid.createHorizontalSeparator(2));
        separators.push_back(grid.createHorizontalSeparator(3));
        separators.push_back(grid.createHorizontalSeparator(4));

        //BOTTOM LINE
        auto textL=guiFontAtlas.calculatePixelWidth("Hide text.");
        auto loc=grid.layout(0,0,1,1);  
        gui::Button buttonText{loc,leftOff, (loc.height-fontSize)/2 - descender, "Hide text.","Show text.", false};

        textL = guiFontAtlas.calculatePixelWidth("Resume loading.");
        loc=grid.layout(1,0,1,1);
        gui::Button buttonLoading{loc,leftOff, (loc.height-fontSize)/2 - descender, "Pause loading.","Resume loading.", true};

        textL=guiFontAtlas.calculatePixelWidth("All      graphs processed.");
        // auto chartL =  fontSize + textL + 20;
        loc=grid.layout(2,0,2,1);
        double chartSize = loc.height;
        // gui::CircularChart progressChart(loc.posx + loc.width - chartL, loc.posy+(loc.height-fontSize)/2 ,fontSize);
        gui::CircularChart progressChart(loc.posx, loc.posy+(loc.height-chartSize)/2 ,chartSize);
        gui::Label labelProgress{loc.posx + chartSize + 20, loc.posy+(loc.height-fontSize)/2 - descender, "No graph processed yet."}; 
        
        //ANOTHER LINE
        double sliderSeparator = fontSize;
        loc = grid.layout(0,1,4,1);
        loc.width -= guiFontAtlas.calculatePixelWidth("100.00") + sliderSeparator; //adjust ofr text
        gui::Slider<int> sliderSize{1, 1, 1,loc,(loc.height-fontSize)/2 - descender,true, sliderSeparator};
        gui::Slider<double> sliderDist{50, 0, 100, loc ,(loc.height-fontSize)/2 - descender,!sliderSize.isActive()};
       
        //ANOTHER LINE

        loc = grid.layout(0,2,2,1);
        textL = guiFontAtlas.calculatePixelWidth("Cluster by edit distance.");
        gui::Button buttonClusterType{
            loc,
            leftOff,(loc.height-fontSize)/2 - descender,
            "Cluster by maximum size.",
            "Cluster by edit distance.", 
            true};

        using ClusterMethod = dendogram::DistanceMatrix::Method;
        using MultiStateButton = gui::MultiStateButton<dendogram::DistanceMatrix::Method>;
        using MethodPair = std::pair<ClusterMethod, std::string>;

        ClusterMethod clusteringMethod = dendogram::DistanceMatrix::WEIGHTED_AVERAGE;

        std::vector<MethodPair>states={
            // {dendogram::DistanceMatrix::AVERAGE,"average cluster distance "},
            {ClusterMethod::WEIGHTED_AVERAGE,"Average distance clustering."},
            {ClusterMethod::SHORTEST,"Shortest distance clustering."},
            {ClusterMethod::LONGEST, "Longest distance clustering."}
        };

        double maxPixels=0;
        for( auto const& pair: states){
            auto pixels = guiFontAtlas.calculatePixelWidth(pair.second);
            maxPixels = pixels> maxPixels? pixels : maxPixels;
        }

        loc = grid.layout(2,2,2,1);
        MultiStateButton clusterMethodButton{loc, leftOff, (loc.height-fontSize)/2 - descender, states,clusteringMethod};

       
        //TOP LINE:
        loc = grid.layout(0,3,4,1);
        gui::Label labelTitle{loc.posx+20, loc.posy+(loc.height-fontSize)/2 - descender, ""};
       
    
        //=================================================================================================

        test::GraphDendogram graphDendogram{buttonLoading.getState()};
        // double dendogramScale=0.00025;
        double dendogramScale=50; //when getData() returns a normalised value this works.


        bool currentExists = false;
        size_t currentSubgraph; 
        size_t initialNumSubgraphs; //used for display in label.

        //for distance
        ////Check if it has arguments. Since It will always have the name of the executable as argv[0],
        //(following the standard convention of a call and unless explicitly overrunned, for example 
        //usng the exec() command in Unix and specifying an argv[0]) in it will need to start checking 
        //if it is larger than 1 
        
        std::thread calculationThread;
        std::mutex recMutex;
        bool recalculateDrawing =false;


        if(argc == 7){ 

            std::string title{argv[1]};
            labelTitle.setText(title);

            std::string entityQueryInputString{argv[2]};
            // std::cout<<entityQueryInputString<<std::endl;
            if(entityQueryInputString.empty()){
                throw(std::runtime_error("No Overpass query for entities."));
            }
            auto entityQuery = test::processQueryString(entityQueryInputString);
            //auto entityQuery = test::processQueryFile(entityQueryInputString);
            std::cout<<"Entitites query: "<<entityQuery<<std::endl; 

             std::string graphQueryInputString{argv[3]};
            // std::cout<<graphQueryInputString<<std::endl;
            if(graphQueryInputString.empty()){
                throw(std::runtime_error("No Overpass query for the graph."));
            }
            auto graphQuery = test::processQueryString(graphQueryInputString);
            // auto graphQuery = test::processQueryFile(graphQueryInputString);
            std::cout<<"Graph query: "<<graphQuery<<std::endl; 

            auto clusteringThresehold = std::atoi(argv[4]);
            auto radius = std::atoi(argv[5]);
            auto limitAngle = std::atoi(argv[6]);


            //Dynamic loading
            // graphDendogram.generateSubgraphs(entityQuery,clusteringThresehold);
            graphDendogram.prepocessSubgraphs(entityQuery,clusteringThresehold);
            initialNumSubgraphs = graphDendogram.numberOfSubsets(); //including 0



             calculationThread = std::thread(
                &test::GraphDendogram::loadAndCalculate<std::function<void(size_t)>>, 
                &graphDendogram, 
                graphQuery,
                radius,
                limitAngle,
                [&](size_t current){
                recMutex.lock();
                recalculateDrawing = true;
                recMutex.unlock(); 
                currentSubgraph = current;
                currentExists = true;
                window.forceRedraw();
                // std::cout<<"forcing redraw"<<std::endl;
            });

        }else{
            throw(std::runtime_error("Wrong number of arguments passed to program (two path to the queries for entitites and the graph, respectively)."));
        }

        renderer.set(); //it needs to be called before draw, in this case static
        textRenderer.set();    


        glEnable(GL_DEPTH_TEST); //it needs it for the first frame it draws

		window.setRenderFunction([&](double currentTime){
                                     //OPENGL 2 alternative
                                    //glClearColor(1.0f,1.0f,1.0f,1.0f);
                                    glClearColor(0.98f,0.98f,0.98f,1.0f);
                                    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);


                                    overlayRenderer.clear(); 

                                    if(recalculateDrawing){

                                        recMutex.lock();
                                        // recalculateDrawing = false;
                                        recMutex.unlock();
                                        // std::cout<<"redrawing"<<std::endl;

                                        renderer.clear(); //it needs to be called before draw, in this case static
                                        textRenderer.clear(); 

                                        renderer.setFont(fontAtlas10); 
                                        textRenderer.setFont(fontAtlas12);
                                       

                                        double yOff=50;

                                        if(sliderSize.isActive()){
                                            renderer.setColor(0.0, 0.0, 0.0);
                                            auto span =  graphDendogram.renderSizedClusters(renderer,textRenderer, faceSize12, faceSize10, sliderSize.getVal() ,dendogramScale);
                                            if(sliderSize.isSelected() && currentSubgraph >= 2){
                                              
                                                auto gSize = graphDendogram.getGraphBoundarySize();
                                                auto valx = sliderSize.getVal()*gSize*test::gDrawingSpacing*2; //*2 becase gSize is radius
                                                auto valy = -span*0.5-gSize*test::vDrawnSpacing - yOff; 
                                                auto topy = span*0.5;
                                                // renderer.setColor(0.0, 0.0, 0.0);
                                                renderer.begin(glaux::Mode::LINES);

                                                //renderer.setColor(0.5, 0.5, 0.5);
                                                renderer.setColor(0.0, 0.0, 0.0);
                                                renderer.setLineThickness(0.25);
                                                // renderer.begin(glaux::Mode::LINES);
                                                renderer.vertex(valx, valy );
                                                renderer.vertex(valx, topy);
                                                renderer.end();

                                                std::ostringstream ss;
                                                ss << sliderSize.getVal();
                                                renderer.setFont(fontAtlas12); 
                                                renderer.renderTextInPos(valx, valy, 0, ss.str(),0,-12);

                                                //and now draw graphics on overlay:
                                                auto sc=renderer.unproject(valx ,valy);

                                                overlayRenderer.setColor(0, 0, 0);

                                                overlayRenderer.setLineThickness(0.75);
                                                overlayRenderer.begin(glaux::Mode::LINES);

                                                overlayRenderer.vertex(sc);
                                                overlayRenderer.vertex(sc[0]+10, sc[1],sc[2]);

                                                overlayRenderer.end();

                                            }


                                        }
                                        else{
                                            renderer.setColor(0.0, 0.0, 0.0);
                                            auto span = graphDendogram.renderDistanceClusters(renderer,textRenderer, faceSize12, faceSize10, sliderDist.getVal(),dendogramScale);

                                            if(sliderDist.isSelected() && currentSubgraph >= 2){
                                                auto valx = sliderDist.getVal()*dendogramScale;
                                                auto gSize = graphDendogram.getGraphBoundarySize();
                                                auto valy = -span*0.5-gSize*test::vDrawnSpacing-yOff; 
                                                auto topy = span*0.5;
                                                // renderer.setColor(0.0, 0.0, 0.0);
                                                renderer.begin(glaux::Mode::LINES);

                                                //renderer.setColor(0.5, 0.5, 0.5);
                                                renderer.setColor(0.0, 0.0, 0.0);
                                                renderer.setLineThickness(0.25);
                                                // renderer.begin(glaux::Mode::LINES);
                                                renderer.vertex(-valx, valy );
                                                renderer.vertex(-valx, topy);
                                                renderer.end();

                                                double offset=30;

                                                std::ostringstream ss;
                                                ss << sliderDist.getVal();
                                                renderer.setFont(fontAtlas12); 
                                                renderer.renderTextInPos(-valx, valy, 0, ss.str(),-offset,-12);

                                                //and now draw graphics on overlay:
                                                auto sc=renderer.unproject(-valx ,valy);

                                                overlayRenderer.setColor(0, 0, 0);
                                                overlayRenderer.setLineThickness(0.75);
                                                overlayRenderer.begin(glaux::Mode::LINES);

                                                overlayRenderer.vertex(sc);
                                                overlayRenderer.vertex(sc[0]-offset, sc[1],sc[2]);

                                                overlayRenderer.end();
                                            }
                                           
                                        }

                                       
                                       
                                        renderer.set(); //it needs to be called before draw, in this case static
                                        textRenderer.set(); 

                                        //recalculate slider. This will happen when updating the tree, or by a change
                                        //of the calculation mode. In this last case the value must have been reset
                                        //for cluster size
                                        if(sliderSize.isActive()){
                                            auto numOfLeaves = static_cast<int>(graphDendogram.numberOfLeaves());
                                            sliderSize.setRange(1,numOfLeaves<=1 ? 1: numOfLeaves-1);
                                        }
                                        else{
                                               //for distance size:
                                            auto maxDis =  graphDendogram.getMaxDistance();
                                            // std::cout<<"max dist: "<<maxDis<<std::endl;
                                            // std::cout<<"val: "<<sliderDist.getVal()<<std::endl;
                                            if(maxDis>0){ //if there is any dendogram:
                                                sliderDist.setRange(maxDis,0);
                                            }
                                            
                                        }

                                        std::ostringstream sStream;
                                        if(currentSubgraph+1 < initialNumSubgraphs){
                                            sStream << currentSubgraph + 1 <<" of " << initialNumSubgraphs <<" graphs processed.";
                                        }
                                        else{
                                            sStream << "All " << initialNumSubgraphs <<" graphs processed.";
                                        }
                                        labelProgress.setText(sStream.str());

                                        // std::cout<<"X"<<std::endl;
                                    }


                                    overlayRenderer.setFont(fontAtlas22);
                                    overlayRenderer.set();


                                    guiRenderer.clear();
                                    guiRenderer.setFont(guiFontAtlas);

                                    sliderSize.render(guiRenderer); 
                                    sliderDist.render(guiRenderer); 
                                    labelProgress.render(guiRenderer);
                                    progressChart.render(guiRenderer,static_cast<double>(currentSubgraph+1)/initialNumSubgraphs);
                                    buttonClusterType.render(guiRenderer);
                                    buttonText.render(guiRenderer);
                                    labelTitle.render(guiRenderer);
                                    //Only show if loading:
                                    if(currentSubgraph < initialNumSubgraphs-1){
                                        buttonLoading.render(guiRenderer);
                                    }

                                    clusterMethodButton.render(guiRenderer);

                                    for(auto& separator: separators){
                                        separator.render(guiRenderer);
                                    }
                                    

                                    guiRenderer.set();

                                    //it should be integrated in the event model of the renderers-
                                    fontProgram.setDepth(0.5f); //move texts a bit to the back.

                                    if(buttonText.getState()){
                                        textRenderer.draw(view);
                                    }

                                    renderer.draw(view);

                                    fontProgram.setDepth(0.0f); //set a bit over
                                    guiRenderer.draw(screenView);
                                    overlayRenderer.draw(screenView);     
                                });

		//ADD NAVIGATION
		auto nav=ui::create2DNavigation(view); 

        window.setMouseButtonFunction([&](ui::MouseInfo const& info){
            bool eventUsed=false;

            if(info.action == ui::MouseActionType::PRESSED){   
                if(sliderSize.isActive() && sliderSize.processSelection(info.x,info.y)){
                    sliderSize.setSelected(true);
                    eventUsed=true;

                }
                else if(sliderDist.isActive()  &&sliderDist.processSelection(info.x,info.y)){
                    sliderDist.setSelected(true);
                    eventUsed=true;
                }
                else if(buttonClusterType.processClick(info.x,info.y)){
                    sliderSize.setActive(buttonClusterType.getState());
                    sliderDist.setActive(!buttonClusterType.getState());

                    recMutex.lock();
                    recalculateDrawing = true;
                    recMutex.unlock();    
                    eventUsed=true;
                }
                else if(buttonText.processClick(info.x,info.y)){
                    eventUsed=true;
                }else if(clusterMethodButton.processClick(info.x,info.y)){
                    // clusteringMethod = clusterMethodButton.getState();
                    graphDendogram.setMethod(clusterMethodButton.getState());
                    graphDendogram.calculateDendogram(); //it is thread safe

                    recMutex.lock();
                    recalculateDrawing = true;
                    recMutex.unlock();    
                    eventUsed=true;
                }
                //Check if it is finished.
                else if(currentSubgraph < initialNumSubgraphs && buttonLoading.processClick(info.x,info.y)){
                    graphDendogram.setLoading(buttonLoading.getState());
                    eventUsed=true;
                }

            }
            if(info.action == ui::MouseActionType::RELEASED){
                if (sliderSize.isActive() && sliderSize.isSelected()){
                    sliderSize.setSelected(false);
                    //this is needed to take care of any redrawing of recalculation 
                    //(removing graphics only drawn while dragging, for example)
                    recMutex.lock();
                    recalculateDrawing = true;
                    recMutex.unlock(); 
                    eventUsed=true;
                }
                else if(sliderDist.isActive() && sliderDist.isSelected()){
                    sliderDist.setSelected(false);
                     //this is needed to take care of any redrawing of recalculation 
                    //(removing graphics only drawn while dragging, for example)
                    eventUsed=true;
                    recMutex.lock();
                    recalculateDrawing = true;
                    recMutex.unlock(); 
                }
            }

            if(!eventUsed){
                nav.mouseButtonFunction(info);
            } 
        });

        window.setMouseMotionFunction([&](ui::MouseInfo const& info){

            if(sliderSize.isActive() && sliderSize.isSelected()){
                sliderSize.processMove(info.x,info.y);
                recMutex.lock();
                recalculateDrawing = true;
                recMutex.unlock();                 
            }
            else if(sliderDist.isActive() && sliderDist.isSelected()){
                sliderDist.processMove(info.x,info.y);
                recMutex.lock();
                recalculateDrawing = true;
                recMutex.unlock(); 
            }
            else{
                nav.mouseMotionFunction(info);
            }    
        });

        window.setScrollFunction([&nav](int xOffset, int yOffset){
            nav.scrollFunction(xOffset,yOffset);
        });

		window.setKeyFunction([&buttonText]
		                      (ui::KeyInfo info){
                                if(info.action==ui::KeyActionType::PRESSED){
		                      		switch(info.key){
		                      			case ui::Key::KEY_T:
                                        buttonText.set(!buttonText.getState());
		                      			break;

		                      			default:
		                      			std::cout<<"not defined key"<<std::endl;       
		                      		}	
		                      	}
                              });

		window.execute();

        //wait to end execution of calculation, in case it is not done
        if(calculationThread.joinable()){ //make sure it is finishe
            calculationThread.join();
        }


	} catch (const std::exception& e){
		std::cerr<<"Exception cought in main thread: \n" << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
