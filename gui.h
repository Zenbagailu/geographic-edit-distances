
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#include <string>
#include <sstream>
#include <cmath>
#include <array>
namespace gui{

	constexpr float PI = 3.1415927;
	const float depth = -0.01; //this is in clipping coordinates, so max is -1.0

	struct Dimensions{
		double posx;
		double posy;
		double width;
		double height;
	};

	//---------------------------------------------------------------------
	//LABEL 
	//---------------------------------------------------------------------
	//Basic text

	class Label
	{
	public:

		Label(
			double posx,
			double posy,
			std::string const& text =""
			):
		posx_{posx},
		posy_{posy},
		text_{text}{}

		virtual ~Label(){};
		Label() = default;

		std::string getText(){return text_;}
		void setText(std::string const& text){text_=text;}
    template<class Renderer>void render(Renderer& renderer);

	private:
		double posx_;
		double posy_;
		Dimensions dims_;
		std::string text_;
	};

	template<class Renderer>
	void 
	Label::render(Renderer& renderer){
		renderer.renderTextInPos(posx_, posy_, 0,text_ ,0,0);
	}

	class Separator
	{
	public:
		Separator(double posx,double posy,double length, bool isHorizontal = true):
		posx_{posx},
		posy_{posy},
		length_{length},
		color_{0.0,0.0,0.0,1.0},
		thickness_{0.5},
		isHorizontal_{isHorizontal}{}
	template<class Renderer>void render(Renderer& renderer);
		void setLength(double length){length_=length;}


	private:
		double posx_;
		double posy_;
		double length_;
		std::array<float, 4> color_;
		double thickness_;
		bool isHorizontal_;

	};

	template<class Renderer>
		void 
		Separator::render(Renderer& renderer){
			renderer.setColor(color_);
			renderer.setLineThickness(thickness_);
			renderer.begin(Renderer::DrawingMode::LINES);
			if(isHorizontal_){
				renderer.vertex(posx_		 ,posy_);
				renderer.vertex(posx_+length_,posy_);
		}else{ //vertical
			renderer.vertex(posx_, posy_);
			renderer.vertex(posx_, posy_ + length_);
		}
		renderer.end();
	}

	//---------------------------------------------------------------------
	//SLIDER 
	//---------------------------------------------------------------------

	template<class VarT>
	class Slider
	{
	public:

		Slider(VarT val, 
			VarT left, 
			VarT right, 
			double posx,
			double posy,
			double size,
			double height,
			double dy,
			bool active = true,
			double separation = 20):
		val_{val},
		parameterLeft_{left},
		parameterRight_{right},
		active_{active},
		textPosy_{posy+dy},
		selected_{false},
		separation_{separation},
		dims_{posx,posy,size+separation,height}{};


		Slider(VarT val, 
			VarT left, 
			VarT right, 
			Dimensions dims,
			double dy,
			bool active = true,
			double separation = 20):
		val_{val},
		parameterLeft_{left},
		parameterRight_{right},
		active_{active},
		textPosy_{dims.posy+dy},
		selected_{false},
		separation_{separation},
		dims_{dims}{};

		virtual ~Slider(){};
		Slider() = default;
		VarT getVal(){return val_;}
		VarT getLeft(){return parameterLeft_;}
		VarT getRight(){return parameterRight_;}
		bool processSelection(double x, double y);
		void processMove(double x, double y);
	    template<class Renderer>void render(Renderer& renderer);

		bool isActive(){return active_;}
		void setActive(bool state){active_=state;}

		bool isSelected(){return selected_;}
		void setSelected(bool state){selected_=state;}

		void setRange(VarT left, VarT right){parameterLeft_=left;parameterRight_=right;}
		void setVal(VarT val){val_=val;}

		Dimensions const& getDimensions(){return dims_;}

		constexpr double toX(VarT val) const {return dims_.posx + (dims_.width*(val- parameterLeft_))/(parameterRight_-parameterLeft_);}
		constexpr VarT toVal(double x) const {
			auto val = parameterLeft_ + static_cast<VarT>((parameterRight_-parameterLeft_)*(x-dims_.posx)/dims_.width); 
			auto min = parameterLeft_;
			auto max = parameterRight_;

		        if(max<min){ //swap if inverse
		        	auto t = min;
		        	min = max;
		        	max = t;
		        }
		        //return val;
		        return val < min ? min : val > max ?  max : val;
		    }

		private:
			VarT val_;
			VarT parameterLeft_;
			VarT parameterRight_; 
			Dimensions dims_;
			double textPosy_;
			double separation_;
			bool active_;
			bool selected_;


		};

	template<class VarT>
		void 
		Slider<VarT>::processMove(double x, double y){
			if(active_){
				val_ = toVal(x);
			}	
		}

	//this is a regular slider, renderer on screen coordinates
	template<class VarT>
		bool 
		Slider<VarT>::processSelection(double x, double y){
	    //std::cout<<x<<", "<<y<<std::endl;
			auto posX = toX(val_);
			if(active_ && y >= dims_.posy  && y <= dims_.posy + dims_.height){ 
	    //if it is within range of slider
				if(x >= dims_.posx  && x <= dims_.posx+dims_.width){ 
	        //if(x >= dims_.posX - 10  && x <= posX + 10){ 
					val_ = toVal(x);
					return true;
				}    
			}
			return false;
		}

	template<class VarT>
	template<class Renderer>
		void 
		Slider<VarT>::render(Renderer& renderer){
			if(!active_){
				return;
			}

			auto posX = toX(val_);

			auto width = dims_.width;

			renderer.setColor(0,0,0);

	    //val
			renderer.setLineThickness(2.0f);
			renderer.begin(Renderer::DrawingMode::LINES);
			renderer.vertex(posX, dims_.posy,0);
			renderer.vertex(posX, dims_.posy+ dims_.height,0);
			renderer.end();

			renderer.setLineThickness(1.0f);
			renderer.begin(Renderer::DrawingMode::LINES);
	    //begin
			renderer.vertex(dims_.posx, dims_.posy,0);
			renderer.vertex(dims_.posx, dims_.posy + dims_.height,0);

	    //end
			renderer.vertex(dims_.posx + width, dims_.posy,0);
			renderer.vertex(dims_.posx + width, dims_.posy + dims_.height,0);

			renderer.end();

			std::ostringstream sStream;
		// sStream << val_;
			sStream << std::fixed << std::setprecision(2) << val_;
	    //sStream << "hallo?";
	    //renderer.renderTextInPos(posX, dims_.posy, 0, sStream.str(),0,-10);

			renderer.renderTextInPos(dims_.posx + dims_.width + separation_ , textPosy_, 0, sStream.str(),0,0);
		}


	//---------------------------------------------------------------------
	//BUTTON
	//---------------------------------------------------------------------


	class Button
	{
	public:
		Button(
		double posx, //posx and y are the positions of the text.
		double posy,
		double w,
		double h,
		double dx,
		double dy,
		std::string const& onText,
		std::string const& offText,
		bool state=true):
		state_{state},
		onText_{onText},
		offText_{offText},
		dims_{posx,posy,w,h},
		color_{0.9,0.9,0.9,1.0},
		label_{posx+dx,posy+dy, state ? onText_ : offText_ }{}


		Button(
			Dimensions dims,
			double dx,
			double dy,
			std::string const& onText,
			std::string const& offText,
			bool state=true):
		state_{state},
		onText_{onText},
		offText_{offText},
		dims_{dims},
		color_{0.9,0.9,0.9,1.0},
		label_{dims.posx+dx,dims.posy+dy, state ? onText_ : offText_ }{}

		virtual ~Button(){};
		Button() = default;

		void set(bool state){
			state_=state;
			label_.setText(state ? onText_ : offText_ );
		}

		bool processClick(double x, double y){
			if(y >= dims_.posy  && y <= dims_.posy + dims_.height && x >= dims_.posx && x <= dims_.posx+dims_.width){
			set(!state_);//toggle
			return true;   
		}
		return false;
	}

	bool getState(){return state_;}
	Dimensions const& getDimensions(){return dims_;}

	template<class Renderer>void render(Renderer& renderer);

	private:
		bool state_;
		std::string onText_;
		std::string offText_;
		Dimensions dims_;
		Label label_;
		std::array<float, 4> color_;

	};

	template<class Renderer>
	void 
	Button::render(Renderer& renderer){

		renderer.setColor(color_);
		renderer.begin(Renderer::DrawingMode::TRIANGLE_FAN);
		renderer.vertex(dims_.posx      		, dims_.posy,depth);
		renderer.vertex(dims_.posx + dims_.width, dims_.posy,depth);
		renderer.vertex(dims_.posx + dims_.width, dims_.posy + dims_.height,depth);
		renderer.vertex(dims_.posx              , dims_.posy + dims_.height,depth);
		renderer.end();

		label_.render(renderer);

	}


	//---------------------------------------------------------------------
	// MULTI-STATE BUTTON
	//---------------------------------------------------------------------


	template<class EnumT> class MultiStateButton
	{
	public:

		using StateMessage = std::pair<EnumT, std::string>;

		MultiStateButton(
			double posx, //posx and y are the positions of the text.
			double posy,
			double w,
			double h,
			double dx,
			double dy,
			std::vector<StateMessage> const& stateMessages,
			EnumT current):
		dims_{posx,posy,w,h},
		stateMessages_{stateMessages},
		color_{0.9,0.9,0.9,1.0}{
			stateNum_ = find(current);
			label_ = Label{posx+dx,posy+dy, stateMessages_[stateNum_].second};
		}

		MultiStateButton(
			Dimensions dims,
			double dx,
			double dy,
			std::vector<StateMessage> const& stateMessages,
			EnumT current):
		dims_{dims},
		stateMessages_{stateMessages},
		color_{0.9,0.9,0.9,1.0}{
			stateNum_ = find(current);
			label_ = Label{dims_.posx+dx,dims_.posy+dy, stateMessages_[stateNum_].second};
		}
		virtual ~MultiStateButton(){};
		MultiStateButton() = default;

		void set(EnumT state){stateNum_=find(state); label_.setText(stateMessages_[stateNum_].second);}

		bool processClick(double x, double y);
		EnumT getState(){return stateMessages_[stateNum_].first;}
		Dimensions const& getDimensions(){return dims_;}

		template<class Renderer>void render(Renderer& renderer);

	private:
		int find(EnumT state);

	private:
		int stateNum_; //the index of the current state
		std::vector<StateMessage> stateMessages_;
		Dimensions dims_;
		Label label_;
		std::array<float, 4> color_;
	};


	template<class EnumT> 
	int
	MultiStateButton<EnumT>::find(EnumT state){
		int s=0;
		// for(auto stateMessage: stateMessages_){
		// 	if(stateMessage.first == state){
		// 		break;
		// 	}
		// 	++s;
		// }
		return s >= stateMessages_.size() ? 0 : s;
	}

	template<class EnumT> 
	bool
	MultiStateButton<EnumT>::processClick(double x, double y){
		if(y >= dims_.posy  && y <= dims_.posy + dims_.height && x >= dims_.posx && x <= dims_.posx+dims_.width){
			++stateNum_;
			stateNum_ = stateNum_ % stateMessages_.size();
			label_.setText(stateMessages_[stateNum_].second);
			return true;   
		}
		return false;
	}


	template<class EnumT> 
	template<class Renderer>
	void 
	MultiStateButton<EnumT>::render(Renderer& renderer){

		renderer.setColor(color_);
		renderer.begin(Renderer::DrawingMode::TRIANGLE_FAN);
		renderer.vertex(dims_.posx      		, dims_.posy,depth);
		renderer.vertex(dims_.posx + dims_.width, dims_.posy,depth);
		renderer.vertex(dims_.posx + dims_.width, dims_.posy + dims_.height,depth);
		renderer.vertex(dims_.posx     			, dims_.posy + dims_.height,depth);
		renderer.end();

		label_.render(renderer);

	}



	//---------------------------------------------------------------------
	//CIRCULAR CHART
	//---------------------------------------------------------------------

	class CircularChart
	{
	public:

		CircularChart(
			double posx,
			double posy,
			double diam
	        // std::string const& text =""

			):
		rad_{diam/2.0},
		cx_{posx+diam/2.0},
		cy_{posy+diam/2.0},
		dims_{posx,posy, diam,diam}{}

		virtual ~CircularChart(){};
		CircularChart() = default;
	    template<class Renderer>void render(Renderer& renderer,double portion);

		Dimensions const& getDimensions(){return dims_;}

	private:
		double cx_;
		double cy_;
		double rad_;
		Dimensions dims_;
	};

	template<class Renderer>
	void 
	CircularChart::render(Renderer& renderer, double portion){

	    // renderer.renderTextInPos(cx_, cy_, 0,text_ ,0,0);
		int slides = 100;
		double step = 2*PI/slides;
		int limit = (1.0-portion) * slides;

		renderer.setColor({1.0f,1.0f,1.0f,1.0f});
		renderer.begin(Renderer::DrawingMode::TRIANGLE_FAN);
		renderer.vertex({static_cast<float>(cx_),static_cast<float>(cy_),1});
		bool set=false;
		for (int i = 0; i <= slides; ++i){

			if(!set && i>=limit ){
				renderer.end();
				renderer.setColor({0.0f,0.0f,0.0f,1.0f});
				renderer.begin(Renderer::DrawingMode::TRIANGLE_FAN);
				renderer.vertex({static_cast<float>(cx_),static_cast<float>(cy_),1});   
				set=true;  
			}

			double ang = i*step + PI/2;
			renderer.vertex({static_cast<float>(cx_+rad_*std::cos(ang)),static_cast<float>(cy_+rad_*std::sin(ang)),1});
		}
		renderer.end();

	}

	class UniformGridLayout{
	public:

		//this creates an evenly spaced layout, with even margins
		UniformGridLayout(
			int columns, int rows, 
			Dimensions dims,
			double horizontalSeparation, double verticalSeparation,
			double margin = 0):
		numCols_{columns}, numRows_{rows},
		dims_{dims},
		top_{margin},bottom_{margin},left_{margin},right_{margin},
		verticalSeparation_{verticalSeparation},
		horizontalSeparation_{horizontalSeparation}{
			if(columns <= 0 || rows <= 0){
				throw std::runtime_error("It is not possible to create a layout of 0 or negative width or height.");
			}
			cellWidth_ = (dims_.width-left_-right_-(columns-1)*horizontalSeparation_)/columns;
			cellHeight_ = (dims_.height-top_-bottom_-(rows-1)*verticalSeparation_)/rows;
		}
		Dimensions getDimensions(){return dims_;}

		Dimensions layout(int col, int row, int numCols, int numRows ){
			if(col < 0 || row < 0){
				throw std::runtime_error("Column and row index need to be 0 or positive.");
			}
			if(col>=numCols_ || row >= numRows_){
				throw std::runtime_error("Row or column number larger than those in layout.");
			}
			return {
				dims_.posx+left_+(cellWidth_+horizontalSeparation_)*col,
				dims_.posy+bottom_+(cellHeight_+verticalSeparation_)*row,
				(cellWidth_+horizontalSeparation_) * (numCols-1) + cellWidth_,
				(cellHeight_+verticalSeparation_) * (numRows-1) + cellHeight_
			};
		}

		Separator createHorizontalSeparator(int row){
			return {
				dims_.posx,
				dims_.posy + bottom_ + (cellHeight_+verticalSeparation_)*row -verticalSeparation_/2,
				dims_.width,
				true
			};
		}

		Separator createVerticalSeparator(int column){
			return {
				dims_.posx+ left_ + (cellWidth_+horizontalSeparation_)*column -horizontalSeparation_/2,
				dims_.posy,
				dims_.height,
				false
			};
		}

		
	private:
		Dimensions dims_;
		int numCols_;
		int numRows_;
		double top_;
		double bottom_;
		double left_;
		double right_;
		double cellWidth_;
		double cellHeight_;
		double verticalSeparation_;
		double horizontalSeparation_;


	};



}