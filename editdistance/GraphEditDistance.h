
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef EDITDISTANCEGRAPHEDITDISTANCE_H_
#define EDITDISTANCEGRAPHEDITDISTANCE_H_ 

#include <kuhnmunkres/Matrix.h>
#include <kuhnmunkres/KuhnMunkres.h>
#include <graph/allIn.h>
#include <cstdlib>

//it uses any graph that implements the operations of Boost Graph 

namespace editdistance {



/**
* considering the size n of cA larger or equal to the size m of cB 
* (number of elements, nodes in the graph, for example),then the resulting 
* matrix is as follows:
* 
* 		     Costs of substitution (for all elements in cA for each node in cB)
* 		     |
* 		     v
* 
* 		     m       n
* 			--- --- --- ---
* 		  |    |           |
* 		n |    |           | <-- cost of deletion for each node in cA
* 		  |    |           |
* 		    --- --- --- ---
* 		m |    |           | <-- all zeros
* 		    --- --- --- ---
* 		     ^ 
* 		     |
* 		     Cost of inserting each node in cB
* 
* 
* This has been implemented as in the description in:
* "Approximate graph edit distance computation by means of bipartite graph matching" 
* Kaspar Riesen , Horst Bunke
* Image and Vision Computing 27 (2009) 950–959
*
*/

/**
* This class abstracts the algorithms for generating the matrices. It takes two CT (container type)
* Objects and a CostCalculator type. CT needs to have a begin() and end() that return types
* corresponding to the iterator concept, and a size() method that returns the number of elements
* The cost object needs to implement the substitution(), deletion() and insertion() functoins
* For the type of objects iterated through.
*/


	template<class CT, class CostCalculator> 
	kuhnmunkres::Matrix<int> 
	generateMatrixOrdered(CT const& cA, CT const& cB, CostCalculator const& costs){

		//make gA always have more or equal elements than gB;
		int n=cA.size();
		int m=cB.size();
		
		if(n<m){
			throw(std::invalid_argument("The first graph is smaller that the second in generateMatrix()"));
		}

		int mSize=n+m;
		auto matrix=kuhnmunkres::createMatrix(mSize,mSize, 0); 

		//and now fill...(all zeros are already set)

		//substitution costs:
		int r=0;
		for(auto vId: cA){
			int c=0;
			for(auto uId: cB){
				matrix(r,c)=costs.substitution(vId,uId);
				++c;
			}
			++r;
		}

		//deletion cost
		r=0;
		for(auto vId: cA){//in rows
			for(size_t c=m;c<m+n;++c){ //columns
				if(r==(c-m)){ //if in diagonal
					matrix(r,c)=costs.deletion(vId);
				}
				else{ //not diagonal, set it to largest possible (implementation tested, it can deal with it)
					matrix(r,c)=std::numeric_limits<int>::max();
				}
			}
			++r;
		}

		//insertion cost
		for(size_t r=n;r<n+m;++r){ //rows 
			int c=0;
			for(auto uId: cB){//in columns
				if((r-n)==c){ //if in diagonal
					matrix(r,c)=costs.insertion(uId);
				}
				else{ //not diagonal, set it to largest possible (implementation tested, it can deal with it)
					matrix(r,c)=std::numeric_limits<int>::max();
				}
				++c;
			}
		}

		return matrix;
	}


	//this is just a simple way of making sure they are ordered without swaping...

	template<class CT, class CostCalculator> 
	kuhnmunkres::Matrix<int> 
	generateMatrix(CT const& cA, CT const& cB, CostCalculator const& costs){

		//make gA always have more or equal nodes than gB;
		if(cA.size()<cB.size()){
			//std::cout<<"swapped in calculating"<<std::endl;
			return generateMatrixOrdered(cB,cA,costs);
		}
		else{
			return generateMatrixOrdered(cA,cB,costs);
		}
	}


}

#endif