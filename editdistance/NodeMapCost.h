
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef EDITDISTANCENODEMAPCOST_H_
#define EDITDISTANCENODEMAPCOST_H_ 
namespace editdistance {

	/**
	* 
	*/

	class NodeMapCost
	{
	public:
		template <class VMInf> int substitution(VMInf vInfA,VMInf vInfB) const;
		template <class VMInf> int deletion (VMInf vInf)const;
		template <class VMInf> int insertion(VMInf vInf)const;
	};

	template<class VMInf>
	int
	NodeMapCost::substitution(VMInf vInfA,VMInf vInfB)const{

		auto const& mA=vInfA.first;
		auto const& vA=vInfA.second;

		auto const& mB=vInfB.first;
		auto const& vB=vInfB.second;

        int dif=mA.at(vA)-mB.at(vB);  //at() allows to use standard maps as const 
		return std::abs(dif);
	}

	template<class VMInf>
	int
	NodeMapCost::deletion(VMInf vInf)const{
		auto const& m=vInf.first;
		auto const& v=vInf.second;
        return m.at(v); //at() allows to use standard maps as const 
	}

	template<class VMInf>
	int 
	NodeMapCost::insertion(VMInf vInf)const{
		auto const& m=vInf.first;
		auto const& v=vInf.second;
        return m.at(v); //at() allows to use standard maps as const 
	}
}
#endif
