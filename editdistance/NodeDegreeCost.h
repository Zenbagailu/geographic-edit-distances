
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef EDITDISTANCENODEDEGREECOST_H_
#define EDITDISTANCENODEDEGREECOST_H_ 
namespace editdistance {

	/**
	* it calculates a distance depending of the difference in degree of the nodes
	*/

	class NodeDegreeCost
	{
	public:
		template <class VInf> int substitution(VInf vInfA,VInf vInfB) const;
		template <class VInf> int deletion (VInf vInf)const;
		template <class VInf> int insertion(VInf vInf)const;
	};

	template<class VInf>
	int
	NodeDegreeCost::substitution(VInf vInfA,VInf vInfB)const{

		auto const& gA=vInfA.first;
		auto const& vA=vInfA.second;

		auto const& gB=vInfB.first;
		auto const& vB=vInfB.second;

		int dif=boost::degree(vA, gA) - boost::degree(vB, gB);
		return std::abs(dif);
	}

	template<class VInf>
	int
	NodeDegreeCost::deletion(VInf vInf)const{
		auto const& g=vInf.first;
		auto const& v=vInf.second;
		return boost::degree(v, g);
	}

	template<class VInf>
	int 
	NodeDegreeCost::insertion(VInf vInf)const{
		auto const& g=vInf.first;
		auto const& v=vInf.second;
		return boost::degree(v, g);
	}
}
#endif
