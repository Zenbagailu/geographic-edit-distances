
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#ifndef EDITDISTANCEMAPCONTAINER_H_
#define EDITDISTANCEMAPCONTAINER_H_ 


namespace editdistance{

    /**
     * This is all to wrap a graph  or property map and vertex iterators together. Otherwise, only with the iterator,
     * it is not possible to access any information from the graph of the vertex (adjacencies, etc) or any data associated
     * with the vertex on a map. Doing it like this, the matrix construction algorithm can be generalised, so it deals 
     * only with iterators; then it can be reused for any type of sets of objects. In this case the vertex iterator returns
     *  a pair object. If it would be necessary to also be able to access edges for each node, for example, it would be 
     *  necessary to implement another Container that would have vertex id, graph and map data, possibly even an edge map... 
     *  see GraphEditDistance to analyse the abstraction of the algorithm and the necessary implementation of the interface 
     *  for the "container" and for the "cost"
	*/

    //Iterator::value_type

	template <class MT, class IT>  class VertexMapIterator{
	public:
        using MapType           = MT;
        using VertexIt          = IT;
		using VertexDesc        = typename VertexIt::value_type;
		using VertexMapPair     = std::pair<MapType const&, VertexDesc>;

		VertexMapIterator(MapType const& mapRef, VertexIt vIt):mapRef_{mapRef},vIt_{vIt}{}

		VertexMapPair operator *(){
			return VertexMapPair{mapRef_, *vIt_};
		}

		bool operator !=(VertexMapIterator other){
			return 
			&(mapRef_) 	!= &(other.mapRef_) ||  
			vIt_ 			!= other.vIt_ ;
		}

		VertexMapIterator operator ++(){ //only preincrement operator necessary
			//create new vertex info with an iterator that is ++  and return 
			return VertexMapIterator{mapRef_,++vIt_};
		}
	private:
		MapType const& mapRef_;
		VertexIt  vIt_;
	};

	//Using IteratorPair it can be parametricised for all vertices or neighbouring vertices, for example
	//it obviously gets invalidated if the graph is changed...
   
    //This class should only be used as a wrapper when passing to distance calculations. Otherwise, 
    //because it has a pointer (or reference) to the map it uses, it may be invalidated while copying, etc.

	template <class MT, class IP, typename ST> class VertexMapContainer{
	public:

		using Map 		    = MT;
		using IteratorPair  = IP;

		using VertexMapIterator = VertexMapIterator<Map, typename IteratorPair::first_type>;  //both have same type...

		VertexMapContainer(Map const& map, IteratorPair itPair, ST size):mapRef_(map),itPair_{itPair},size_{size}{}

		VertexMapIterator begin() const{return VertexMapIterator{mapRef_,itPair_.first};}
		VertexMapIterator end()const{return VertexMapIterator{mapRef_,itPair_.second};}
		size_t size()const {return size_;}

	private:
		//possibly a better idea is to only deal with pointers (and use shared pointers, for example)
        //rather than references
		Map const& mapRef_; 
		IteratorPair itPair_;
		ST const size_;
	};

	template <class MT, class IP, typename ST> VertexMapContainer<MT,IP,ST> createVertexMapContainer(MT const& map, IP iteratorPair, ST size){
		return VertexMapContainer<MT,IP,ST>(map,iteratorPair,size);

	}
}

#endif