
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef EDITDISTANCENEIGHBOURDEGREECOST_H_
#define EDITDISTANCENEIGHBOURDEGREECOST_H_ 
#include "VertexMapContainer.h"
namespace editdistance {

	/**
	* it calculates a distance depending of the difference in degree of the nodes that a node has as neighbours
	*/

	class NeighbourDegreeCost
	{
	public:
		template <class VInf> int substitution(VInf vInfA,VInf vInfB) const;
		template <class VInf> int deletion (VInf vInf)const;
		template <class VInf> int insertion(VInf vInf)const;
	};

	template<class VInf>
	int
	NeighbourDegreeCost::substitution(VInf vInfA,VInf vInfB)const{

		auto const& gA=vInfA.first;
		auto const& vA=vInfA.second;

		auto const& gB=vInfB.first;
		auto const& vB=vInfB.second;

		 auto vANeighbourContainer=editdistance::createVertexMapContainer(gA,                              //graph
                                                                       boost::adjacent_vertices(vA, gA),//pair of begin,end
                                                                       boost::degree(vA, gA));          //size

		 auto vBNeighbourContainer=editdistance::createVertexMapContainer(gB, 
                                                                       boost::adjacent_vertices(vB, gB),
                                                                       boost::degree(vB, gB));

		// //for the cost calculator (for all neighbours...) one can simply use the NodeDegreeCost.
        //and treat the neighbour noodes as graphs to compare
		NodeDegreeCost costCalculator;

		auto edgeSubstitutionMatrix=generateMatrix(vANeighbourContainer,vBNeighbourContainer,costCalculator);

		auto result=kuhnmunkres::calculateKuhnMunkres(reducedColumnsCopy(edgeSubstitutionMatrix) );
		auto cost = assignmentCost(edgeSubstitutionMatrix,result);
		 return cost;

	}

	template<class VInf>
	int
	NeighbourDegreeCost::deletion(VInf vInf)const{
		auto const& g=vInf.first;
		auto const& v=vInf.second;
		int cost= 0;
		for(auto vo : graph::allIn(boost::adjacent_vertices(v, g))){
			cost+=boost::degree(vo, g);
		}
		return cost;
	}

	template<class VInf>
	int 
	NeighbourDegreeCost::insertion(VInf vInf)const{
		auto const& g=vInf.first;
		auto const& v=vInf.second;
		int cost= 0;
		for(auto vo : graph::allIn(boost::adjacent_vertices(v, g))){
			cost+=boost::degree(vo, g);
		}
		return cost;
	}
}
#endif