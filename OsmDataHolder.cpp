
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "OsmDataHolder.h"
#include <algorithm>
#include <osm/OsmClient.h>
#include <boost/property_tree/json_parser.hpp>
#include <set>

namespace test {

    WayData getWay(boost::property_tree::ptree const& tree){
    
        std::vector<OsmDataHolder::IdType> ids;
        
        for (boost::property_tree::ptree::value_type const& nodeId : tree.get_child("nodes") ){
            ids.push_back(nodeId.second.get_value<OsmDataHolder::IdType>());
        }

        return {ids,getAttributeContainerFromTags(tree)};
    }

    template<class Projector>NodeData getNode(boost::property_tree::ptree const& tree, Projector projector){

        boost::property_tree::ptree const& id = tree.get_child("id");
        auto idName=id.get_value<OsmDataHolder::IdType>();
        boost::property_tree::ptree const& lat = tree.get_child("lat");
        auto latData=lat.get_value<double>();
        boost::property_tree::ptree const& lon = tree.get_child("lon");
        auto lonData=lon.get_value<double>();
        return{idName,lonData,latData, projector,tree};  
    }

    void OsmDataHolder::addData(std::string  const& query){

        auto tree=getPtreeFromQuery(query);

        std::set<IdType> usedNodesIds; //to store nodes used in paths. Skip drawing 
        std::set<IdType> usedWaysIds; //to store ways used in relations. Skip drawing 

        for (boost::property_tree::ptree::value_type &element : tree.get_child("elements"))
        {
            // Get the label of the element
            //std::string name = element.first; //the label is going to be empty string in this case
            boost::property_tree::ptree& elementTree = element.second;
            boost::property_tree::ptree& type = elementTree.get_child("type");
            auto typeName=type.data();
            //in this case it is going to be element.
            boost::property_tree::ptree& id = elementTree.get_child("id");
            auto idName=id.get_value<IdType>();
            
            if(typeName=="relation"){ //it is a relation

                //NONE OF THIS IS REALLY NECESSARY: KEEP THE CODE JUST IN CASE
                //It will through an exception if there is a tag with the relation type
                //(if the query included out skel, for example, there will be no tags)
                //and it does not correspont to multipolygon (for example waterways)
                //It is just to remind to implement those cases than anything else.

                //search for tag defining the type of relation
                auto relationType = elementTree.get_optional<std::string>("tags.type");
                if(!relationType){
                    std::cout<<"no relation type found"<<std::endl;
                }
                else{
                    if(*relationType!="multipolygon"){
                        //it throws right now to make clear that is not implemented
                        //throw std::runtime_error("cannot process relations of type: " + relationType );
                        std::cout<<"found relation of type " << relationType << ", which is not implemented yet" <<std::endl;
                        continue;
                    }

                }
                
                std::vector<IdType> outerWaysIds;
                std::vector<IdType> innerWaysIds;

                for (boost::property_tree::ptree::value_type &member : elementTree.get_child("members")){
                    //since we are only allowing multipolyons, they will be ways.
                    //
                    auto memberTree=member.second;

                    auto memberType=memberTree.get<std::string>("type");

                    if(memberType == "relation"){
                        std::cout<<"processing for relation within relation not implemented."<<std::endl;
                        continue;
                    }
                    if(memberType != "way"){
                        std::cout<<"Non way member in relation, processing of "<< memberType <<" not implemented."<<std::endl;
                        continue;
                        // throw std::runtime_error("non way member in relation: " +  memberType );
                    }
                    auto refId=memberTree.get<size_t>("ref");
                    usedWaysIds.insert(refId);
                    auto memberRole=memberTree.get<std::string>("role");
                    if(memberRole == "outer"){
                        outerWaysIds.push_back(refId);    
                    }
                    else if(memberRole == "inner"){
                        innerWaysIds.push_back(refId);    
                    }
                    else{
                        throw std::runtime_error("unknown type of role " + memberRole);
                    }         
                }

                relations_.push_back({outerWaysIds,innerWaysIds,getAttributeContainerFromTags(elementTree)});


            //we originally store all nodes and ways as dependent, and move them 
            //when we actually know if they are not
            }else if(typeName=="node"){ //it is a node
                dependentNodes_[idName]=getNode(elementTree,projector_);

            }
            else if(typeName=="way"){
                dependentWays_[idName]=getWay(elementTree);
                for(auto nodeId: dependentWays_[idName].nodeIds){
                    usedNodesIds.insert(nodeId);
                }
            }
            else{
                throw std::runtime_error("unknown element type in osm: " + typeName );
            }

        }



        for(auto iter = dependentWays_.begin(); iter != dependentWays_.end();){
            if(usedWaysIds.find(iter->first) == usedWaysIds.end()){
                ways_.push_back(std::move(iter->second));
                iter=dependentWays_.erase(iter);
            }
            else{
                iter++;
            }
        }

        for(auto iter = dependentNodes_.begin(); iter != dependentNodes_.end(); ){
            if(usedNodesIds.find(iter->first) == usedNodesIds.end()){
                nodes_.push_back(std::move(iter->second));
                iter=dependentNodes_.erase(iter);
            }
            else{
                iter++;
            }
        }

        std::cout<<"number of relations: "<< relations_.size() << std::endl;
        std::cout<<"number of ways: "<< ways_.size() << std::endl;
        std::cout<<"number of nodes: "<< nodes_.size() << std::endl; 

    }


    OsmDataHolder::OsmDataHolder(
                                 std::string  const& query,
                                 double centreLat,
                                 double centreLon
                                 ):
    projector_{centreLon, centreLat}
    {

        addData(query);

        
    }



}