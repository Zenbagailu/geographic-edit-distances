
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "FontAtlas.h"
#include "Face.h"

#include <vector>
#include <stdexcept>
#include <cstdlib>

#include <locale> //for conversions
#include <codecvt>//for conversions

#include <glaux/functions.h>

namespace glfonts {

	FontAtlas::FontAtlas(FontProgram fontProgram, Face const& face):
	fontProgram_(fontProgram)
	 {
	 	auto ftFace=face.getFT_Face();

        numOfChars_=ftFace->num_glyphs;

		charactersInfoPtr_=std::make_shared<CharactersInfoContainer>(numOfChars_); 

		findMinimumSizeOfTexture(ftFace);

		/* Create a texture that will be used to hold all glyphs */
		bindTexture();

		/* We require 1 byte alignment when uploading texture data */
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		/* Clamping to edges is important to prevent artifacts when scaling */
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		/* Linear filtering usually looks best for text */
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


		glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, w_, h_, 0, GL_ALPHA, GL_UNSIGNED_BYTE, 0);

		pasteSubglyphsOnTexture(ftFace);

		//glBindTexture(GL_TEXTURE_2D, 0);
		// std::cout<<"Generated a "<< w_ <<" x "<< h_ <<" "<< w_ * h_ / 1024 <<" kb atlas"<< std::endl;
	
	}

	FontAtlas::~FontAtlas() {
	}

	void FontAtlas::findMinimumSizeOfTexture(FT_Face const& face){
         
		FT_GlyphSlot g = face->glyph;
		unsigned int roww = 0;
		unsigned int rowh = 0;
		 w_ = 0;
		 h_ = 0;

		/* Find minimum size for a texture holding all visible ASCII characters */
		for (int i = 0; i < numOfChars_; i++) {
            auto glyphIndex = FT_Get_Char_Index(face,i);
            if (FT_Load_Glyph(face, glyphIndex, FT_LOAD_RENDER | FT_LOAD_TARGET_LIGHT)) {
				throw std::runtime_error("Loading character " + std::string(1,static_cast<wchar_t>(i))  + " failed");
			}
			if (roww + g->bitmap.width + 1 >= MAXWIDTH) {
				w_ = std::max(w_, roww);
				h_ += rowh;
				roww = 0;
				rowh = 0;
			}

			roww += g->bitmap.width + 1;
			rowh = std::max(rowh, static_cast<unsigned int>(g->bitmap.rows));
		}

		w_ = std::max(w_, roww);
		h_ += rowh;
	}

	void FontAtlas::pasteSubglyphsOnTexture(FT_Face const& face){
		/* Paste all glyph bitmaps into the texture, remembering the offset */
		FT_GlyphSlot g = face->glyph;
		int ox = 0;
		int oy = 0;

		unsigned int rowh = 0;


		for (int i = 0; i < numOfChars_; i++) { 	
            auto glyphIndex = FT_Get_Char_Index(face,i);
            if (FT_Load_Glyph(face, glyphIndex, FT_LOAD_RENDER | FT_LOAD_TARGET_LIGHT)) {
				throw std::runtime_error("Loading character " + std::string(1,static_cast<wchar_t>(i))  + " failed");
			}

			if (ox + g->bitmap.width + 1 >= MAXWIDTH) {
				oy += rowh;
				rowh = 0;
				ox = 0;
			}

			if(g->bitmap.width>0 && g->bitmap.rows>0){
				//only the alpha channel is used (this is for glsl es 1.0)
				//in later versions R channel must be used instead, as in:
				//http://www.lazyfoo.net/tutorials/OpenGL/35_glsl_font/index.php

				glTexSubImage2D(GL_TEXTURE_2D, 0, ox, oy, g->bitmap.width, g->bitmap.rows, GL_ALPHA, GL_UNSIGNED_BYTE, g->bitmap.buffer);
			}
			
			auto& charsInfo=*charactersInfoPtr_;

			charsInfo[i].advanceX = g->advance.x >> 6; // >> 6, store advance in pixels: https://www.freetype.org/freetype2/docs/tutorial/step2.html
			charsInfo[i].advanceY = g->advance.y >> 6;

			charsInfo[i].bitmapWidth = g->bitmap.width;
			charsInfo[i].bitmapHeight = g->bitmap.rows;

			charsInfo[i].bitmapLeft = g->bitmap_left;
			charsInfo[i].bitmapTop = g->bitmap_top;

			charsInfo[i].textureX = static_cast<float>(ox) / w_;
			charsInfo[i].textureY = static_cast<float>(oy) / h_;

			rowh = std::max(rowh, g->bitmap.rows);
			ox += g->bitmap.width + 1;
		}

	}

	void FontAtlas::bindTexture()const{
		glBindVertexArray(vAO_.getIndex());
		glActiveTexture(GL_TEXTURE0);
		glUniform1i(fontProgram_.textureUnitLocation, 0);
		glBindTexture(GL_TEXTURE_2D,  texture_.getIndex());
	}

    //simply convert a standard string  of 8 bit chars (possibly using extended characters through
    //UTF-8, ISO/IEC 8859-1 for latin alphabet characters) to wchar_t string using the correct
    //encoding
    int FontAtlas::renderText(std::string const& text, int x, int y, int scw, int sch) {
        static auto converter=std::wstring_convert<std::codecvt_utf8<wchar_t>>{};
        return renderText(converter.from_bytes(text.data()),x,y,scw,sch);

    }

    //x and y are in pixel coordinates and follow the traditional convention,
    //in which the left border of the screen is x=0, and the bottom y=0, 
    //top and right directions are positive directions of the axes
    //it returns the final values of x 

    //It is better to use std::wstring. The Glyphs will be generated with the wchar_t encoding
    // for the machine/OS (UNICODE, in the case od OSX, for example).

    //TODO: 
    //also, if the scaling and rtanslformations would be set correctly before, it would not be necessary to pass scw and sch.
    //In fact this is a very hacky thing to do. The transformations to the shader program should be done somewhere else,
    //and this would not need to make any use of screen dimensions. 
	int FontAtlas::renderText(std::wstring const& text, int x, int y, int scw, int sch) {

		if(charactersInfoPtr_== nullptr){
			throw std::runtime_error("character info is empty in Font Atlas. It may not have been intialised.");
		}

		//this is a shortcut to using a transformation matrix. An ortho projection matrix would 
		// have as its M[0][0]  2.f/(l-r), and as its M[1][1]  2.f/(t-b):
		//https://en.wikipedia.org/wiki/Orthographic_projection

		float sx = 2.f/scw;
		float sy = 2.f/sch;

		struct point {
			GLfloat x;
			GLfloat y;
			GLfloat s;
			GLfloat t;
		};

		std::vector<point> coords(text.length()*6);


		/* Loop through all characters */
		int ct=0;
		for(wchar_t p : text) { //wchar_t because auto makes it into char, which drops all non ascii characters

            if(p>=numOfChars_){
                //throw std::runtime_error("Character " + std::string(1,p)  + " does not exist in this fontAtlas.");
                //It does not exist in this font atlas, so ignore (don't render at all). Another possibility
            	//is to choose a default character, like space (ascii 32).
                continue;
            }
			/* Calculate the vertex and texture coordinates */

			auto cInfo= (*charactersInfoPtr_)[p];

			float x2 = (x + cInfo.bitmapLeft)* sx-1;
			float y2 = (-y - cInfo.bitmapTop)* sy+1;
			float w = cInfo.bitmapWidth * sx;
			float h = cInfo.bitmapHeight * sy;

			/* Advance the cursor to the start of the next character */
			x += cInfo.advanceX;
			y += cInfo.advanceY;

			/* Skip glyphs that have no pixels */
			if (!w || !h)
				continue;

			GLfloat charTexWidth= static_cast<GLfloat>(cInfo.bitmapWidth) / w_;
	        GLfloat charTexHeight= static_cast<GLfloat>(cInfo.bitmapHeight) /h_;

			coords[ct++]={
				x2, -y2, 
				cInfo.textureX, 
				cInfo.textureY};

			coords[ct++]={
				x2 + w, -y2, 
				cInfo.textureX + charTexWidth, 
				cInfo.textureY};

			coords[ct++]={
				x2, -y2 - h, 
				cInfo.textureX, 
				cInfo.textureY + charTexHeight};

			coords[ct++]={
				x2 + w, -y2, 
				cInfo.textureX + charTexWidth, 
				cInfo.textureY};

			coords[ct++]={
				x2, 
				-y2 - h, 
				cInfo.textureX, 
				cInfo.textureY + charTexHeight};

			coords[ct++]={
				x2 + w, 
				-y2 - h, 
				cInfo.textureX + charTexWidth, 
				cInfo.textureY + charTexHeight};
 
		}

		//glBindVertexArray(vAO_.getIndex());
 
		glUseProgram(fontProgram_.program.getIndex()); 

		bindTexture();

		glBindBuffer(GL_ARRAY_BUFFER, vBO_.getIndex());

		glBufferData(GL_ARRAY_BUFFER, 
		             coords.size() * sizeof(point), 
		             coords.data(),
		             GL_DYNAMIC_DRAW);

		/* Set up the VBO for our vertex data */
		
		glVertexAttribPointer(fontProgram_.coordAttrLocation, 4, GL_FLOAT, GL_FALSE, 0,  nullptr);
		glEnableVertexAttribArray(fontProgram_.coordAttrLocation);


		/* Draw all the character on the screen in one go */
		//for orphaning we would need the size of the previous buffer. 
		//Storing the length could be implemented
		
		glDrawArrays(GL_TRIANGLES, 0, coords.size());
	    
	    // unbind the VBO and VAO
	    glBindBuffer(GL_ARRAY_BUFFER, 0);
	    glBindVertexArray(0);

	    return x; //return final x
	}

	int  FontAtlas::calculatePixelWidth(std::string const& text) {
        static auto converter=std::wstring_convert<std::codecvt_utf8<wchar_t>>{};
        return calculatePixelWidth(converter.from_bytes(text.data()));
    }


	int  FontAtlas::calculatePixelWidth(std::wstring const& text){

		if(charactersInfoPtr_== nullptr){
			throw std::runtime_error("character info is empty in Font Atlas. It may not have been intialised.");
		}

		int x=0;

		for(wchar_t p : text) { //wchar_t because auto makes it into char, which drops all non ascii characters

            if(p>=numOfChars_){
                //It does not exist in this font atlas, so ignore (don't render at all). Another possibility
            	//is to choose a default character, like space (ascii 32).
                continue;
            }
			/* Calculate the vertex and texture coordinates */

			auto cInfo= (*charactersInfoPtr_)[p];
			/* Advance the cursor to the start of the next character */
			x += cInfo.advanceX;
		}

		return x;
	}

	void FontAtlas::debugPrint() const{
		std::cout<<"All info in charsInfo:"<< std::endl;
		auto charsInfo=*charactersInfoPtr_;
        for (int i = 0; i < numOfChars_; i++){
			auto& info=charsInfo[i];
			std::cout<<"adX: "<< info.advanceX <<" adY: "<< info.advanceY
			<<" bW: "<< info.bitmapWidth <<" bH: "<< info.bitmapHeight
			<<" bL: "<< info.bitmapLeft <<" bT: "<< info.bitmapTop
			<<" tX: "<< info.textureX <<" tY: "<< info.textureY<<std::endl;
		}

	}

	
}