
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLFONTFONTLIBRARY_H_
#define GLFONTFONTLIBRARY_H_ 

#include <ft2build.h>
#include FT_FREETYPE_H

#include "Face.h"
#include <string>
#include <memory>
#include <type_traits>


namespace glfonts {

	//just used to create the resources needed when loading the font
	//one per thread is needed.

	class FreeTypeFontLibrary final
	{
	public:
		typedef  std::shared_ptr<std::remove_pointer<FT_Library>::type> FTLibraryPtr;

		FreeTypeFontLibrary();
		 ~FreeTypeFontLibrary();
		//FreeTypeFontLibrary( FreeTypeFontLibrary const&)=delete;
		//FreeTypeFontLibrary& operator = (FreeTypeFontLibrary const&)=delete;

		//Only for debugging!!!
		//FT_Library const& getFTLibrary(){return ftLibrary_;}

		Face makeFaceFromFile(std::string const& font_path);
		Face makeFaceFromName(std::string const& font_name);
		bool makeFace(std::string const& font_name, FT_Face& toCreate);

	private:
		FTLibraryPtr ftLibraryPtr_;


	};

}
#endif