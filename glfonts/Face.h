
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLFONTSFACE_H_
#define GLFONTSFACE_H_ 

#include <ft2build.h>
#include FT_FREETYPE_H

#include <iostream>
#include <memory>
#include <type_traits>
#include <stdexcept>

namespace glfonts {

	using FTLibraryPtr = std::shared_ptr<std::remove_pointer<FT_Library>::type>;


	class Face final
	{
		friend class FreeTypeFontLibrary; // only FreeTypeFontLibrary can call the constructor
	public:
		using FacePtr = std::shared_ptr<std::remove_pointer<FT_Face>::type>;

		Face()=delete;
		~Face();

		void setSize(int size)const; //it changes a state, but not any member...so const

		FT_Face const getFT_Face() const {return facePtr_.get();}

		//these are just to access some general metrics and information that may be useful in creating
		//guis or seting up text:

		int getAscender(){return facePtr_->size->metrics.ascender>>6;}
		int getDescender(){return facePtr_->size->metrics.descender>>6;}
		int getHeight(){return facePtr_->size->metrics.height>>6;}
		int getMaxAdvance(){return facePtr_->size->metrics.max_advance>>6;}
		

	private:
		Face(FT_Face fRawPtr,FTLibraryPtr libraryPtr);


	private:
		//FT_Face is a typedef to a FT_FaceRec pointer, but here we deduce type with remove pointer
		//The order is very important, as it will make sure that facePtr_ is destroyed 
		//before libraryPtr_ (in revers order as they are declared, and which is the order they are constructed)
		//otherwise FTLibrary will be destroyed first (if it is holding the last pointer to it) and then
		//when FacePtr attempts to destroy, it will give a segmentation fault. There is surelly a better idiom to
		//represent these dependencies...

		FTLibraryPtr libraryPtr_; //This makes sure that the actual FT_Library is not destroyed as long as any Face object still exists
		FacePtr facePtr_;
	};



}
#endif