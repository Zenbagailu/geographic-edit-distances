
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#ifndef GLFONTSFONTATLAS_H_	
#define GLFONTSFONTATLAS_H_	 


#include <GL/glew.h>

#include <ft2build.h>
#include FT_FREETYPE_H
#include <array>
#include <stdexcept>
#include <string>
#include <glsl/definitions.h>
#include <iostream>
#include <memory>
#include <vector>


 namespace glfonts{

 	class Face; 

 	typedef std::array<float,4> Color;

 	//just a convenient struct for keeping all program related data together...
 	struct FontProgram
	{
		//void setColor(Color color){glUniform4fv( colAttrLocation,1, color);}

		FontProgram(){}

		FontProgram(glsl::ProgramO fontProgram):program{fontProgram}{
			coordAttrLocation=glsl::getAttributeLocation(program, "coord");
			//coordAttrLocation=glGetAttribLocation(fontProgram.getIndex(),"coord");
        	colUniformLocation=glsl::getUniformLocation(program, "color");
         	textureUnitLocation=glsl::getUniformLocation(program,"textureUnit");
         	depthUniformLocation=glsl::getUniformLocation(program,"depth");
		}

        void setColor(Color color){
            glUseProgram(program.getIndex()); 
            glUniform4fv( colUniformLocation,1, color.data());
        }

        void setDepth(GLfloat depth){
        	glUseProgram(program.getIndex()); 
            glUniform1f(depthUniformLocation, depth);
        }

		glsl::ProgramO program;
		GLint colUniformLocation;
		GLint textureUnitLocation;
		GLint coordAttrLocation;
		GLint depthUniformLocation;
	};

 	class FontAtlas
 	{	
 	public:
 		struct charInfo{
			long advanceX;	// advance.x
			long advanceY;	// advance.y

			float bitmapWidth;	// bitmap.width;
			float bitmapHeight;	// bitmap.height;

			int bitmapLeft;	// bitmap_left;
			int bitmapTop;	// bitmap_top;

			float textureX;	// x offset of glyph in texture coordinates
			float textureY;	// y offset of glyph in texture coordinates
		}; 

 	public:

 		typedef std::vector<charInfo>					 CharactersInfoContainer;
 		typedef std::shared_ptr<CharactersInfoContainer> CharactersInfoArrayPtr;

 		FontAtlas(){};
 		~FontAtlas();

 		//the members are all either ints or smart pointers, so copying is cheap
 		FontAtlas& operator = (FontAtlas&& )=default; 
 		FontAtlas& operator = (FontAtlas const& )=default; 
 		FontAtlas(FontAtlas const& )=default;

 		FontAtlas(FontProgram fontProgram, Face const& face);
 		
 		//it returns the final x
        int renderText(std::string const& text, int x, int y, int scw, int sch);
        int renderText(std::wstring const& text, int x, int y, int scw, int sch);
 		void debugPrint() const;

 		 //These are useful to calculate sizes of labels for example, when sizing gui elements
 		int calculatePixelWidth(std::string const& text);
 		int calculatePixelWidth(std::wstring const& text);

 	private:
 		void pasteSubglyphsOnTexture(FT_Face const& face);
 		void findMinimumSizeOfTexture(FT_Face const& face);	
 		void bindTexture ()const;
		

	private:
		unsigned int w_;			// width of texture in pixels
		unsigned int h_;			// height of texture in pixels

		constexpr static size_t MAXWIDTH=1024;
        std::size_t numOfChars_;
		CharactersInfoArrayPtr charactersInfoPtr_; //this makes copying feasible.
		FontProgram fontProgram_;

		glsl::TextureO texture_;
		//location of the "sampler2D"  uniform in the shader, to be matched with the 
		glsl::VAO vAO_;
		glsl::VBOFloat vBO_;
		
	};

}


#endif