
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "Face.h"

namespace glfonts {
	
	Face::Face(FT_Face fRawPtr,FTLibraryPtr libraryPtr): //rawPtr...
	libraryPtr_(libraryPtr),
	facePtr_{fRawPtr, [](FT_Face fptr){ //create the shared pointer with a deleter
		if(  FT_Done_Face(fptr) ){
			//throwing exceptions in the destructor is not a good idea? just print an error then...
			std::cout << "failed cleaning a face in FacePtr's deleter"<<std::endl;
		}
	}}
	{} //empty body of constructor

	Face::~Face(){}

	void Face::setSize(int size)const{
		if(FT_Set_Pixel_Sizes(facePtr_.get(), 0, size)){ // pixel size font
			throw std::runtime_error("could not change size of font");
		}
	}
}