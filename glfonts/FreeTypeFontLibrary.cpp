
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "FreeTypeFontLibrary.h"
#include <stdexcept>

#if defined(__APPLE__) && defined(__MACH__)
#include <AGL/agl.h>
//#include <freetype/ftmac.h>
#include FT_MAC_H
#endif
#ifdef LINUX
#include <fontconfig/fontconfig.h>
#include <string.h>
#endif


namespace glfonts {


	FreeTypeFontLibrary::FreeTypeFontLibrary(){ 
		FT_Library rawFTLibraryPtr;
		if(FT_Init_FreeType(&rawFTLibraryPtr)) {
			throw std::runtime_error("Could not init freetype library");
		}

		ftLibraryPtr_={rawFTLibraryPtr,[](FT_Library libPtr){
			if(FT_Done_FreeType(libPtr)){
				std::cout << "failed deleting FreeTypeLibrary in FTLibraryPtr deleter"<<std::endl;
			}

			//std::cout << "FTLibraryPtr deleter called"<<std::endl;

		}};
	}

	FreeTypeFontLibrary::~FreeTypeFontLibrary(){
		//FT_Done_FreeType(ftLibrary_);
	}

    Face FreeTypeFontLibrary::makeFaceFromFile(std::string const& font_path){
        FT_Face rawFacePtr;
        FT_Long face_index;
        if(FT_New_Face(ftLibraryPtr_.get(),font_path.c_str(), face_index, &rawFacePtr)){
            throw std::runtime_error("Could not create FT_Face for file "+font_path);
        }

        return Face(rawFacePtr, ftLibraryPtr_);//it can access the private constructor because FreeTypeFontLibrary is a friend class
    }

    

	Face  FreeTypeFontLibrary::makeFaceFromName(std::string const& fontName){
		FT_Face rawFacePtr;
		if(makeFace(fontName,rawFacePtr)){
			throw std::runtime_error("Could not create FT_Face for "+fontName);
		}
		return Face(rawFacePtr, ftLibraryPtr_);//it can access the private constructor because FreeTypeFontLibrary is a friend class
	}


	bool FreeTypeFontLibrary::makeFace(std::string const& font_name,FT_Face& toCreate){ 
        
        std::string full_font_path;
        
        
#if defined(__APPLE__) && defined(__MACH__)
        char path[1024];
        FT_Long face_index;
        
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
        
        FT_Error r = FT_GetFilePath_From_Mac_ATS_Name( font_name.c_str(),
                                                      (UInt8 * )path,
                                                      1024,
                                                      &face_index );
        
        //std::cout<<"path: "<< path << "face index:"<< face_index << std::endl;
        
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
        
        if ( r != FT_Err_Ok ) {
            return false;
        }
        
        full_font_path = path;
        return (FT_New_Face(ftLibraryPtr_.get(),full_font_path.c_str(), face_index, &toCreate));
        
#endif
#ifdef WIN32
        std::cout<<"CALLING NOT TESTED WIN32 PART IN CREATEFACE!!!"<<std::endl;
        string font_display_name, font_file_name;
        if( !GetFontFile( font_name.c_str(), font_display_name, font_file_name ) ) {
            return false;
        }
        
        string path = string( getenv( "windir" ) ) + "\\fonts\\";
        full_font_path = path + font_file_name;
        return(FT_New_Face(ftLibraryPtr_.get(),full_font_path.c_str(), 0, &toCreate));
        
        
#endif
#ifdef LINUX
        std::cout<<"CALLING NOT TESTED LINUX PART IN CREATEFACE!!!"<<std::endl;
        full_font_path = FC_GetFontByName( font_name.c_str(), bold, italic );
        if( full_font_path == "" ){
            return false;
        }
        return(FT_New_Face(ftLibraryPtr_.get(),full_font_path.c_str(), 0, &toCreate));
#endif
        
         return true;
    }
	
}