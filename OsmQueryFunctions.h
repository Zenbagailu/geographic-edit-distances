
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef TESTOSMQUERYFUNCTIONS_H_
#define TESTOSMQUERYFUNCTIONS_H_ 
#include <string>
#include <iostream>
#include <fstream>

#include <array>
namespace test {

    struct BoundingCoords{
        enum {SOUTHLAT=0, WESTLONG=1, NORTHLAT=2, EASTLONG=3};
        double coords[4];

        BoundingCoords():coords{0,0,0,0}{}

        double& operator[](size_t indx){
            return coords[indx];
        }
        double operator[](size_t indx) const{
            return coords[indx];
        }

        void set(size_t indx, std::string const& sval){
        //std::cout<<sval<<std::endl;
            coords[indx] = std::stod(sval);
        }

        bool isSet(){
            for(auto coord: coords){
                if(coord!=0){
                    return true; 
                }
            }
            return false;  
        }

        friend std::ostream& operator<< (std::ostream& stream, const BoundingCoords& bbox){
            return stream 
            << "[ "
            << bbox.coords[SOUTHLAT] <<","
            << bbox.coords[WESTLONG] <<","
            << bbox.coords[NORTHLAT] <<","
            << bbox.coords[EASTLONG] 
            << " ]";
        }

    };


    std::string processQueryString(std::string const& queryString);
    std::string processQueryFile(std::string const& pathToQuery);
    size_t getQueryTypePos(std::string const& query, size_t pos=0);
    BoundingCoords getBoundingBoxFromSingleQuery(std::string const& query);
    BoundingCoords getBoundingBoxFromBoundingBoxQuery(std::string const& query);
    std::string removeBoundingBoxFromBoundingBoxQuery(std::string const& query); //this is for reusing the query for another area
    //rathern than passing BoundingCoords and the << operator, this makes sure the string is formatted correctly.
    std::string addBoundingBoxToQuery(std::string const& query, double south, double west, double north, double east);  

    template <class STREAM> std::string processQueryStream(STREAM& queryStream){
        std::string resultQuery;
        std::string line;

        while (std::getline(queryStream,line)) {
        //std::cout << line << std::endl;
            auto endString = line.find("//");
            if (endString  == std::string::npos) { //No comment found, consider whole line
                endString = line.length();
            }

            line=line.substr(0,endString);

        //remove duplicate spaces (but leave single ones)
        //from:
        //https://stackoverflow.com/questions/8362094/replace-multiple-spaces-with-one-space-in-a-string

            auto newEndIt = std::unique(line.begin(),
                                        line.end(),
                                        [](char lhs,char rhs)->bool{
                                            return (lhs == rhs) && (lhs == ' ');
                                        });
            line.erase(newEndIt,line.end());  

        //Now replace all spaces (single) with %20 (space character in html request)
        //Actually this should be done for all special characters...
            std::string toSubs(" ");
            std::string subs("%20"); 
            auto pos = line.find(toSubs);
            while( pos != std::string::npos){
                line.replace(pos, toSubs.size(), subs);
                pos =line.find(toSubs, pos + toSubs.size());
            }

            resultQuery=resultQuery + line;
        }  

        return resultQuery; 

    }

}
#endif