
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef TESTDRAWGRAPH_H_
#define TESTDRAWGRAPH_H_ 

#include <graph/allIn.h>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

namespace test {
	template<class Graph,class Renderer> 
	void
	drawGraph(Graph const& graph, Renderer& renderer){
		drawGraphNodes(graph, renderer);
		drawGraphEdges(graph, renderer);
	}

	template<class Graph,class Renderer>
	void
	drawGraphNodes(Graph const& graph, Renderer& renderer){
		renderer.begin(Renderer::DrawingMode::POINTS);
		for(auto vId: graph::allIn(boost::vertices(graph))){
			auto const& v=graph[vId];
			renderer.vertex(static_cast<float>(v[0]),static_cast<float>(v[1]));
		}
		renderer.end();
	}

	template<class Graph,class Renderer> 
	void
	drawGraphEdges(Graph const& graph, Renderer& renderer){
		renderer.begin(Renderer::DrawingMode::LINES);
		for(auto eId: graph::allIn(boost::edges(graph))){
			auto const& v=graph[boost::source(eId, graph)];
		 	auto const& u=graph[boost::target(eId, graph)];
		 	renderer.vertex(static_cast<float>(v[0]),static_cast<float>(v[1]));
		 	renderer.vertex(static_cast<float>(u[0]),static_cast<float>(u[1]));
		}
	 	renderer.end();
 	}

 	template<class Renderer> 
 	void 
 	drawGraphText(Renderer& renderer){
 	}
}
#endif