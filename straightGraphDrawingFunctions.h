
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef TESTSTRAIGHTGRAPHDRAWINGFUNCTION_H_
#define TESTSTRAIGHTGRAPHDRAWINGFUNCTION_H_ 

#include <random>
#include <straightPaths.h>
#include <graph/GlobalDepth.h>

namespace test {
    template< class Graph, class Renderer> 
    void 
    drawPathGraph(Graph const& graph,
                  typename PathGraphDefinitions<Graph>::PathGraphT const& pathGraph,
                  Renderer& renderer){

        static std::random_device rd;
        static std::mt19937 gen(rd());
        static std::uniform_real_distribution<float> cols(0, 1);

        renderer.setLineThickness(0.5f);

        float offset=0;
        for(auto pVId: graph::allIn(boost::vertices(pathGraph))){
            renderer.setColor(cols(gen),cols(gen),cols(gen));
            renderer.begin(Renderer::DrawingMode::LINE_STRIP);

            for(auto vId: pathGraph[pVId]){
                auto const& v=graph[vId];
                renderer.vertex(v[0],v[1],offset+=0.05);
            } 
            renderer.end();
        }
        //and edges...
        renderer.setPointSize({5.0});
        renderer.begin(Renderer::DrawingMode::POINTS);
        offset=0;
        for(auto pEId: graph::allIn(boost::edges(pathGraph))){
            auto const& v=graph[pathGraph[pEId]];
            renderer.vertex(v[0],v[1],offset+=0.05);
        }
        renderer.end();
    }

    template< class Graph, class Renderer> 
    void 
    drawSimplifiedGraph(Graph const& graph,
                        typename PathGraphDefinitions<Graph>::PathGraphT const& pathGraph,
                        Renderer& renderer){
        renderer.setLineThickness(0.5f);
        using Point = std::array<double,3>;
        std::vector<Point> positions(boost::num_vertices(pathGraph));

        float offset=0;
        for(auto pVId: graph::allIn(boost::vertices(pathGraph))){
            Point pt ={0,0,1};
            for(auto vId: pathGraph[pVId]){
                pt[0] += graph[vId][0];
                pt[1] += graph[vId][1]; 
            } 
            pt[0] /= pathGraph[pVId].size();
            pt[1] /= pathGraph[pVId].size();
            positions[pVId] = pt;
        }

        renderer.setPointSize({5.0});
        renderer.begin(Renderer::DrawingMode::POINTS);
        for(auto pVId: graph::allIn(boost::vertices(pathGraph))){
            renderer.vertex(positions[pVId]);
        }
        renderer.end();

        //and edges...
        renderer.setLineThickness(1.5f);
        renderer.begin(Renderer::DrawingMode::LINES);

        for(auto pEId: graph::allIn(boost::edges(pathGraph))){
            pathGraph[pEId];
            auto sId = boost::source(pEId, pathGraph);
            auto tId = boost::target(pEId, pathGraph);

            renderer.vertex(positions[sId]);
            renderer.vertex(positions[tId]);
        }
        renderer.end();
    }

    template< class Graph, class Depth, class Renderer> 
    void 
    drawPathGraphDepth(Graph const& graph,
                       typename PathGraphDefinitions<Graph>::PathGraphT const& pathGraph,
                       Depth const& depth,
                       Renderer& renderer, 
                       double zOffset = 0){

        using Vector = std::array<double,3>;

        graph::GetClosenessCentrality calculation;
        // //graph::GetMeanDepth calculation;
 
        //with closeness centrality the values need to be inverted (since the 
        //minimum global depth will produce the maximum closeness centrality
        //and viceversa)
        double max = calculation(depth.getMin(),depth.size());
        double min = calculation(depth.getMax(),depth.size());

        // std::cout<<"max: "<<max<<" min: "<<min<<std::endl;

        //auto const& baseGraph = *graphPtr_;
        double maxThick=7.5;
        double minThick=0.1;
         for(auto pVId: graph::allIn(boost::vertices(pathGraph))){

            double val=calculation(depth[pVId],depth.size());
            auto thickness = isinf(val) ? maxThick : minThick + ((maxThick-minThick)*(val-min)*(val-min)/((max-min)*(max-min)));
            auto const& vertices=pathGraph[pVId];
            auto nextIt = vertices.begin();
            auto prevIt = nextIt++;

            renderer.begin(Renderer::DrawingMode::TRIANGLE_STRIP);

            auto const& ptb0=graph[*prevIt];
            auto const& ptb1=graph[*nextIt];
            Vector prevdir={ptb1[0]-ptb0[0],ptb1[1]-ptb0[1]};
            auto size = std::sqrt(prevdir[0]*prevdir[0] + prevdir[1]*prevdir[1]);
            prevdir[0]/=size;
            prevdir[1]/=size;
            Vector perp = {-prevdir[1], prevdir[0]};
            perp[0] *= thickness;
            perp[1] *= thickness;
            //first two points
            renderer.vertex(ptb0[0] + perp[0],ptb0[1] + perp[1], zOffset);
            renderer.vertex(ptb0[0] - perp[0],ptb0[1] - perp[1], zOffset);

            auto last = vertices.end()-1; //don't do last vertex

            for(;nextIt != last; ++nextIt){

                auto const& pt0=graph[*prevIt];
                auto const& pt1=graph[*nextIt];
                Vector dir={pt1[0]-pt0[0],pt1[1]-pt0[1]};
                auto size = std::sqrt(dir[0]*dir[0] + dir[1]*dir[1]);
                dir[0]/=size;
                dir[1]/=size;
                Vector bisect = {-(prevdir[1]+dir[1])/2, (prevdir[0]+dir[0])/2};
                double cosAlfa = - dir[1] * bisect[0] + dir[0] * bisect[1];

                bisect[0] *= cosAlfa*thickness;
                bisect[1] *= cosAlfa*thickness;

                renderer.vertex(pt1[0] + bisect[0],pt1[1] + bisect[1], zOffset);
                renderer.vertex(pt1[0] - bisect[0],pt1[1] - bisect[1], zOffset);

                prevIt=nextIt;
                prevdir = dir;
            }

            //do last
            perp = {-prevdir[1], prevdir[0]};
            perp[0] *= thickness;
            perp[1] *= thickness;
            //last two points
            auto const& ptl=graph[*last];
            renderer.vertex(ptl[0] + perp[0],ptl[1] + perp[1], zOffset);
            renderer.vertex(ptl[0] - perp[0],ptl[1] - perp[1], zOffset);
            
            renderer.end();
        }
    }

}
#endif