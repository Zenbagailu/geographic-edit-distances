
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#ifndef GLAUXLINEDRAWER_H_
#define GLAUXLINEDRAWER_H_ 

//This (and MonochormeDrawer) should be solved with variadic templates. That would
//also make possible to expand it if we need normals... etc perhaps even StaticMeshWithTopology
//could be made into this? Policies in variadic templates? (so it would be possible to generate
//methods like bindColorsTo... specifically for every type...or functors?)

//#include <glsl/definitions.h>
#include "definitions.h"
#include "functions.h"

namespace glaux {

	//This is a generic drawer type that can be static, dynamic, streamed...
	//it has a minor unnecessary buffer orphaning if static, but it is a minor
	//detail that should not affect efficiency

	//none of the methods are const even if they don't realy modify 
	//the members it does affect the resources they refer to.
	
	class LineDrawer final
	{
	public:

	    LineDrawer(GLenum drawType_);
	    
		void set(void* ptBuffer, 
		    	 void* nPtBuffer, 
		    	 void* colBuffer, 
		    	 void* thickBuffer, 
		         void* dirBuffer, 
		    	 void* faceBuffer, 
		    	 size_t newVertexSize,
		    	 size_t newFaceSize);

		//Other constructors should follow that take other types besides raw pointers ot floats
		//also perhaps those with a stride.

		virtual ~LineDrawer(){};
		LineDrawer( LineDrawer const&)=default;
		LineDrawer& operator = (LineDrawer const&)=default;
		//the appropiate program (to which the vertices have been binded through 
		//bindVerticesTo) should be current before calling render.
		void render(GLenum mode); 

		void bindVerticesTo(GLint attributeLocation);
		void bindColorsTo(GLint attributeLocation);
		void bindOtherVerticesTo(GLint attributeLocation);
		void bindThicknessesTo(GLint attributeLocation);
		void bindDirectionTo(GLint attributeLocation);
	
	private:

		GLenum drawType_;
		VAO vAO_;
        VBOFloat verticesVBO_;
        VBOFloat otherVerVBO_;
        VBOFloat colorsVBO_;
        VBOFloat thicknessesVBO_;
        VBOFloat directionVBO_;
        glaux::VBOIndices faceIndicesVBO_;
        size_t   vertexSize_;
        size_t   faceSize_;
        
	};
}
#endif