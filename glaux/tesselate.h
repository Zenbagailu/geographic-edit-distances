
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLAUXTESSELATE_H_
#define GLAUXTESSELATE_H_ 

//this wraps up the glutesselator. Unfortunatelly there are no many alternative
//good codes or libraies for easily tesselating polygons.This uses glutess, the glu tesselation library. glutess needs to be included
//in the path. It can be downloaded from: https://github.com/CesiumGS/glutess

//pragmas so it does not do warnings about glu. Eventually the code will need 
//to be copied locally, rather than relying of being part of opengl distributions.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"


//GL/glew includes all necessary and correct OpenGL headers. 
//If not using glew, change this for the convinient headers
//OpenGL/gl.h in OS X, or GL/gl.h in other operating systems,
//or gl3.h.
#include <GL/glew.h> 

//GLU won't be included if emscripten and glew  are used, since glu os not part of  OpenGL ES
//that means that GLUTesselator will need to be included some other way.
// #ifdef EMSCRIPTEN
//#include <GL/glu.h>
// #endif
#include <glutess/glutess.h>

#include <memory>
#include <vector>
#include <array>

namespace glaux {

    template<class VertexProcessor>class Tesselator{


    public:
        ~Tesselator();
        Tesselator(bool trianglesOnly=true);
        static void beginCallback(GLenum which,  void *data );
        static void endCallback(void *data );
        static void errorCallback(GLenum errorCode, void *data );
        static void vertexCallback(GLvoid *vertex, void *data );
        static void edgeFlagCallback(GLboolean);

        void beginPolygon(VertexProcessor& vProcessor);
        void endPolygon();
        void beginContour();
        void endContour();
        template<class VT> void vertex(VT const& v);

    private:
        GLUtesselator* tessObjPtr;

        //internal copies are needed because the tesselator uses them... see page 
        //482 of redbook, chapter eleven or: http://www.glprogramming.com/red/chapter11.html
        //"Polygon Definition." if the data exists independently (if the point type of polygon 
        //passed consists of arrays of doubles, (for example std::array<double,3>) the copying
        //is unnecesary. The vertex function can then be specialised
        std::vector< std::array<double,3> > vertices;
        std::vector< size_t> polygonIndx;
    };

    template<class VertexProcessor>
    Tesselator<VertexProcessor>::~Tesselator(){ 
        gluDeleteTess(tessObjPtr);
    }

    template<class VertexProcessor>
    Tesselator<VertexProcessor>::Tesselator(bool trianglesOnly){
        tessObjPtr = gluNewTess();
        //DATA versions
        gluTessCallback(tessObjPtr, GLU_TESS_VERTEX_DATA,  reinterpret_cast<GLvoid(*)()>(&vertexCallback));
        gluTessCallback(tessObjPtr, GLU_TESS_BEGIN_DATA,   reinterpret_cast<GLvoid(*)()>(&beginCallback));
        gluTessCallback(tessObjPtr, GLU_TESS_END_DATA,     reinterpret_cast<GLvoid(*)()>(&endCallback));
        gluTessCallback(tessObjPtr, GLU_TESS_ERROR_DATA,   reinterpret_cast<GLvoid(*)()>(&errorCallback));
        if(trianglesOnly){
            //This is for forcing triangles, see GLU_TESS_EDGE_FLAG in
            // https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluTessCallback.xml
            gluTessCallback(tessObjPtr, GLU_TESS_EDGE_FLAG, reinterpret_cast<GLvoid(*)()>(&edgeFlagCallback));
        } 
        //Not implemented in this case. For examples check:  http://www.glprogramming.com/red/chapter11.html
        // gluTessCallback(tobj, GLU_TESS_COMBINE_DATA, reinterpret_cast<GLvoid(*)()>(&combineCallback));
    }

    //the only meaning of this is to force triangles... see explanation of GLU_TESS_EDGE_FLAG in
    // https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluTessCallback.xml 
    template<class VertexProcessor>
    void 
    Tesselator<VertexProcessor>::edgeFlagCallback(GLboolean) {} 

    template<class VertexProcessor>
    void 
    Tesselator<VertexProcessor>::beginCallback(GLenum which,  void *data ){
       VertexProcessor& vProcessor = *reinterpret_cast<VertexProcessor*>(data);
       vProcessor.begin(static_cast<typename VertexProcessor::DrawingMode>(which));
    }

    template<class VertexProcessor>
    void 
    Tesselator<VertexProcessor>::endCallback(void *data ){
        VertexProcessor& vProcessor = *reinterpret_cast<VertexProcessor*>(data);
        vProcessor.end();
    }

    template<class VertexProcessor>
    void 
    Tesselator<VertexProcessor>::errorCallback(GLenum errorCode, void *data ){
       throw std::runtime_error("error in tesselation: " + std::string{reinterpret_cast<const char*>(gluErrorString(errorCode))} );
    }

    template<class VertexProcessor>
    void 
    Tesselator<VertexProcessor>::vertexCallback(GLvoid *vertex, void *data ){
        //example of additional data in the vertex, such as normals or colors..
       // const GLdouble *pointer;
       // pointer = (GLdouble *) vertex;
       // glColor3dv(pointer+3);
       // glVertex3dv(vertex);
       
       VertexProcessor& vProcessor = *reinterpret_cast<VertexProcessor*>(data);
       vProcessor.vertex(reinterpret_cast<GLdouble*>(vertex));    
    }
    
    template<class VertexProcessor>
    void 
    Tesselator<VertexProcessor>::beginPolygon(VertexProcessor& vProcessor){
       gluTessBeginPolygon(tessObjPtr,&vProcessor);
        //gluTessProperty() could be called to change the default winding rules
        //("GLU_TESS_WINDING_RULE"),set the tesselator to "GLU_TESS_BOUNDARY_ONLY" 
        //or set the tolerance for the combine callback if used
        //The defaults are used in this case (winding rule "GLU_TESS_WINDING_ODD")
    }

    template<class VertexProcessor>
    void 
    Tesselator<VertexProcessor>::endPolygon(){
        //Now we know that all data is in memory (in "vertices"), so we actually process
        //the tesselation and make all calls to the tesselator
        
        size_t prev=0;
        for(auto indX:polygonIndx){
            gluTessBeginContour(tessObjPtr);
            for(auto i=prev; i<indX; ++i ){
                gluTessVertex(tessObjPtr, vertices[i].data(),vertices[i].data());
            }
            gluTessEndContour(tessObjPtr);
            prev=indX;
        }
        
        gluTessEndPolygon(tessObjPtr);
        vertices.clear();
        polygonIndx.clear();
    }

    template<class VertexProcessor>
    void 
    Tesselator<VertexProcessor>::beginContour(){          
    }

    template<class VertexProcessor>
    void 
    Tesselator<VertexProcessor>::endContour(){
        polygonIndx.push_back(vertices.size());
    }

    template<class VertexProcessor>
    template<class VT> 
    void 
    Tesselator<VertexProcessor>::vertex(VT const& v){
        vertices.push_back({v[0],v[1],v[2]});
    }
    

    //single polygon
    template< class VertexProcessor,class Polygon>
    bool
    tesselatePolygon(VertexProcessor& vertexProcessor, Polygon const& polygon){

        //create tesselator
        static Tesselator<VertexProcessor> tesselator;
        tesselator.beginPolygon(vertexProcessor);
        tesselator.beginContour();
       

        for(auto& v:polygon){
            tesselator.vertex(v);
        }

        tesselator.endContour();
        tesselator.endPolygon();

        return true;
    }

    template< class VertexProcessor,class Polygons>
    bool
    tesselatePolygons(VertexProcessor& vertexProcessor, Polygons const& polygons){

        //create tesselator
        static Tesselator<VertexProcessor> tesselator;
        tesselator.beginPolygon(vertexProcessor);
        for(auto& poly:polygons){
            tesselator.beginContour();
            for(auto& v:poly){
                tesselator.vertex(v);
            }
            tesselator.endContour();
        }
 
        tesselator.endPolygon();

        return true;
    }

}
#endif