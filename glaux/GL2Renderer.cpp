
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#include "GL2Renderer.h"
#include <aux/utils.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>

namespace glaux {

    void BasicRendererData::draw(DrawMode& drawingMode){ 
        size_t fcIndex=0;
        for(auto const& event: events_){
            
            //first draw with states set before event
            if(fcIndex<event.second){
                drawer_.render(drawingMode,fcIndex,event.second);
                fcIndex=event.second;
            }
            //then change settings
            event.first();
        }
        drawer_.render(drawingMode, fcIndex, vertices_.size());
    }

    void  BasicRendererData::vertex(Point const& pt){
        vertices_.push_back(pt);
    }

    void BasicRendererData::clear(){
        vertices_.clear();
        colors_.clear();
        events_.clear();
    }


    void LineRendererData::draw(){
        drawer_.render(GL_TRIANGLES);
    }


	GL2Renderer::GL2Renderer(GLenum renderType):
    renderType_{renderType},
    pointRenderData_{renderType},
    facesRenderData_{renderType},
    currentFontTransMatrix_(1.0),
    modeSet_{false},
    currentColor_{0.0f,0.0f,0.0f,1.0f},
    currentLineThickness_{1.0f},
    numberLineData_{1},
    currentLineData_{0}
	{
        lineRendersData_.emplace_back(renderType);

	    programFaces_=glsl::ProgramO{
          glsl::ShaderO{GL_VERTEX_SHADER, aux::getStringFromFile("shaders/ColorVS.glsl")}, 
          glsl::ShaderO{GL_FRAGMENT_SHADER, aux::getStringFromFile("shaders/ColorFS.glsl")}

	    };

	    programPoints_=glsl::ProgramO{
	      //glsl::ShaderO{GL_VERTEX_SHADER, aux::getStringFromFile("shaders/ColorVS.glsl")}, 
          glsl::ShaderO{GL_VERTEX_SHADER, aux::getStringFromFile("shaders/CircularPointsVS.glsl")}, 
	      glsl::ShaderO{GL_FRAGMENT_SHADER, aux::getStringFromFile("shaders/CircularPointsFS.glsl")}
	    };

         programLines_=glsl::ProgramO{
            glsl::ShaderO{GL_VERTEX_SHADER, aux::getStringFromFile("shaders/LinesVS.glsl")}, 
            glsl::ShaderO{GL_FRAGMENT_SHADER, aux::getStringFromFile("shaders/LinesFS.glsl")}
          
        };

	}
	GL2Renderer::~GL2Renderer(){}

	void GL2Renderer::clear(){
        pointRenderData_.clear();
        facesRenderData_.clear();
		fontEvents_.clear();

        for(int i=0; i<numberLineData_; ++i){
            lineRendersData_[i].clear();
        }
        numberLineData_=1; //there is always 1, at least
        currentLineData_=0;
        lineEvents_.clear();

        currentFontTransMatrix_=glm::mat4(1.0); //needs to be reset

	}

	void GL2Renderer::set(){
        for (int i = 0; i < numberLineData_; ++i){
            lineRendersData_[i].set();
        }
        //lineRenderData_.set();
        pointRenderData_.set();
        facesRenderData_.set();
	}

    void GL2Renderer::vertex(glaux::Point const& pt){
        if(!modeSet_){
            return;
        }

        switch (currentMode_){

            case Mode::POINTS:
            pointRenderData_.vertex(pt);
            pointRenderData_.color(currentColor_);
            break;

            case Mode::LINES:
            case Mode::LINE_STRIP://addEventTo set Polyline mode 
            case Mode::LINE_LOOP:
            lineRendersData_[currentLineData_].thickness(currentLineThickness_);
            lineRendersData_[currentLineData_].color(currentColor_);
            lineRendersData_[currentLineData_].vertex(pt);
            break;

            case Mode::TRIANGLES:
            case Mode::TRIANGLE_STRIP:
            case Mode::TRIANGLE_FAN:
            case Mode::QUADS:
            case Mode::QUAD_STRIP:
            facesRenderData_.vertex(pt);
            facesRenderData_.color(currentColor_);
            break;

            default:
            throw std::runtime_error("Trying to add a vertex without specifying mode with begin()" );
        }
    }


	void GL2Renderer::setPointSize(float ptSize){
        pointRenderData_.insertEvent(
            [ptSize](){
                glUniform1f(glsl::getUniformLocationFromCurrentProgram("pointSize"), ptSize);
            }); 
	}

    void GL2Renderer::setColor(float r,float g,float b, float a){
        currentColor_={r,g,b,a}; 
    }


	void GL2Renderer::setLineThickness(float thickness){
        //it is always possible with current line renderer
        currentLineThickness_=thickness;
	}

	void GL2Renderer::setTransformation(glm::mat4 const& trans){

        if(modeSet_){
            throw std::runtime_error("Calling setTransformation() between calls to begin()/end() is not allowed in GL2Renderer." ); 
        }

        auto transformation=[trans, this](){
            auto modelMat = matrixInfo_.getModelMatrix() * trans;
            glUniformMatrix4fv(matrixInfo_.getModelLocation(), 1, GL_FALSE, glm::value_ptr(modelMat));
        };

        pointRenderData_.insertEvent(transformation);
        facesRenderData_.insertEvent(transformation);

        //The event needs to:
        // have an index to the associated lineData. 
        //for rendering it would be necessary to go through all events so they can be executed in relation to their correspondoing data
        //execute the event...

        lineEvents_.push_back({transformation,numberLineData_});

		fontEvents_.push_back(
		[trans, this](){
			currentFontTransMatrix_ = trans;
		});
	}


	void GL2Renderer::setFont(glfonts::FontAtlas fontAtlas){
		fontEvents_.push_back(
			[fontAtlas,this](){
				currentFontAtlas_= fontAtlas;
			});	
	}


    void GL2Renderer::renderTextInPos(float x, float y, float z, std::string const& text,float xOff,float yOff){
        fontEvents_.push_back(
            [x,y,z,text,xOff,yOff,this](){

                //we can use the matrixInfo set for rendering any of the VAOs before, since 
                //this is not using any of the locations in the programs of the matrices...

                glm::vec3 screenCoord = glm::project(glm::vec3{x, y, z}, 
                                                currentFontTransMatrix_* matrixInfo_.getModelMatrix(), 
                                                matrixInfo_.getProjMatrix(), 
                                                glm::vec4{0,0, matrixInfo_.getWidth(),matrixInfo_.getHeight()});

                currentFontAtlas_.renderText(text, 
                                             screenCoord.x+xOff, 
                                             screenCoord.y+yOff, 
                                             matrixInfo_.getWidth(), 
                                             matrixInfo_.getHeight()); 
        });
    }

	void GL2Renderer::renderTextRelative(float xOff,float yOff,std::string const& text){
		fontEvents_.push_back(
			[xOff,yOff,text,this](){

				//we can use the matrixInfo set for rendering any of the VAOs before, since 
				//this is not using any of the locations in the programs of the matrices...

				glm::vec3 screenCoord = glm::project(glm::vec3{0, 0, 0}, 
				                                currentFontTransMatrix_* matrixInfo_.getModelMatrix(), 
				                                matrixInfo_.getProjMatrix(), 
				                                glm::vec4{0,0, matrixInfo_.getWidth(),matrixInfo_.getHeight()});


				currentFontAtlas_.renderText(text, 
                                             screenCoord[0]+xOff, 
                                             screenCoord[1]+yOff, 
                                             matrixInfo_.getWidth(), 
                                             matrixInfo_.getHeight()); 
			});
	}



	void GL2Renderer::renderText(float x,float y,std::string const& text){
		fontEvents_.push_back(
			[x,y,text,this](){
				//we can use the matrixInfo set for rendering any of the VAOs before, since 
				//this is not using any of the locations in the programs of the matrices...
				auto res = currentFontAtlas_.renderText(text, x, y, matrixInfo_.getWidth(), matrixInfo_.getHeight()); 
			});


	}

    void GL2Renderer::vertex(float x, float y, float z){
        vertex(Point{x,y,z});
    }

    void GL2Renderer::begin(Mode mode){

        if(modeSet_){
            throw std::runtime_error("Calling begin() between other calls to begin()/end() is not allowed." ); 
        }

        currentMode_= mode; //necessary when adding vertices
        switch (currentMode_){
            case Mode::POINTS:
            modeSet_=true;
            break;

            case Mode::LINES:
            case Mode::LINE_STRIP://addEventTo set Polyline mode 
            case Mode::LINE_LOOP:

            //if there have been events added since last draw
            if(lineEvents_.size()>0 && lineEvents_.back().second > currentLineData_){
                ++numberLineData_;
                ++currentLineData_;
                if(lineRendersData_.size()<=numberLineData_){ //if there was no lineRenderData...
                     lineRendersData_.emplace_back(renderType_);
                }      
            }
            lineRendersData_[currentLineData_].begin(mode);
            
            modeSet_=true;
            break;

            case Mode::TRIANGLES:
            case Mode::TRIANGLE_STRIP:
            case Mode::TRIANGLE_FAN:
            case Mode::QUADS:
            case Mode::QUAD_STRIP:
            facesRenderData_.insertEvent([this,mode](){
                drawingMode_ = static_cast<DrawMode> (mode);
            });
            modeSet_=true;
            break;
        }
    }

    void GL2Renderer::end(){

        if(!modeSet_){
            throw std::runtime_error("Calling end() before a previous calls to begin() is not allowed." ); 
        }

        switch (currentMode_){
            case Mode::POINTS:
            break;

            case Mode::LINES:
            case Mode::LINE_STRIP:
            case Mode::LINE_LOOP:
            lineRendersData_[currentLineData_].end();
            break;

            //Not implemented yet
            case Mode::TRIANGLES:
            case Mode::TRIANGLE_STRIP:
            case Mode::TRIANGLE_FAN:
            case Mode::QUADS:
            case Mode::QUAD_STRIP:

            facesRenderData_.insertEvent([](){
                    //empty event... it only needs to send the indices of the vertices of the faces,
                    //drawn with the specific mode so nothing needs to be done. The problem with this
                    //is that every begin() and end() of faces will produce an unnecesary event if the same
                    //mode is used all the time. 
            });
            break;
        }
        modeSet_=false;
    }

    Point GL2Renderer::unproject(double x, double y, double z){
         glm::vec3 screenCoord = glm::project(
            glm::vec3{x, y, z},
            matrixInfo_.getModelMatrix(),
            matrixInfo_.getProjMatrix(),
            glm::vec4{0,0, matrixInfo_.getWidth(),matrixInfo_.getHeight()});
         return Point{screenCoord.x,screenCoord.y,screenCoord.z};

    }



}