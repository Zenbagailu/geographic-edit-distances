
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "OriginDrawer.h"
#include <aux/utils.h>


namespace glaux {

		OriginDrawer::OriginDrawer():axes_{GL_STATIC_DRAW}{

		colorProgramO_=glsl::ProgramO{
                glsl::ShaderO{GL_VERTEX_SHADER, aux::getStringFromFile("shaders/ColorVS.glsl")}, 
                glsl::ShaderO{GL_FRAGMENT_SHADER, aux::getStringFromFile("shaders/ColorFS.glsl")}
        };

        
        glUseProgram(colorProgramO_.getIndex()); //TEST 2019

        std::vector<Point> vertices;
		std::vector<Color> colors;

		vertices.push_back({-1,0,0});
		colors.push_back({0,0,0,1});
		vertices.push_back({1,0,0});
		colors.push_back({1,1,0,1});

		vertices.push_back({0,-1,0});
		colors.push_back({0,0,0,1});
		vertices.push_back({0,1,0});
		colors.push_back({1,1,0,1});

		vertices.push_back({0,0,-1});
		colors.push_back({0,0,0,1});
		vertices.push_back({0,0,1});
		colors.push_back({1,1,0,1});

		axes_.set(vertices.data(), colors.data(), vertices.size());

        //Bind with program
        axes_.bindVerticesTo(glGetAttribLocation(colorProgramO_.getIndex(),"position"));
        axes_.bindColorsTo(glGetAttribLocation(colorProgramO_.getIndex(),"color"));

	}
}