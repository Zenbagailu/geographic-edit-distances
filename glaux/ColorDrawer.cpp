
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#include "ColorDrawer.h"


namespace glaux {
	
	ColorDrawer::ColorDrawer(GLenum drawType): drawType_{drawType},size_{0}{
	
    }

    void ColorDrawer::set(void* ptBuffer, void* colBuffer, size_t newSize){

		glBindVertexArray(vAO_.getIndex());
	    //of course orphaning is unnecesary the first time we do it, and
	    //then if rendered using GL_STATIC_DRAW and only calling this function once,
	    //but the alternatives (a condition that then it will be checked every time it is
	    //drawn dynamically), or a different class or function, seem overkill.

	    glBindBuffer(GL_ARRAY_BUFFER, verticesVBO_.getIndex());  //need to bind for glBufferData
	    
	    //"orphaning" the buffer, in case it was previously set.
	    //the only reason for the if statement is to prevent GLSL to produce an GL_INVALID_VALUE
	    //error, which will be ignored (it will have no effect), but it may make difficult debugging errors 
	    if(size_>0){ 
			glBufferData(GL_ARRAY_BUFFER, 
			             size_* PointByteSize,
			             nullptr,
			             drawType_);
		}
	    //and send the stuff now...
	    //if they are empty 
	    if( newSize>0){
	    	glBufferData(GL_ARRAY_BUFFER,
		             newSize * PointByteSize, 
		             ptBuffer,
		             drawType_);
	    }
		
	    glBindBuffer(GL_ARRAY_BUFFER, colorsVBO_.getIndex());  //need to bind for glBufferData

	    //"orphaning" the buffer

	    //same reason for the if statment as above.
	    if(size_>0){
		    glBufferData(GL_ARRAY_BUFFER, 
		                 size_ * ColorByteSize, 
		                 nullptr,
		                 drawType_);
		}
	    //and send the stuff now. 
	    if( newSize>0){
		    glBufferData(GL_ARRAY_BUFFER, 
		                 newSize * ColorByteSize,
		                 colBuffer,
		                 drawType_);
		}

		 // unbind the VBO and VAO
    	glBindBuffer(GL_ARRAY_BUFFER, 0);
    	glBindVertexArray(0);

		size_= newSize;

     }

     void ColorDrawer::render(GLenum mode, size_t begin, size_t end){
	 	glBindVertexArray(vAO_.getIndex());
        glDrawArrays(mode,begin,end-begin);
        glBindVertexArray(0);
 	}

    void ColorDrawer::render(GLenum mode){
	 	glBindVertexArray(vAO_.getIndex());
		glDrawArrays(mode,0,size_);
		glBindVertexArray(0);
 	}

 	void ColorDrawer::bindVerticesTo(GLint attributeLocation){
 		glBindVertexArray(vAO_.getIndex()); //the VAO needs to be bound...
 		glBindBuffer(GL_ARRAY_BUFFER, verticesVBO_.getIndex());  //need to bind for glBufferData
        glVertexAttribPointer(attributeLocation, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(attributeLocation);
        glBindVertexArray(0);
 	}

 	void ColorDrawer::bindColorsTo(GLint attributeLocation){
 		glBindVertexArray(vAO_.getIndex()); //the VAO needs to be bound...
 		glBindBuffer(GL_ARRAY_BUFFER, colorsVBO_.getIndex());  //need to bind for glBufferData
        glVertexAttribPointer(attributeLocation, 4, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(attributeLocation);
        glBindVertexArray(0);
 	}

}