
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#include "LineDrawer.h"

#include <stdexcept>

#include <iostream>


namespace glaux {
	
	LineDrawer::LineDrawer(GLenum drawType): drawType_{drawType},vertexSize_{0},faceSize_{0}{
	
    }

    void LineDrawer::set(
    	void* ptBuffer, 
    	void* nPtBuffer, 
    	void* colBuffer, 
    	void* thickBuffer, 
        void* dirBuffer, 
    	void* faceBuffer, 
    	size_t newVertexSize,
    	size_t newFaceSize){

		glBindVertexArray(vAO_.getIndex());
	    //of course orphaning is unnecesary the first time we do it, and
	    //then if rendered using GL_STATIC_DRAW and only calling this function once,
	    //but the alternatives (a condition that then it will be checked every time it is
	    //drawn dynamically), or a different class or function, seem overkill.

	    glBindBuffer(GL_ARRAY_BUFFER, verticesVBO_.getIndex());  //need to bind for glBufferData
	    
	    //"orphaning" the buffer, in case it was previously set.
	    //the only reason for the if statement is to prevent GLSL to produce an GL_INVALID_VALUE
	    //error, which will be ignored (it will have no effect), but it may make difficult debugging errors 
	    if(vertexSize_>0){ 
			glBufferData(GL_ARRAY_BUFFER, 
			             vertexSize_* PointByteSize,
			             nullptr,
			             drawType_);
		}
	    //and send the stuff now...
	    //if they are empty 
	    if( newVertexSize>0){
	    	glBufferData(GL_ARRAY_BUFFER,
		             newVertexSize * PointByteSize, 
		             ptBuffer,
		             drawType_);
	    }
		
	    glBindBuffer(GL_ARRAY_BUFFER, colorsVBO_.getIndex());  //need to bind for glBufferData
	    //"orphaning" the buffer
	    //same reason for the if statment as above.
	    if(vertexSize_>0){
		    glBufferData(GL_ARRAY_BUFFER, 
		                 vertexSize_ * ColorByteSize, 
		                 nullptr,
		                 drawType_);
		}
	    //and send the stuff now. 
	    if( newVertexSize>0){
		    glBufferData(GL_ARRAY_BUFFER, 
		                 newVertexSize * ColorByteSize,
		                 colBuffer,
		                 drawType_);
		}

		//Next points
        glBindBuffer(GL_ARRAY_BUFFER, otherVerVBO_.getIndex());  //need to bind for glBufferData

        if(vertexSize_>0){
         	glBufferData(GL_ARRAY_BUFFER,
         				 vertexSize_ * PointByteSize,
            			 nullptr,
            			 drawType_);
        }

        if(newVertexSize>0){
         	glBufferData(GL_ARRAY_BUFFER,
            			 newVertexSize * PointByteSize,
            			 nPtBuffer,
            			 drawType_);
         }

        //Thicknesses
        glBindBuffer(GL_ARRAY_BUFFER, thicknessesVBO_.getIndex());  //need to bind for glBufferData

        if(vertexSize_>0){
         	glBufferData(GL_ARRAY_BUFFER,
         				 vertexSize_ * sizeof(float),
            			 nullptr,
            			 drawType_);
        }

        if(newVertexSize>0){
         	glBufferData(GL_ARRAY_BUFFER,
            			 newVertexSize * sizeof(float),
            			 thickBuffer,
            			 drawType_);
        }

        //Directions
        glBindBuffer(GL_ARRAY_BUFFER, directionVBO_.getIndex()); 

        if(vertexSize_>0){
            glBufferData(GL_ARRAY_BUFFER,
                         vertexSize_ * sizeof(float),
                         nullptr,
                         drawType_);
        }

        if(newVertexSize>0){
            glBufferData(GL_ARRAY_BUFFER,
                         newVertexSize * sizeof(float),
                         dirBuffer,
                         drawType_);
        }

        //And the faces
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, faceIndicesVBO_.getIndex());

        if(faceSize_>0){
         	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
         				 faceSize_ * sizeof(Face),
            			 nullptr,
            			 drawType_);
        }

        if(newFaceSize>0){
         	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
            			 newFaceSize * sizeof(Face),
            			 faceBuffer,
            			 drawType_);
        }

		// unbind the VBOs and VAO
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    	glBindBuffer(GL_ARRAY_BUFFER, 0);
    	glBindVertexArray(0);
		vertexSize_= newVertexSize;
		faceSize_= newFaceSize;
     }

     void LineDrawer::render(GLenum mode){
     	if(mode != GL_TRIANGLES && mode != GL_TRIANGLE_STRIP){
     		throw std::runtime_error(std::string("Can't render in LineDrawer except with GL_TRIANGLES or GL_TRIANGLE_STRIP"));
     	}
     	
        glEnable(GL_DEPTH_TEST);
        glDepthMask(GL_FALSE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glBindVertexArray(vAO_.getIndex());
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, faceIndicesVBO_.getIndex()); //necessary to bind the elements
        glDrawElements(mode, faceSize_*3, GL_UNSIGNED_INT, nullptr);
        glBindVertexArray(0);

        glDepthMask(GL_TRUE);
        glDisable(GL_BLEND);
 	}

 	void LineDrawer::bindVerticesTo(GLint attributeLocation){
 		glBindVertexArray(vAO_.getIndex()); //the VAO needs to be bound...
 		glBindBuffer(GL_ARRAY_BUFFER, verticesVBO_.getIndex());  //need to bind for glBufferData
        glVertexAttribPointer(attributeLocation, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(attributeLocation);
        glBindVertexArray(0);
 	}

 	void LineDrawer::bindColorsTo(GLint attributeLocation){
 		glBindVertexArray(vAO_.getIndex()); //the VAO needs to be bound...
 		glBindBuffer(GL_ARRAY_BUFFER, colorsVBO_.getIndex());  //need to bind for glBufferData
        glVertexAttribPointer(attributeLocation, 4, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(attributeLocation);
        glBindVertexArray(0);
 	}

	void LineDrawer::bindOtherVerticesTo(GLint attributeLocation){
		glBindVertexArray(vAO_.getIndex()); //the VAO needs to be bound...
		glBindBuffer(GL_ARRAY_BUFFER, otherVerVBO_.getIndex());  //need to bind for glBufferData
        glEnableVertexAttribArray(attributeLocation);
        glVertexAttribPointer(attributeLocation, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glBindVertexArray(0);
	}
	void LineDrawer::bindThicknessesTo(GLint attributeLocation){
		glBindVertexArray(vAO_.getIndex()); //the VAO needs to be bound...
        glBindBuffer(GL_ARRAY_BUFFER, thicknessesVBO_.getIndex());  //need to bind for glBufferData
        glEnableVertexAttribArray(attributeLocation);
        glVertexAttribPointer(attributeLocation, 1, GL_FLOAT, GL_FALSE, 0, NULL);
        glBindVertexArray(0);
	}

        void LineDrawer::bindDirectionTo(GLint attributeLocation){
        glBindVertexArray(vAO_.getIndex()); //the VAO needs to be bound...
        glBindBuffer(GL_ARRAY_BUFFER, directionVBO_.getIndex());  //need to bind for glBufferData
        glEnableVertexAttribArray(attributeLocation);
        glVertexAttribPointer(attributeLocation, 1, GL_FLOAT, GL_FALSE, 0, NULL);
        glBindVertexArray(0);
    }


}