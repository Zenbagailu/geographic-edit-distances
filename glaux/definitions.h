
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLAUXDEFINITIONS_H_
#define GLAUXDEFINITIONS_H_ 

#include <glsl/definitions.h>
#include <array>

#include <views/Perspective.h>
#include <views/Ortho.h>

#include <GL/glew.h>

namespace glaux {

	constexpr static size_t PointSize=3;
	constexpr static size_t VertexSize=PointSize;
	constexpr static size_t VectorSize=PointSize;

	constexpr static size_t ColorSize=4;

	constexpr static size_t EdgeSize=2;
	constexpr static size_t FaceSize=3;
	constexpr static size_t AdjacencyFaceSize=6;


	using PointValueType 	= GLfloat;
	using VectorValueType 	= PointValueType;
	using ColorValueType 	= GLfloat;

	constexpr static size_t PointByteSize=PointSize * sizeof(PointValueType);
	constexpr static size_t ColorByteSize=ColorSize * sizeof(ColorValueType);

	using Point 	= std::array<PointValueType,PointSize>;
	using Vector 	= Point;
	using Vertex 	= Point;
	using Color 	= std::array<ColorValueType,ColorSize>;

	using Face 			= std::array<GLuint,FaceSize>;
	using AdjacencyFace = std::array<GLuint,AdjacencyFaceSize> ;
	using Edge 			= std::array<GLuint,EdgeSize> ;

	//not necessary (VBOFloat does the same)
	//try to remove them and test (they are only used in StaticMeshWithTopology)
	using VBOPoint 	= glsl::OglObject<glsl::VertexBufferObject<Point>>;
	using VBOVector = VBOPoint;
	// using glsl::OglObject<glsl::VertexBufferObject<Color>> VBOColor;
	
	using VAO 			= glsl::OglObject<glsl::VertexArrayObject>;
	using VBOFloat 		= glsl::OglObject<glsl::VertexBufferObject<GLfloat>>;
	using VBOIndices 	= glsl::OglObject<glsl::VertexBufferObject<GLuint>>;

}
#endif