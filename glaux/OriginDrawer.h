
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLAUXORIGINDRAWER_H_
#define GLAUXORIGINDRAWER_H_ 

#include "ColorDrawer.h"
#include <glsl/Program.h>
#include "definitions.h"
#include <vector>

#include "functions.h"
#include <iostream>

//It integrates everything (program...etc) so it is easy
//to include for tests, etc.

namespace glaux {
	class OriginDrawer final
	{
	public:
		OriginDrawer();
		~OriginDrawer(){}
		OriginDrawer( OriginDrawer const&)=delete;
		OriginDrawer& operator = (OriginDrawer const&)=delete;

		template<typename V> void render(V const& view);

	private:
		ColorDrawer axes_;
		glsl::ProgramO colorProgramO_;
	
	};

	template<typename V>
	void 
	OriginDrawer::render(V const& view){

		checkAndPrintOpenGLError();
		glUseProgram(colorProgramO_.getIndex()); //render as we want
        applyViewToCurrentProgram(view);
        //setLineAntialias();
        glLineWidth(0.75f);
        axes_.render(GL_LINES);
	}
}
#endif