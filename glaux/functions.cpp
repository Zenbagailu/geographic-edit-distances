
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "functions.h"

#include <GL/glew.h>

#include <stdexcept>
#include <iostream>

namespace glaux {
	
	//because glUniformMatrix4fv need to be applied the program must be current...
	void applyViewToCurrentProgram(views::View const& view){
		
        auto proj_location = glsl::getUniformLocationFromCurrentProgram("proj_matrix");
        auto mv_location   = glsl::getUniformLocationFromCurrentProgram("mv_matrix");

        glUniformMatrix4fv(proj_location, 1, GL_FALSE, view.getProjectionPtr());
        glUniformMatrix4fv(mv_location, 1, GL_FALSE, view.getModelPtr()); 
	}


	void checkAndPrintOpenGLError(std::string const& label){
		//Check https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetError.xhtml
		//for description of glGetError()
		GLenum  errorCode;
		while((errorCode = glGetError()) != GL_NO_ERROR){
			if(label.empty()){
				std::cout<<"Error code: "<<  errorCode;
			}else{
				std::cout<<"Error code at "<< label <<": "<<  errorCode;
			}
			
			switch (errorCode){
				case GL_INVALID_ENUM: 
				std::cout<<" GL_INVALID_ENUM (An unacceptable value has been specified for an enumerated argument)."<<std::endl;
				break;
				case GL_INVALID_VALUE:
				std::cout<<" GL_INVALID_VALUE (A numeric argument is out of range).";
				break;
				case GL_INVALID_OPERATION:
				std::cout<<" GL_INVALID_OPERATION (The specified operation is not allowed in the current state).";
				break;
				case GL_INVALID_FRAMEBUFFER_OPERATION:
				std::cout<<" GL_INVALID_FRAMEBUFFER_OPERATION (The framebuffer object is not complete)."; 
				break;
				case GL_OUT_OF_MEMORY:
				std::cout<<" GL_OUT_OF_MEMORY (There is not enough memory left to execute the command)."; 
				break;
				case GL_STACK_UNDERFLOW:
				std::cout<<" GL_STACK_UNDERFLOW (An attempt has been made to perform an operation that would cause an internal stack to underflow)."; 
				break;
				case GL_STACK_OVERFLOW:
				std::cout<<" GL_STACK_OVERFLOW (An attempt has been made to perform an operation that would cause an internal stack to overflow)."; 
				break;
				default:
				std::cout<<" Unknown error? it should not happen!"; 
			}
			std::cout<<std::endl;
		}	

	}




}