
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLAUXSINGLELINEBUFFERS_H_
#define GLAUXSINGLELINEBUFFERS_H_ 

//Basic data and functions for creating lines as triangular meshes in frame buffer 
#include "definitions.h"
#include <vector>
namespace glaux{


	class SingleLineBuffers
	{
	public:
		SingleLineBuffers():currentColor_{0,0,0,1},currentThickness_{1.0f},isDrawing_{false}{}
		virtual ~SingleLineBuffers(){}
		SingleLineBuffers( SingleLineBuffers const&)=delete;
		SingleLineBuffers( SingleLineBuffers&&)=default;
		SingleLineBuffers& operator = (SingleLineBuffers const&)=delete;

		void addLine(Point const& pt0, Point const& pt1);
		void addLine(Point const& pt0, Point const& pt1, Color const& color, float thickness);

		void begin(GLenum type);
		void end();
		void vertex(Point const& pt);

		void clear();

		void addLine(
			Point const& pt0, Color const& col0, float thickness0,
			Point const& pt1, Color const& col1, float thickness1);

		void setColor(Color const& color){ previousColor_ = currentColor_; currentColor_=color;}
		void setThickness(float thickness){previousThickness_ = currentThickness_; currentThickness_=thickness;}

		void* getVertexPtr(){return vertices_.data();}
		void* getOtherVertexPtr(){return otherVer_.data();}
		void* getColorPtr(){return colors_.data();}
		void* getThicknessPtr(){return thicknesses_.data();}
		void* getDirectionPtr(){return direction_.data();}
		void* getFacesPtr(){return faces_.data();}
		size_t numVertices(){return vertices_.size();}
		size_t numFaces(){return faces_.size();}


	private:
		std::vector<Point> vertices_;
        std::vector<Point> otherVer_;
        std::vector<Color> colors_;
        std::vector<float> direction_; //this is really just a boolean value
        std::vector<float> thicknesses_;
        std::vector<Face>  faces_;

        Color currentColor_;
        float currentThickness_;

        //used for drawing lines and polylines
        bool isDrawing_;
        GLenum currenType_;
        size_t vcount_;

        Point previousPt_;
        Color previousColor_; 
        float previousThickness_;

        Point firstPt_; //only used for closing
        Color firstColor_; 
        float firstThickness_;


	};

}
#endif