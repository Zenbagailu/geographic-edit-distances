
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "TestTriangleDrawer.h"
#include <aux/utils.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>


namespace glaux {

	TestTriangleDrawer::TestTriangleDrawer():vertices_{GL_STATIC_DRAW}{

		colorProgramO_=glsl::ProgramO{
			glsl::ShaderO{GL_VERTEX_SHADER, aux::getStringFromFile("shaders/ColorVS.glsl")}, 
			glsl::ShaderO{GL_FRAGMENT_SHADER, aux::getStringFromFile("shaders/ColorFS.glsl")}
		};

        glUseProgram(colorProgramO_.getIndex()); //TEST 2019

        GLfloat vertexData[] = {
        //  X     Y     Z
        	0.0f, 0.8f, -1.0f, //a bit back on Z, so it is possible to test with something rendererd on z=0
        	-0.8f,-0.8f, -1.0f,
        	0.8f,-0.8f, -1.0f,
        };


        GLfloat colorData[] = { 
        //  R     G     B     A
        	0.0f, 1.0f, 0.0f, 1.0f,
        	1.0f, 1.0f, 0.0f, 1.0f,
        	0.0f, 1.0f, 1.0f, 1.0f
        };

        vertices_.set(vertexData, colorData, 3);
        //vertices_.set(vertices.data(), colors.data(), vertices.size());

        //Bind with program
        vertices_.bindVerticesTo(glGetAttribLocation(colorProgramO_.getIndex(),"position"));
        vertices_.bindColorsTo(glGetAttribLocation(colorProgramO_.getIndex(),"color"));

    }

    void TestTriangleDrawer::TestTriangleDrawer::render(){
		glUseProgram(colorProgramO_.getIndex()); //render as we want

		//set unit matrices 
		auto proj_location = glsl::getUniformLocationFromCurrentProgram("proj_matrix");
        auto  mv_location  = glsl::getUniformLocationFromCurrentProgram("mv_matrix");

        glm::mat4 unitMatrix(1.0f); //the default constructor is not a unit matrix, 2019.

        glUniformMatrix4fv(proj_location, 1, GL_FALSE, glm::value_ptr(unitMatrix));
        glUniformMatrix4fv(mv_location, 1, GL_FALSE, glm::value_ptr(unitMatrix)); 

        vertices_.render(GL_TRIANGLES);
	}
}