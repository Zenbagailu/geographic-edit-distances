
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "SingleLineBuffers.h"
#include <stdexcept>

namespace glaux{

	void SingleLineBuffers::clear(){
		if(isDrawing_){
			throw(std::runtime_error("Call to SingleLineBuffers::clear() from an unfinished drawing cycle."));
		}
		vertices_.clear();
        otherVer_.clear();
        colors_.clear();
        thicknesses_.clear();
        direction_.clear();
        faces_.clear();

	}

	void SingleLineBuffers::begin(GLenum type){
		if(isDrawing_){
			throw(std::runtime_error("Call to SingleLineBuffers::begin() from an unfinished drawing cycle."));
		}
		currenType_ = type;
		switch(currenType_){
			case GL_LINES:
			case GL_LINE_STRIP:
			case GL_LINE_LOOP:
			break;

			default:
			throw(std::runtime_error("unknown GLenum or primitive type in SingleLineBuffers::begin()"));
		}

		isDrawing_=true;
		vcount_= 0;


	}

	void SingleLineBuffers::end(){
		if(isDrawing_){
			isDrawing_=false;
			if(currenType_ == GL_LINE_LOOP){
				//addLine(previousPt_,firstPt_);
				addLine(previousPt_, previousColor_,previousThickness_,firstPt_,firstColor_,firstThickness_);
			}
		}

	}

	void SingleLineBuffers::vertex(Point const& pt){
		if(!isDrawing_){
			throw(std::runtime_error("Call to SingleLineBuffers::vertex() without previous call to SingleLineBuffers::begin()"));
		}

		switch(currenType_){
			case GL_LINES:

			if(vcount_ == 0){
				++vcount_;
			}
			else{
				//addLine(previousPt_,pt);
				addLine(previousPt_, previousColor_,previousThickness_,pt,currentColor_,currentThickness_);
				vcount_= 0;
			}

			break;

			case GL_LINE_LOOP:

			if(vcount_ == 0){ //only for GL_LINE_LOOP
				firstPt_=pt;
				firstColor_=currentColor_;
				firstThickness_ = currentThickness_;
			}

			case GL_LINE_STRIP:

			if(vcount_ > 0) {  //for both GL_LINE_LOOP and GL_LINE_STRIP
				addLine(previousPt_, previousColor_,previousThickness_,pt,currentColor_,currentThickness_);
			}
			++vcount_;

			break;


			default:
			throw(std::runtime_error("unknown GLenum or primitive type in SingleLineBuffers::begin()"));
		}

		previousPt_=pt;
		//These need to be updated with every new vertex...
		previousColor_=currentColor_;
		previousThickness_ = currentThickness_;

	}

	void SingleLineBuffers::addLine(Point const& pt0, Point const& pt1){
		SingleLineBuffers::addLine(pt0, currentColor_,currentThickness_, pt1, currentColor_,currentThickness_);
	}

	void SingleLineBuffers::addLine(Point const& pt0, Point const& pt1, Color const& color, float thickness){
		SingleLineBuffers::addLine(pt0, color,thickness, pt1, color,thickness);
	}

	void SingleLineBuffers::addLine(
		Point const& pt0, Color const& col0, float thickness0, 
		Point const& pt1, Color const& col1, float thickness1){

		auto cI=vertices_.size();
	    vertices_.push_back(pt0);
	    otherVer_.push_back(pt1);
	    thicknesses_.push_back(-thickness0);
	    direction_.push_back(1.0);
	    colors_.push_back(col0);

	    vertices_.push_back(pt0);
	    otherVer_.push_back(pt1);
	    thicknesses_.push_back(thickness0);
	    direction_.push_back(1.0);
	    colors_.push_back(col0);

	    vertices_.push_back(pt1);
	    otherVer_.push_back(pt0);
	    thicknesses_.push_back(thickness1);
	    direction_.push_back(-1.0); //invert sign of thinckness value in shader (not its direction)
	    colors_.push_back(col1);

	    vertices_.push_back(pt1);
	    otherVer_.push_back(pt0);
	    thicknesses_.push_back(-thickness1);
	    direction_.push_back(-1.0); //invert sign of thinckness value in shader (not its direction)
	    colors_.push_back(col1);


	    // auto cI=vertices_.size();
	    // vertices_.push_back(pt0); //two copies
	    // otherVer_.push_back(pt1);
	    // thicknesses_.push_back(-thickness0);
	    // colors_.push_back(col0);

	    // vertices_.push_back(pt0);
	    // otherVer_.push_back(pt1);
	    // thicknesses_.push_back(thickness0);
	    // colors_.push_back(col0);

	    // glaux::Point next{2*pt1[0]-pt0[0],2*pt1[1]-pt0[1],2*pt1[2]-pt0[2]};

	    // vertices_.push_back(pt1);
	    // otherVer_.push_back(next);
	    // thicknesses_.push_back(-thickness1);
	    // colors_.push_back(col1);

	    // vertices_.push_back(pt1);
	    // otherVer_.push_back(next);
	    // thicknesses_.push_back(thickness1);
	    // colors_.push_back(col1);

	    //ccw
	    faces_.push_back({cI  , cI+2, cI+1});
	    faces_.push_back({cI+2, cI+3, cI+1});
	}
}