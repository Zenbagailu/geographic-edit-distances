
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#ifndef GLAUXDRAWINGMODE_H_
#define GLAUXDRAWINGMODE_H_ 
//not very clear that it should be in aux, it is used by glaux and by svg, and eventually be other
//forms of drawing or rendering... possibly a generic renderer should have all the basic functions (begin, end...)
//defining the generic interface and then different versions may implement opengl, svg or other type of rendering 

//GL/glew includes all necessary and correct OpenGL headers. 
//If not using glew, change this for the convinient headers
//OpenGL/gl.h in OS X, or GL/gl.h in other operating systems,
//or gl3.h.
#include <GL/glew.h>

namespace glaux {
    //this is just make GL modes visible
    enum Mode: GLenum{
        POINTS          = GL_POINTS,
        LINES           = GL_LINES,
        LINE_STRIP      = GL_LINE_STRIP, 
        LINE_LOOP       = GL_LINE_LOOP, 
        TRIANGLES       = GL_TRIANGLES, 
        TRIANGLE_STRIP  = GL_TRIANGLE_STRIP, 
        TRIANGLE_FAN    = GL_TRIANGLE_FAN, 
        QUADS           = GL_QUADS, 
        QUAD_STRIP      = GL_QUAD_STRIP, 
        //POLYGON         = GL_POLYGON,
    };
}
#endif