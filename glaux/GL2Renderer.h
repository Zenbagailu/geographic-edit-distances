
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLAUXGL2RENDERER_H_
#define GLAUXGL2RENDERER_H_ 

//This is a modified version of the general renderer so it has no "think lines" version. 
//This is because thick lines us a geometry renderer not available in previous versions of GLSL (like GLSL ES 1.00)

#include <utility> //for pair
#include <array>
#include <vector>
#include <functional>

#include "ColorDrawer.h"
#include "LineDrawer.h"
#include "SingleLineBuffers.h"
#include "DrawingMode.h"

#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <glfonts/FontAtlas.h>

#include <string>

#include <map>


namespace glaux {
    using Point = std::array<float,3>;
    using Color = std::array<float,4>;
    using ChangeFunc = std::function<void()>;
    //the first element is the function, the second the starting index from which
    //the change of state of the event will be valid
    using Event = std::pair<ChangeFunc,int>; 
    //using Mode = DrawingMode;
    using DrawMode = std::underlying_type<Mode>::type; //it should in principle be GLenum


	//Color buffers are used because, after testing, the monochrome version means no performance 
    //improvement at all (even if all colors have to be assigned... actually in some cases it was
    //faster.This also makes it more versatile, as functions for vertices with individual colors 
    //can be easily produced.

    //Used for triangles and points. It can be used for lines if used with GL_LINES and equivalents,
    //but it is not a reliable form of producing good antialiased lines, unles a geometry shader is 
    //used to expand the lines into quads. Since this is made for OpenGL ES 2, that is not possible.
    class BasicRendererData
    {
        using DrawerType = glaux::ColorDrawer;
    public:
        BasicRendererData(GLenum drawType):drawer_{drawType}{}
        DrawerType& getDrawer(){return drawer_;}
        void draw(DrawMode& drawingMode);
        
        void vertex(Point const& pt);
        template<class PT> void vertex(PT const& pt);
        
        void color(Color const& color){colors_.push_back(color);}
        void clear();
        void set(){drawer_.set(vertices_.data(),colors_.data(),vertices_.size());}
        void insertEvent(ChangeFunc const& func){events_.push_back({func,vertices_.size()});};

        std::size_t size(){return vertices_.size();}


    private:
        DrawerType drawer_;
        std::vector<Point> vertices_;
        std::vector<Color> colors_;
        std::vector<Event> events_;

    };


    //Any type that implements [] operator
    template<class PT> 
    void 
    BasicRendererData::vertex(PT const& pt){
        vertices_.push_back({
            static_cast<float>(pt[0]),
            static_cast<float>(pt[1]),
            static_cast<float>(pt[2])});
    }

    //This is a class for rendering lines in GL ES 2, using expansion into quads in the vertex shader.
    //It develops the ideas explained in: https://mattdesl.svbtle.com/drawing-lines-is-hard

    class LineRendererData
    {
    	using DrawerType = glaux::LineDrawer;
    public:
    	LineRendererData(GLenum drawType):drawer_{drawType}{}
        void draw();
        DrawerType& getDrawer(){return drawer_;}
        //void color(Color const& color){colors_.push_back(color);}
        void clear(){buffers_.clear();}

        void thickness(float thickness){buffers_.setThickness(thickness);}
        void color(Color const&  color){buffers_.setColor(color);}
        void begin(Mode mode){buffers_.begin(static_cast<DrawMode> (mode));}
        void vertex(Point const& pt){buffers_.vertex(pt);}
        template<class PT> void vertex(PT const& pt);
        void end(){buffers_.end();}


        void set(){drawer_.set(
        	buffers_.getVertexPtr(),
        	buffers_.getOtherVertexPtr(),
        	buffers_.getColorPtr(),
            buffers_.getThicknessPtr(),
            buffers_.getDirectionPtr(),
            buffers_.getFacesPtr(),
            buffers_.numVertices(),
            buffers_.numFaces());
    	}


    private:
    	DrawerType drawer_;
    	SingleLineBuffers buffers_;
    };

        //Any type that implements [] operator
    template<class PT> 
    void 
    LineRendererData::vertex(PT const& pt){

        vertex(Point{
            static_cast<float>(pt[0]),
            static_cast<float>(pt[1]),
            static_cast<float>(pt[2])});
    }


    
	class GL2Renderer
	{
	public:

		//static const std::map<Mode, GLenum> ModeMap;
    
        using DrawingMode = Mode; //to allow using simply Renderer::DrawingMode

		GL2Renderer(GLenum renderType = GL_DYNAMIC_DRAW);
		virtual ~GL2Renderer();
		GL2Renderer( GL2Renderer const&)=delete;
		GL2Renderer& operator = (GL2Renderer const&)=delete;

		void clear();
		void set();
		template<class VT> void draw(VT const& view);

		void setPointSize(float ptSize);
		void setLineThickness(float thickness);
		void setColor(float r, float g, float b, float a=1.0f);
		void setColor(Color const& color){currentColor_= color;}
        template<class CT> void setColor(CT const& col);

		//implementing a general, old opengl paradigm:
		void begin(Mode mode);
		//PT needs to implement []. Specialised versions for arrays and std::array
		template<class PT> void vertex(PT const& pt); 
		void vertex(float x, float y, float z=0); 
		void vertex(Point const& pt); 
		template<class PTIT> void vertices(PTIT ptBegin, PTIT ptEnd);
		void end();

	   // glm::mat4 const& getTransformation();
		void setTransformation(glm::mat4 const& trans);

		void setFont(glfonts::FontAtlas fontAtlas);

        //these two are simple general utiliy funtions and don't need to be here 
        // static glfonts::Face createFaceFromSystem(std::string const& fontName);
        // static glfonts::Face createFaceFromFile(std::string const& fontPath);

        // //this needs the font program to bind the textures
        // glfonts::FontAtlas createFontAtlasFromFace(glfonts::Face const& face, int size)

		void renderTextInPos(float x, float y, float z, std::string const& text,float xOff=0,float yOff=0);
		void renderTextRelative(float xOff,float yOff,std::string const& text); //considers current transMatrix
		void renderText(float x,float y,std::string const& text);

        Point unproject(double x, double y, double z=0); //this is a utility function, not used in the rendering

	private:

		//variables for matrices and locations needs to be visible when 
		//using the lambdas for transformations, so member variables 
		//can be used to access these values when they are called in draw().

		struct MatrixInfo{
		public:
			MatrixInfo(){}

			MatrixInfo(GLint mvLoc, GLint pLoc, 
			           glm::mat4 const& mvMatrix, glm::mat4 const& pMatrix,
			           float width, float height):
			modelLocation{mvLoc},
			projLocation{pLoc},
			modelMatrix{&mvMatrix},
			projMatrix{&pMatrix},
			w{width},
			h{height}
			{}

			GLint getModelLocation()const{return modelLocation;}
			GLint getProjLocation()const{return projLocation;}
			glm::mat4 const& getModelMatrix()const{return *modelMatrix;}
			glm::mat4 const& getProjMatrix()const{return  *projMatrix;}
			float getWidth()const{return w;}
			float getHeight()const{return h;}

		private:
			GLint modelLocation;
			GLint projLocation;
			glm::mat4 const* modelMatrix; //pointer to const
			glm::mat4 const* projMatrix;  //pointer to const
			float w,h;
		};

        //this is only used for creating render data in other places than constructor, for example elements in LineRendererData
        //and eventually meshes.
        GLenum renderType_; 

		//Standard Color
        glsl::ProgramO programPoints_;
		glsl::ProgramO programLines_;
		glsl::ProgramO programFaces_;
        //glsl::ProgramO programFonts_;
        
        //using programPoints_
        BasicRendererData pointRenderData_;
        //using programLines_
        std::vector<LineRendererData> lineRendersData_;
        size_t numberLineData_;
        size_t currentLineData_;
        std::vector<Event> lineEvents_;

        //using programFaces_
        BasicRendererData facesRenderData_;


		std::vector<ChangeFunc> fontEvents_; //it does not need indices
		glfonts::FontAtlas currentFontAtlas_; //to store it while drawing
		glm::mat4 currentFontTransMatrix_; //just used in the text drawing to calculate relative pos

		MatrixInfo matrixInfo_;

        DrawMode drawingMode_; //changed by events with begin(). Used for polylines and faces
		Mode currentMode_; //for using begin(), vertex(), end()
		//it controls if currentMode_ has been set. To use one of
		//the enums would expose the state through the interface.
		bool modeSet_; 

        Color currentColor_;
        float currentLineThickness_;
        // float lastLineThickness_;
        // float lastPolylineThickness_;



		
	};

	template<class VT>
	void 
	GL2Renderer::draw(VT const& view){

        //if w or h are <=0, it won't be able to draw lines with thickness or text
        if (view.getWidth()<=0 || view.getHeight()<=0){
            throw std::runtime_error("Width and height of view in GL2Renderer::draw() " 
                                     "need to be larger than 0 to be able to draw.");
        }

        //DRAW POINTS with points program. Drawn first so lines are drawn behind
        //---------------------------------------------------------------------------
        glUseProgram(programPoints_.getIndex());
        glaux::applyViewToCurrentProgram(view);//default view transformations

        //and get again for current program
        matrixInfo_={
            glsl::getUniformLocationFromCurrentProgram("mv_matrix"),
            glsl::getUniformLocationFromCurrentProgram("proj_matrix"),
            view.getModelMatrix(),
            view.getProjectionMatrix(),
            view.getWidth(),
            view.getHeight()
        };

        drawingMode_= GL_POINTS; // it will be the same, no events registered with begin()
        pointRenderData_.getDrawer().bindVerticesTo(glGetAttribLocation(programPoints_.getIndex(),"position"));
        pointRenderData_.getDrawer().bindColorsTo(glGetAttribLocation(programPoints_.getIndex(),"color")); 

        glEnable(GL_DEPTH_TEST); //in case it was disabled
        glDepthMask(GL_FALSE);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        pointRenderData_.draw(drawingMode_);

        glDepthMask(GL_TRUE);
        glDisable(GL_BLEND);

         //DRAW FACES . Drawn first so lines are drawn behind
        //---------------------------------------------------------------------------
        glUseProgram(programFaces_.getIndex());
        glaux::applyViewToCurrentProgram(view);//default view transformations
        
		//just in case there are transformation events, it needs to reset.
		matrixInfo_={
			glsl::getUniformLocationFromCurrentProgram("mv_matrix"),
			glsl::getUniformLocationFromCurrentProgram("proj_matrix"),
			view.getModelMatrix(),
			view.getProjectionMatrix(),
			view.getWidth(),
			view.getHeight()
		};

        facesRenderData_.getDrawer().bindVerticesTo(glGetAttribLocation(programFaces_.getIndex(),"position"));
        facesRenderData_.getDrawer().bindColorsTo(glGetAttribLocation(programFaces_.getIndex(),"color"));
        facesRenderData_.draw(drawingMode_);

        //DRAW LINES
        //---------------------------------------------------------------------------
        glUseProgram(programLines_.getIndex());
        glaux::applyViewToCurrentProgram(view);//default view transformations
        //just in case there are transformation events, it needs to reset.
		matrixInfo_={
			glsl::getUniformLocationFromCurrentProgram("mv_matrix"),
			glsl::getUniformLocationFromCurrentProgram("proj_matrix"),
			view.getModelMatrix(),
			view.getProjectionMatrix(),
			view.getWidth(),
			view.getHeight()
		};

        // for (int i = 0; i < numberLineData_; ++i){
        //     lineRendersData_[i].getDrawer().bindVerticesTo(glGetAttribLocation(programLines_.getIndex(),"position"));
        //     lineRendersData_[i].getDrawer().bindColorsTo(glGetAttribLocation(programLines_.getIndex(),"color"));
        //     lineRendersData_[i].getDrawer().bindOtherVerticesTo(glGetAttribLocation(programLines_.getIndex(),"otherPos"));
        //     lineRendersData_[i].getDrawer().bindThicknessesTo(glGetAttribLocation(programLines_.getIndex(),"thickness"));
        //     lineRendersData_[i].getDrawer().bindDirectionTo(glGetAttribLocation(programLines_.getIndex(),"direction"));

        //     auto vPLoc = glsl::getUniformLocationFromCurrentProgram("viewPort");
        //         std::array<float,2> viewPort{view.getWidth(),view.getHeight()};
        //         glUniform2fv(vPLoc,1,viewPort.data());

        //     lineRendersData_[i].draw();
           
        // }

        size_t lEInd=0;
        for(auto const&  lEvent: lineEvents_){
            
            //first draw with states set before event
            if(lEInd < lEvent.second){
                lineRendersData_[lEInd].getDrawer().bindVerticesTo(glGetAttribLocation(programLines_.getIndex(),"position"));
                lineRendersData_[lEInd].getDrawer().bindColorsTo(glGetAttribLocation(programLines_.getIndex(),"color"));
                lineRendersData_[lEInd].getDrawer().bindOtherVerticesTo(glGetAttribLocation(programLines_.getIndex(),"otherPos"));
                lineRendersData_[lEInd].getDrawer().bindThicknessesTo(glGetAttribLocation(programLines_.getIndex(),"thickness"));
                lineRendersData_[lEInd].getDrawer().bindDirectionTo(glGetAttribLocation(programLines_.getIndex(),"direction"));

                auto vPLoc = glsl::getUniformLocationFromCurrentProgram("viewPort");
                    std::array<float,2> viewPort{view.getWidth(),view.getHeight()};
                    glUniform2fv(vPLoc,1,viewPort.data());

                lineRendersData_[lEInd].draw();

                lEInd = lEvent.second;
            }
            //then change settings
            lEvent.first();
        }

        //Now do last. (or if no events) 
        if(lEInd < numberLineData_){
            lineRendersData_[lEInd].getDrawer().bindVerticesTo(glGetAttribLocation(programLines_.getIndex(),"position"));
            lineRendersData_[lEInd].getDrawer().bindColorsTo(glGetAttribLocation(programLines_.getIndex(),"color"));
            lineRendersData_[lEInd].getDrawer().bindOtherVerticesTo(glGetAttribLocation(programLines_.getIndex(),"otherPos"));
            lineRendersData_[lEInd].getDrawer().bindThicknessesTo(glGetAttribLocation(programLines_.getIndex(),"thickness"));
            lineRendersData_[lEInd].getDrawer().bindDirectionTo(glGetAttribLocation(programLines_.getIndex(),"direction"));

            auto vPLoc = glsl::getUniformLocationFromCurrentProgram("viewPort");
                std::array<float,2> viewPort{view.getWidth(),view.getHeight()};
                glUniform2fv(vPLoc,1,viewPort.data());

            lineRendersData_[lEInd].draw();

        }
        

	
        //DRAW TEXTS //needs antialias set
        //---------------------------------------------------------------------------

        matrixInfo_={
            glsl::getUniformLocationFromCurrentProgram("mv_matrix"),
            glsl::getUniformLocationFromCurrentProgram("proj_matrix"),
            view.getModelMatrix(),
            view.getProjectionMatrix(),
            view.getWidth(),
            view.getHeight()
        };
    
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        for(auto const& fEvent: fontEvents_){
            fEvent();
        }   

        glDisable(GL_BLEND);
	}


    template<class PT> 
    void 
    GL2Renderer::vertex(PT const& pt){

         if(!modeSet_){
            return;
        }

        switch (currentMode_){

            case Mode::POINTS:
            pointRenderData_.vertex(pt);
            pointRenderData_.color(currentColor_);
            break;

            case Mode::LINES:
            case Mode::LINE_STRIP://addEventTo set Polyline mode 
            case Mode::LINE_LOOP:
            lineRendersData_[currentLineData_].thickness(currentLineThickness_);
            lineRendersData_[currentLineData_].color(currentColor_);
            lineRendersData_[currentLineData_].vertex(pt);
            break;

            case Mode::TRIANGLES:
            case Mode::TRIANGLE_STRIP:
            case Mode::TRIANGLE_FAN:
            case Mode::QUADS:
            case Mode::QUAD_STRIP:
            facesRenderData_.vertex(pt);
            facesRenderData_.color(currentColor_);
            break;

            default:
            throw std::runtime_error("Trying to add a vertex without specifying mode with begin()" );
        }

    }

    //process vertices by calling vertex for each of them...
	template<class PTIT> 
	void 
	GL2Renderer::vertices(PTIT ptBegin, PTIT ptEnd){
		while(ptBegin!=ptEnd){
			vertex(*(ptBegin++));
		}
	}

    template<class CT> 
    void 
    GL2Renderer::setColor(CT const& col){
        currentColor_={
            static_cast<float>(col[0]),
            static_cast<float>(col[1]),
            static_cast<float>(col[2]),
            static_cast<float>(col[3])
        }; 
    }


}
#endif