
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GLAUXTESTTRIANGLEDRAWER_H_
#define GLAUXTESTTRIANGLEDRAWER_H_ 

#include "ColorDrawer.h"
#include <glsl/Program.h>
#include "definitions.h"
#include <vector>

#include "functions.h"
#include <iostream>

//It integrates everything (program...etc) so it is easy
//to include for tests, etc.

namespace glaux {
	class TestTriangleDrawer final
	{
	public:
		TestTriangleDrawer();
		~TestTriangleDrawer(){}
		TestTriangleDrawer( TestTriangleDrawer const&)=delete;
		TestTriangleDrawer& operator = (TestTriangleDrawer const&)=delete;
		void render();
		template<typename V> void render(V const& view);
		

	private:
		ColorDrawer vertices_;
		glsl::ProgramO colorProgramO_;
	
	};

	template<typename V>
	void 
	TestTriangleDrawer::render(V const& view){

		glUseProgram(colorProgramO_.getIndex()); //render as we want
        applyViewToCurrentProgram(view);
        vertices_.render(GL_TRIANGLES);
	}
}
#endif