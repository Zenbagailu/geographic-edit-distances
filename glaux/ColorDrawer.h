
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#ifndef GLAUXCOLORDRAWER_H_
#define GLAUXCOLORDRAWER_H_ 

//This (and MonochormeDrawer) should be solved with variadic templates. That would
//also make possible to expand it if we need normals... etc perhaps even StaticMeshWithTopology
//could be made into this? Policies in variadic templates? (so it would be possible to generate
//methods like bindColorsTo... specifically for every type...or functors?)

//#include <glsl/definitions.h>
#include "definitions.h"
#include "functions.h"

namespace glaux {

	//This is a generic drawer type that can be static, dynamic, streamed...
	//it has a minor unnecessary buffer orphaning if static, but it is a minor
	//detail that should not affect efficiency

	//none of the methods are const even if they don't realy modify 
	//the members it does affect the resources they refer to.
	
	class ColorDrawer final
	{
	public:

	    ColorDrawer(GLenum drawType_);
		void set(void* ptBuffer, void* colBuffer, size_t newSize);

		//Other constructors should follow that take other types besides raw pointers ot floats
		//also perhaps those with a stride.

		virtual ~ColorDrawer(){};
		ColorDrawer( ColorDrawer const&)=default;
		ColorDrawer& operator = (ColorDrawer const&)=default;
		//the appropiate program (to which the vertices have been binded through 
		//bindVerticesTo) should be current before calling render.
		void render(GLenum mode); 
		void render(GLenum mode, size_t begin, size_t end);
		void bindVerticesTo(GLint attributeLocation);
		void bindColorsTo(GLint attributeLocation);
	
	private:
		GLenum drawType_;
		VAO vAO_;
        VBOFloat verticesVBO_;
        VBOFloat colorsVBO_;
        size_t   size_;
        
	};
}
#endif