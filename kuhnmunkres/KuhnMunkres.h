
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

/**Copyright (C) 2017  Pablo Miranda Carranza
* the Hungarian algorithm or Kuhn-Munkres algorithm, for
* the solution of the (minimum) assignment problem.
* Based on original code from Knuth's Stanford Graphbase
*/

#ifndef KUHNMUNKRESALGORITHM_H_
#define KUHNMUNKRESALGORITHM_H_

#include <vector>
#include <array>
#include <list>
//#include <deque>
#include <stdexcept>
#include <cassert>

namespace kuhnmunkres
{

// ----------------------------------------------------------------------------------------

    template <class MType>
    typename MType::num_t assignmentCost (
        MType const& cost,
        std::vector<std::size_t> const& assignment //for each row
    )
    { 
        typename MType::num_t temp = 0;
        for (std::size_t i = 0; i < assignment.size(); ++i){
            temp += cost(i, assignment[i]);
        }
        return temp;
    }

// ----------------------------------------------------------------------------------------

    //For using the heuristic in 
    //http://www-cs-faculty.stanford.edu/~uno/sgb.html

    template <class MType>
    MType reducedColumnsCopy(MType const& cost){
      
        MType reduced=cost;
        
        for (std::size_t t = 0; t < cost.nc(); ++t){
            typename MType::num_t min=reduced(0,t);
            for (std::size_t s = 1; s < cost.nr(); ++s){
                min = reduced(s,t) < min ? reduced(s,t) : min;
            }

            for (std::size_t s = 0; s < cost.nr(); ++s){
                reduced(s,t)-= min;
            }
        }

        return reduced;
    }

// ----------------------------------------------------------------------------------------
    //Used to pass the (unmatched) column and a row (in the forest) to rematch and extend matching
    struct Start{

     private:
        std::size_t row_=0;
        std::size_t col_=0;
        bool valid=false; 

    public:
        Start(): row_{0},col_{0}, valid{false}{}
        Start(std::size_t rowp,std::size_t colp){
            row_=rowp;
            col_=colp;
            valid=true;
        }
        std::size_t row()const{return row_;}
        std::size_t col()const{return col_;}
        bool isValid() {return valid;}
     };

// ----------------------------------------------------------------------------------------

    template<typename MType> class KuhnMunkres{

    public:

       
        using num_t = typename MType::num_t;
       
        static const std::size_t Unmatched;

        KuhnMunkres( MType const& matrix); //corresponds to step 16 in Knuth

        void  calculate();                                               //corresponds to step 18 in Knuth
        void  resetTransients();                                         //corresponds to step 17 in Knuth
        Start explorePath();                                             //corresponds to step 19 in Knuth
        Start introduceNewZero();                                        //corresponds to step 21 in Knuth
        Start lookAtNewZero(std::size_t const col,num_t const minSlack);  //corresponds to step 22 in Knuth
        void  updateMatching(Start const& start);                        //corresponds to step 20, breakthru

        std::vector<std::size_t> const& getMatchingOfRows(){return CofR;}

        void printDebug();
        std::string getIndexString(std::size_t indx);
  
    private:

        MType const& cost_;
        std::size_t nc,nr;
      
        //These are the values for the algorithm, which will remain and be modified
        //through the different iterations . Tof[s] stores the t vertex (column) the 
        //s vertex (row) is matched with, and Sof[t] the s vertex the t vertex is associated with. 
        //They have an Unmatched value if they are not matched or assigned. 

        std::vector<num_t> slack; //the minimum uncovered elements in each column
        std::vector<std::size_t> slackRow; //The row corresponding to minimum uncovered elem in the column 

        std::vector<std::size_t> parentRow; //this is equivalent to "parent_row" in Knuth 

        std::vector<std::size_t> CofR;  //column matching a row (col_mate)
        std::vector<std::size_t> RofC;  //row matching a column (row_mate)

        std::vector<num_t> rowDec; //the amount subtracted from a ginven row
        std::vector<num_t> colInc; //the amount added to a given column

        //since we add elements while iterating through it explorePath(), it should be a list
        //or other container that does not need resizing (and thus invalidates) its iterator
        std::list<std::size_t> unchosenRows; 
    };

    template<typename MType>
    const std::size_t KuhnMunkres<MType>::Unmatched = std::numeric_limits<std::size_t>::max();


     /**
    The constructor takes care of the inial stage (16) in Knuth:
    It simply goes through the matrix and looks for zeroes, matching as many 
    rows and columns as it can. This stage also initializes table entries 
    that will be useful in later stages.
    */

    template<typename MType> 
    KuhnMunkres<MType>::KuhnMunkres( MType const& matrix):
    cost_{matrix},
    nc{matrix.nc()},
    nr{matrix.nr()},
    slack(nc, std::numeric_limits<num_t>::max()),
    slackRow(nc),
    parentRow(nc,Unmatched), //stores the ancestor row of a chosen column (not its matched row)
    CofR(nr),
    RofC(nc, Unmatched), //intialise them unmatched
    rowDec(nr),
    colInc(nc,0)
    {
        for(std::size_t r=0;r<nr;++r){ //for all rows
            rowDec[r]=cost_.minInRow(r);

            bool matched=false;
            for(std::size_t c=0; c<nc;++c){ //for all columns in row
                 if (rowDec[r]==cost_(r,c) && RofC[c]==Unmatched){
                    CofR[r]=c;
                    RofC[c]=r;
                    matched=true;
                    break;
                 }  
            }
            if(!matched){
                CofR[r]=Unmatched;
                unchosenRows.push_back(r);
            }
        }
    }

    /**
    * Equivalent to stage 17 in Knuth's version. It just resets the values
    * of transient data before a new stage
    */

    template<typename MType> 
    void
    KuhnMunkres<MType>::resetTransients(){
        unchosenRows.clear(); 
        parentRow.assign(nc,Unmatched);
        slack.assign(nc, std::numeric_limits<num_t>::max());

       for(std::size_t r=0;r<nr;++r){ //for all rows
            if(CofR[r]==Unmatched){
                 unchosenRows.push_back(r);
            }
       }
    }

    /**
    * Equivalent to stage 19 in Knuth's version.
    * The goal of this algorithm is mainly to find which rows and columns are chosen,
    * That is, to define a cover of the bipartite graph defined by the nodes represented
    * by the columns and rows of the cost matrix. This is necessary for the next 
    * stage of the algorithm, which will introduce new zeros by modifying the values 
    * assigned to each node in the bipartite graph (rowDec and colInc). 
    * The algorithm makes use of the properties of the paths of matchings in bipartite graphs,
    * as described by Egerváry and Kónig (the origin of the Hungarian)Algorithm):
    *
    * If zeros selected for matching define a relation r=c and unmatched ones r-c
    * between rows and columns, then for a column to be chosen (as a line to be 
    * counted in the cover),it needs to be reacheable through a path of the form:
    * r0-c1=r1-c2=r2...-cn=rn. 
    * n can be 1, so r0-c1=r2 means that c1 is chosen.
    * A row is chosen if and only if it is matched with a column that is not chosen.
    * 
    * The code in explorePath starts with a forest of unchosen rows (we know that 
    * there are 0s in each row, this is a constant we keep from the very beginning of 
    * the algorithm, but those 0s were in a column that was already matched with another 
    * row, otherwise the row in question would not have been added to unchosenRows). 
    * It finds a 0 in the row, and through its column it finds the row the column is 
    * matched with (and marks the column as chosen (slack[c]=0). Then, it adds that row 
    * to unchosen, since the row cannot then be chosen (according to the definition for 
    * chosen rows above). This also makes sense if one thinks of lines (rows or columns) 
    * covering 0s on the matrix: the 0 that could have defined this matched row as chosen 
    * (as a line in the cover) is already covered by the column, so for the row to be 
    * chosen it would need to be matched with another column (have another 0 in an 
    * unchosen column). It won't be able to run through any chosen rows, since they 
    * will not be reachable from other unchosen rows (they won't share columns).
    *
    * It also updates the slack for the next phase of the algorithm (stage 21), which 
    * introduces new zeros (the slack is used to find minimum values in unchosen lines), 
    * and returns if it detects a new 0 in a previously unmatched column (a new matching 
    * that would imply to update the matchings in step 22). If the main algorithm (step18) 
    * has called explorePath() it means that there are unmatched rows and thus new matching 
    * 0s are needed).
    */

    template<typename MType>
    Start
    KuhnMunkres<MType>::explorePath(){

        for(auto row: unchosenRows){
            auto rd=rowDec[row];
            for(std::size_t c=0;c<nc;++c){ //for all elements in the row
                if(slack[c]>0){ //if the column of the element has not been chosen
                    num_t del=cost_(row,c) - rd + colInc[c];
                    if(del<slack[c]){
                        if(del==0){ //zero found
                            if(RofC[c]==Unmatched){ //new 0 that creates a matching found
                                return Start(row,c); //goto breakthru
                            }
                            slack[c]=0; //this means that the column has more than one 0, it will be chosen
                            parentRow[c]=row; //set the unchosen row used to reach the column
                            unchosenRows.push_back(RofC[c]); //the row intially matched is not chosen for this 0 (the column is)
                        }
                        else{
                            slack[c]=del;
                            slackRow[c]=row;

                        }
                    }
                }
            }
        }

        return Start(); //return invalid
    }

    /**
    * Equivalent to stage 21 in Knuth's version. After exploring the entire "Forest"
    * of unchosen rows, non has lead to a breakthrough. An unchosen column with smallest slack
    * will allow to make further progress, by introducing a new 0, modifying rowDec and colInc.
    * If the matching is increased, then goto breakthru.
    */

    template<typename MType>
    Start
    KuhnMunkres<MType>::introduceNewZero(){
        auto minSlack=std::numeric_limits<num_t>::max();
        for(std::size_t c=0;c<nc;++c){ //for all columns
            if(slack[c]>0 && slack[c]<minSlack){ //slack[c]==0 means that is a chosen column.
                minSlack=slack[c];
            }
        }
        //now minSlack is the smallest of the non 0 slacks (of the slacks that are)
        //not in chosen columns... because of the algorithm these won't be in chosen
        //rows either...

        for(auto row: unchosenRows){
            rowDec[row]+=minSlack;
        }

        for(std::size_t c=0;c<nc;++c){ //for all columns
            if(slack[c]>0){ //column c is not chosen
                slack[c]-=minSlack;
                if(slack[c]==0){ //new 0 found
                    //look at new 0, step 22
                    return lookAtNewZero(c,minSlack);
                }

            }
            else{ //column was chosen
                colInc[c]+=minSlack;
            }
        }
        return Start();
    }

    /**
    * corresponds to step 22 in Knuth
    * There might be several columns tied for smallest slack. Any of them 
    * can lead to a breakthrough. But we must finish the loop 
    * on the columns before going to breakthru, in order to update the colInc 
    * variables for the next stage. Within column c, there might be several 
    * rows that produce the same slack; slackRow[c] points to only one of 
    * them, but one is sufficient for our purposes. Either theres is a 
    * breakthrough or otherwise we choose column c, regardless of which row 
    * or rows led to consider that column.
    */

    template<typename MType>
    Start
    KuhnMunkres<MType>::lookAtNewZero(std::size_t const col, num_t const minSlack){
        auto row=slackRow[col];

        //in the original code by Knuth this is insode the equivalen  of 
        //"if(RofC[col]==Unmatched){"
        //but if updates all colInc here, it actually works with the
        //matrices used to test the code (and it doesn't otherwise...)
        //it also seems to make sense to update them here according to the algorithm

        for(std::size_t c=col+1; c<nc;++c){
            if(slack[c]==0){ //if chosen column
            colInc[c]+=minSlack;
            }
        }
        //now col and row indicate the position of the new 0 in the Matrix
        if(RofC[col]==Unmatched){ //finish updating the colInc[c] and return   
            return Start(row,col); //new unmatced row found
        }
        else{ //if it the row in which the 0 appeared was matched, not a breakthrough
            parentRow[col]=row;
            unchosenRows.push_back(RofC[col]); // but forest grows
        }
        return Start(); //new 0 introduced, but row was matched
    }   

    /**
     * Corresponds to step 20 in Knuth's implmentation. Update the matching by paring the row and 
     * column in start. 
     * At this point, column in Start is unmatched, and row in Start is in the forest (unchosenRows). 
     * By following parent links in the forest (unchosenRows), we can rematch rows and columns so that a 
     * previously unmatched row r(0) gets a mate column. 
    */

    template<typename MType>
    void
    KuhnMunkres<MType>::updateMatching(Start const& start){
         auto r=start.row();
         auto c=start.col();
        while(true){
            auto pCol=CofR[r];
            CofR[r]=c;
            RofC[c]=r;
            if(pCol==Unmatched){
                return;
            }
            r=parentRow[pCol];
            c=pCol;
        }
    }       


    template<typename MType>
    void
    KuhnMunkres<MType>::calculate(){

        while(!unchosenRows.empty()){

            Start start;

            while(true){

                start=explorePath();
                if(start.isValid()){
                    break; //goto breakthru 
                }

                //if explorePath failed, introduce new 0 into the matrix by modifying rowDec and colInc
                //if the matching can be increased, goto breakthru 

                start=introduceNewZero();
                if(start.isValid()){
                    break; //goto breakthru 
                }
            }
            //breakthru ...Start is valid the matching can be extended
            updateMatching(start);

            //resetTransients() will pickup any unassigned rows and add them
            //if after that unchosenRows is empty,it means that it is done (all rows are assigned).
            //In the orignal algorithm there is a counter that checks this before reseting,
            // which spares the extra work of resetting transient variables, but this is simpler...
            resetTransients();
        }
        //done
    }


    template<typename MType>
    std::string
    KuhnMunkres<MType>::getIndexString(std::size_t indx){
        if(indx==Unmatched){
            return "Un";
        }
        return std::to_string(indx);
    }


// ----------------------------------------------------------------------------------------

    template <typename MType>
    std::vector<std::size_t> calculateKuhnMunkres( MType const& cost){

        using num_t        = typename MType::num_t;
        using KuhnMunkres = KuhnMunkres<MType>;
        // The algorithm  works if the elements of the cost matrix can be reliably 
        // compared using operator== (only0s are compared). An integer cost matrix 
        // is necesary at the moment.

        assert(std::numeric_limits<num_t>::is_integer);

        //the algorithm cannot handle matrices with more rows than columns
        //(it goes into an endless loop). The opposite however (matrices with more 
        //columns than rows) is not a problem. It may be possible to transpose
        //the defective matrix or otherwise fill it with 0s, so it becomes square. 
        if(cost.nr() > cost.nc()){
            throw(std::invalid_argument("The matrix has more rows than columns."));
        }

        if (cost.nr() == 0){ //return empty vector if empty
            return std::vector<std::size_t>();
        }

        KuhnMunkres kuhnMunkres(cost);
        kuhnMunkres.calculate();
        return kuhnMunkres.getMatchingOfRows();
    }

} //end namespace

#endif


