
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef KUHNMUNKRES_H_	
#define KUHNMUNKRES_H_	 
#include <vector>
#include <ostream>
namespace kuhnmunkres {
	
	//It needs forward declarations to be able to implement templated friend function
	template<typename T>class Matrix;
	template<typename T> std::ostream& operator<<(std::ostream& os, Matrix<T> const& matrix);
	template<typename T> Matrix<T> createMatrix(std::initializer_list<std::initializer_list<T>> list);
	//these could be members, but as separate funtions (implemented as friends) allows higher
	//flexibility to implement for other matrix types, for example.

	/**
	* This is a simple class for storing a matrix of a type T. It implments the basic functionality necessary 
	* to be used in solving the minimum or maximum assignment problem thrugh the Kuhn-Munkres (or Hungarian) 
	* algorithm.
	*/

	template<typename T>class Matrix
	{
	public:
		using num_t = T;
		Matrix():rowN_{0},colN_{0}{}
		Matrix(std::size_t rowN,std::size_t colN);
		Matrix(std::size_t rowN,std::size_t colN, T const& initVal);
		//virtual ~Matrix(){} //then a default move constructor will be created

		//Matrix( Matrix const&)=delete;				//use default
		//Matrix& operator = (Matrix const&)=delete;    //use default
		//move constructor
		//Matrix(Matrix&& other):rowN_(other.rowN_),colN_(other.colN_),values_(std::move(other.values_)){}

		T& operator()(std::size_t row, std::size_t column);
		T const& operator()(std::size_t row, std::size_t column)const;

		std::size_t nc()const {return colN_;}
		std::size_t nr()const {return rowN_;}

		T maxInRow(std::size_t row)const;
		T minInRow(std::size_t row)const;
		T maxInColumn(std::size_t col)const;
		T minInColumn(std::size_t col)const;


		friend std::ostream& operator<< <T>(std::ostream& os, Matrix const& matrix);
		friend Matrix createMatrix<T>(std::initializer_list<std::initializer_list<T>> list);

		
		bool isSquare()const{return rowN_==colN_;}

	private:
		std::size_t rowN_;
		std::size_t colN_;
		std::vector<T> values_;
	};

	template<typename T>
	Matrix<T>::Matrix(std::size_t rowN, std::size_t colN):
	rowN_{rowN},
	colN_{colN},
	values_(colN*rowN){ }

	template<typename T>
	Matrix<T>::Matrix(std::size_t rowN,std::size_t colN, T const& initVal):
	rowN_{rowN},
	colN_{colN},
	values_(colN*rowN, initVal){}


	template<typename T>
	T &
	Matrix<T>::operator()(std::size_t row, std::size_t column){
		return values_[colN_*row + column];
	}

	template<typename T>
	T const&
	Matrix<T>::operator()(std::size_t row, std::size_t column)const{
		return values_[colN_*row + column];
	}

	template<typename T>
	T 
	Matrix<T>::maxInRow(std::size_t row)const{
		T max=values_[colN_*row];
		for(std::size_t i=colN_*row+1;i<colN_*(row+1);++i){
			max=values_[i] > max ? values_[i] : max;
		}
		return max;
	}

	template<typename T>
	T 
	Matrix<T>::minInRow(std::size_t row)const{
		T min=values_[colN_*row];
		for(std::size_t i=colN_*row+1;i<colN_*(row+1);++i){
			min=values_[i] < min ? values_[i] : min;
		}
		return min;
	}

	template<typename T>
	T 
	Matrix<T>::maxInColumn(std::size_t col)const{
		T max=values_[col];
		for(std::size_t i=col+colN_;i<values_.size();i+=colN_){
			max=values_[i] > max ? values_[i] : max;
		}
		return max;
	}

	template<typename T>
	T 
	Matrix<T>::minInColumn(std::size_t col)const{
		T min=values_[col];
		for(std::size_t i=col+colN_;i<values_.size();i+=colN_){
			min=values_[i] < min ? values_[i] : min;
		}
		return min;
	}


	template<typename T>
	std::ostream& 
	operator<<(std::ostream& os, Matrix<T> const& matrix){

		int colN=matrix.colN_;
		int rowN=matrix.rowN_;

		int inf=std::numeric_limits<T>::max();

	    for(std::size_t i=0;i<rowN;++i){
	    	os<<"row "<<i<<": {";
	    	for(std::size_t j=0;j<colN-1;++j){
	    		os<< (matrix.values_[colN*i + j]==inf? "Inf": std::to_string(matrix.values_[colN*i + j])) <<", ";		
	    	}
	    	os<< (matrix.values_[colN*i + colN-1] == inf ? "Inf": std::to_string(matrix.values_[colN*i + colN-1]))<<"}"<<std::endl;
	    }
	    return os;
	}

	template<typename T> Matrix<T> createMatrix(std::initializer_list<std::initializer_list<T>> list){
		Matrix<T> matrix(list.size(),list.begin()->size());
		int rn=0;
		for(auto const& row : list){
			std::copy(row.begin(), row.end(), &matrix.values_[rn* matrix.colN_]);
			++rn;
		}
		return matrix;
	}

	//does not need to be friend...just used for deducing T parameter
	template<typename T> Matrix<T> createMatrix(std::size_t rowNumber,std::size_t columnNumber, T const& initVal){
		return Matrix<T>{rowNumber,columnNumber,initVal};
	}
}
#endif