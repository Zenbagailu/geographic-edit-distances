
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#ifndef STRAIGHTPATH_H_
#define STRAIGHTPATH_H_ 

#include <array>
#include <vector>
#include <queue>
#include <functional>
#include <algorithm>
#include <type_traits>
#include <set> //so we don't have to implement a hash function for edgeId as in unordered_set

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp> //the graph type must be an implmentation of
#include <boost/graph/copy.hpp>

#include <graph/allIn.h>
#include <graph/algorithms.h>
#include <disjointsets/DisjointSets.h>


/**
 * The basic idea of the algorithm is is to us the line graph L(G) of G, and 
 * a modified version of Kruskal's algorithm for minimum spanning trees so the trees are restricted to their 
 * trivial form of a path (a tree of one branch).
 *
 * It is not necessary to explicilty build theL(G) graph; it is actually more clear to work on it inplicitly.
 *
 * The algorithm would work like this:
 *
 *
 * Use a a type called JoinEvent consisting of the cost of joining edges of the G graph (equivalent to 
 * edges of the L(G) graph), a reference to the vertex in G that generated it, and two references to the 
 * two edges it would join. It should also cotain a flag "joinsTwoPaths" indicating if the event links two paths,
 * initialialised to false.
 *
 * Generate JoinEvents corresponding to all pair of edges incident in to a node/vertes in G, and place them 
 * on a container, sorted depending on cost.
 *
 * Create a map M of vertices in G of order larger than 2, to indices (if the container for the JoinEvents supports it) 
 * or pointers of each of the JoinEvents. 
 *
 * Create a disjoint set DJ, consisting of all the edges of G
 *
 * iterate through the sorted JoinEvents and, if their flag "joinsTwoPaths" is false,  join the sets of its two edges in
 *  DJ, until the cost is larger than the maximum accepted. If the vertex around which the edges are being joined is of order > 2, do:
 *
 * find all other JoinEvents corresponding to the vertex using map M. if the  JoinEvents include any of the edges being joined
 * by the JoinEvent under consideration, set their "joinsTwoPaths" flag to true.
 * 
 * Finally the resulting disjoint sets, which will be paths, need to be processed into some graph structure.
 *
 * For this a graph consisting of nodes that contain a path in G, and edges that define crossings of these paths 
 * is built. 
 *
 */

//The base graph it is associated with can be copied as long as the vertex_descriptors of the copy correspond 
//to the ones of the original. Otherwise the vertex_descriptors in the PathGraphVertex and PathGraphEdge
//will obviously be invalid. 
namespace test {

    constexpr float PI = 3.1415927;

    template <class BaseGraph> struct PathGraphDefinitions{

        using BaseGraphVertexId      = typename boost::graph_traits<BaseGraph>::vertex_descriptor;
        using BaseGraphEdgeId        = typename boost::graph_traits<BaseGraph>::edge_descriptor;

        using PathGraphVertex   = std::vector<BaseGraphVertexId> ;
        using PathGraphEdge     = BaseGraphVertexId; 

        using PathGraphT = boost::adjacency_list<       //  adjacency_list is a template depending on :
        //boost::listS,                                 
        boost::vecS,                                               
        //boost::listS,                                   
        boost::vecS,
        boost::undirectedS,                             //  directed or undirected edges ?.                             
        PathGraphVertex,                                //  The type that describes a VertexProperties.(as Bundled properties)
        PathGraphEdge                                   //  The type that describes an Edge. (as Bundled properties)
        > ;  

        using PathGraphVertexId     = typename boost::graph_traits<PathGraphT>::vertex_descriptor;
        using PathGraphEdgeId       = typename boost::graph_traits<PathGraphT>::edge_descriptor;      
    };


    template<class Graph> 
    double 
    calculateCost(
                  Graph const& graph,
                  typename PathGraphDefinitions<Graph>::BaseGraphVertexId hingeVertexId,
                  typename PathGraphDefinitions<Graph>::BaseGraphEdgeId edgeId0,
                  typename PathGraphDefinitions<Graph>::BaseGraphEdgeId edgeId1){
        
        auto v0Id = graph::getOtherVertex(edgeId0, hingeVertexId, graph);
        auto v1Id = graph::getOtherVertex(edgeId1, hingeVertexId, graph);

        auto const& hingeVertex=graph[hingeVertexId];
        auto const& vertex0=graph[v0Id];
        auto const& vertex1=graph[v1Id];

        auto dir0X = vertex0[0] - hingeVertex[0];
        auto dir0Y = vertex0[1] - hingeVertex[1];

        auto dir1X = vertex1[0] - hingeVertex[0];
        auto dir1Y = vertex1[1] - hingeVertex[1];

        auto angle = atan2(dir1Y, dir1X) - atan2(dir0Y, dir0X);

        //set the angle counterclockwise (+) always, from 0 to 2PI
        angle = angle < 0 ? angle+2*PI : angle; 
        //in case that the result is larger than PI, take the complement
        angle = angle > PI ? 2*PI-angle : angle;
        
        return (PI-angle)/PI; //normalised cost
    }


    template< class Graph> 
    typename PathGraphDefinitions<Graph>::PathGraphT
    calculateStraightPathGraph(Graph const& graph,  double maxCost){

        using PathGraphDefinitions  = PathGraphDefinitions<Graph>;
        using BaseGraphVertexId     = typename PathGraphDefinitions::BaseGraphVertexId;
        using BaseGraphEdgeId       = typename PathGraphDefinitions::BaseGraphEdgeId;
        using PathGraphVertex       = typename PathGraphDefinitions::PathGraphVertex;
        using PathGraphEdge         = typename PathGraphDefinitions::PathGraphEdge; 
        using PathGraphT            = typename PathGraphDefinitions::PathGraphT;
        using PathGraphVertexId     = typename PathGraphDefinitions::PathGraphVertexId;
        using PathGraphEdgeId       = typename PathGraphDefinitions::PathGraphEdgeId;    

        struct JoinEvent{
            BaseGraphEdgeId edgeAId, edgeBId;
            BaseGraphVertexId vertexId;
            double cost;
            bool joinsTwoPaths=false;
        };

        std::vector<JoinEvent> joinEvents;

        //Go through all nodes in G...
        for(auto vId: graph::allIn(boost::vertices(graph))){
            auto itPair=boost::out_edges(vId, graph);
            //...and all pairs of edges incident to it... 
            for(auto itA=itPair.first; itA!=itPair.second; ++itA){
                auto edgeIdA=*itA;
                auto itB=itA;
                ++itB;
                for(;itB!=itPair.second; ++itB){
                    //...and create a JoinEvent from each pair.
                    auto edgeIdB=*itB;
                    joinEvents.push_back(JoinEvent{edgeIdA,edgeIdB,vId,calculateCost(graph,vId,edgeIdA,edgeIdB)});
                }
            }
        }

        
        //now sort the joinEvents, from smaller to larger.
        std::sort(joinEvents.begin(),joinEvents.end(),[](JoinEvent const& eA, JoinEvent const& eB){
            return eA.cost < eB.cost;
        });

        //and create a map of vertices with indices larger than 2   
        std::map<BaseGraphVertexId, std::vector<size_t>> vertexToEvent;

        size_t index=0;
        for(auto const& event: joinEvents){
            if(boost::degree(event.vertexId,graph) > 2){
                vertexToEvent[event.vertexId].push_back(index);
            }
            ++index;
        }

        //Create the disjoint set used to calculate the paths. 
        disjointsets::DisjointSets<BaseGraphEdgeId>  gEdgeSets;
        for(auto eId: graph::allIn(boost::edges(graph))){
            gEdgeSets.makeSet(eId);
        }

        for(auto const& event: joinEvents){
            if(event.cost > maxCost){
                break;
            }
            if(event.joinsTwoPaths){
                continue;
            }
            //join
            gEdgeSets.join(event.edgeAId, event.edgeBId);
            //check if it is a crossing, and invalidate other events if necessary
            if(boost::degree(event.vertexId,graph) > 2){
                for(auto eventIndx:vertexToEvent[event.vertexId]){
                    auto& otherEvent = joinEvents[eventIndx];
                    //if any of the edges is the same as any of the two of this event
                    if(otherEvent.edgeAId == event.edgeAId ||
                       otherEvent.edgeAId == event.edgeBId ||
                       otherEvent.edgeBId == event.edgeAId ||
                       otherEvent.edgeBId == event.edgeBId){
                        otherEvent.joinsTwoPaths=true; //prevent branches
                    }
                }
            }   
        }

        //now we derive the PathGraph. 
        PathGraphT pathGraph;
        std::map<BaseGraphEdgeId, PathGraphVertexId> edgeToPathVertexMap;
        for(auto edgeId: graph::allIn(boost::edges(graph))){
            
            if(edgeToPathVertexMap.find(edgeId) != edgeToPathVertexMap.end()){ //if already in map
                continue;
            }

            using Path =  std::vector<BaseGraphVertexId>;

            //create vertex in pathGraph and add entry to map
            auto pathGraphVertexId = boost::add_vertex(pathGraph); 
            //edgeToPathVertexMap[edgeId] = pathGraphVertexId;

            //function (lambda) for generating the straigh paths
            auto straightWalk =[&gEdgeSets, 
                                &edgeToPathVertexMap, 
                                &pathGraphVertexId,
                                &graph]
            (BaseGraphEdgeId currentEdgeId, BaseGraphVertexId currentVertexId) -> Path{
                Path path;
                bool pathContinues;
                //auto firstEdgeId = currentEdgeId; //used to avoid forever loops in circuits
                do{
                    pathContinues=false;
                    path.push_back(currentVertexId);
                    for(auto nextEdgeId: graph::allIn(boost::out_edges(currentVertexId, graph))){
                        if(nextEdgeId == currentEdgeId){
                            continue;
                        }
                        //this walks the path, and breaks if it cannot walk anymore or if the path is
                        //a complete circuit
                        //if the next edge is on the same set and the other vertex in G
                         //corresponds to the last one added (correct direction, not going back).
                       
                        if(gEdgeSets.areSameSet(currentEdgeId, nextEdgeId)){
                            //add to map
                            //check if it is already in map(then is a circuit). It is not possible 
                            //that the edge has been "walked over" (it is in the edgeToPathVertexMap) 
                            // unless it has been done in this walk (it is a circuit), because of the
                            // first test in the beginning of the loop iterating over all edges, and 
                            // since the edge in question is added at the end of the loop.
                            if(edgeToPathVertexMap.find(nextEdgeId) == edgeToPathVertexMap.end()){
                            //if(nextEdgeId != firstEdgeId ){//not circuit path, continue walking
                                //update currentEdgeId and currentVertexId
                                edgeToPathVertexMap[nextEdgeId] = pathGraphVertexId;
                                currentEdgeId=nextEdgeId;
                                currentVertexId = graph::getOtherVertex(currentEdgeId, currentVertexId, graph);
                                pathContinues=true;
                            }
 
                            break;   
                        }
                    }
                }while(pathContinues);

                //we could add the vertex to a list of breaking vertices...
                return path;
            };

            auto path1 = straightWalk( edgeId, boost::source(edgeId, graph));
            //check if it is a circuit: then the current edge has already been added to the map
            //in the walk along the path
        
            if(edgeToPathVertexMap.find(edgeId) == edgeToPathVertexMap.end()){ //if it is not a circuit       
                auto path2 = straightWalk( edgeId, boost::target(edgeId, graph));
                std::reverse(path1.begin(), path1.end());
                path1.insert(
                             path1.end(), 
                             std::make_move_iterator(path2.begin()), 
                             std::make_move_iterator(path2.end()  )
                             );
            }
            else{
                //std::cout<<"circuit"<<std::endl;
            }
            pathGraph[pathGraphVertexId] = path1;
            edgeToPathVertexMap[edgeId] = pathGraphVertexId;

        }
        //add all edges joining paths to pathGraph
        
        for(auto const& event: joinEvents){

            auto pathVertexAId=edgeToPathVertexMap[event.edgeAId];
            auto pathVertexBId=edgeToPathVertexMap[event.edgeBId];

            if(pathVertexAId != pathVertexBId){

                if(event.joinsTwoPaths){ // a crossing
                    //avoid duplicates 
                    bool exists=false;
                    for(auto pathNeigVertexId: graph::allIn(boost::adjacent_vertices(pathVertexAId,pathGraph))){
                        if (pathVertexBId == pathNeigVertexId){
                            exists=true;
                            break;
                        }
                    }
                    if(!exists){
                         boost::add_edge(pathVertexAId, pathVertexBId, event.vertexId, pathGraph);
                    }
                }
                else{ // two adjacent vertices (not crossing). No need to test if it exists
                    boost::add_edge(pathVertexAId, pathVertexBId, event.vertexId, pathGraph);
                }
            }
        } 

        // std::cout<<"Number of nodes in graph: "<< boost::num_vertices(graph)<<std::endl;
        // std::cout<<"Number of edges in graph: "<< boost::num_edges(graph)<<std::endl;

        // std::cout<<"Number of nodes in pathGraph graph: "<< boost::num_vertices(pathGraph)<<std::endl;
        // std::cout<<"Number of edges in pathGraph graph: "<< boost::num_edges(pathGraph)<<std::endl;  

        return pathGraph;               
    }

}

#endif









