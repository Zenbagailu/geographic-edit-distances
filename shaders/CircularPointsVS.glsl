
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

attribute vec3  position;
attribute vec4  color;
// Color output  to fragment shader                        
varying vec4 vs_color;
// Matrices
uniform mat4 mv_matrix;
uniform mat4 proj_matrix;

uniform float pointSize;

void main(void)
{
	gl_Position = proj_matrix * mv_matrix * vec4(position,1);
	gl_PointSize = pointSize;
	//gl_Position = vec4(position,1.0);
	vs_color=color;

} 