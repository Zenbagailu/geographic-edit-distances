
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#ifdef GL_OES_standard_derivatives
    #extension GL_OES_standard_derivatives : enable
    precision mediump float;
#endif  

                         
//inputs from the vertex buffer
varying float edge;
varying float absThick;
varying vec4 vs_color;

//in pixels. The given thickness in the vertex shader would have been ofset this amount, and used
//for creating the rectangular areas in the Vertex shader (or a geometry shader). It could be done 
//into uniforms that can be set, but in this case it is hard coded. 
const float outer = 2.5; 
//const vec3 color2 = vec3(0.8);

void main() {

  	//edge may be + or -, depending on direction, but because the interpolation is linear, and both 
  	//interpolated values are at the same distance, 0 will be the location of the line (the center
  	//of the rectangle formed by the two triangles).
  	float dis = abs(edge); 

  	//because cutoff will be negative if outer > absThick, it will make the line black in those cases
  	float cutoff = absThick-outer; 
  	//float alpha = dis < cutoff ? 1.0 : (outer- dis + cutoff)/outer; //linear:
  	float alpha = 1.0 - smoothstep(cutoff, absThick, dis); //hermite: https://thebookofshaders.com/glossary/?search=smoothstep
    gl_FragColor = vec4(vs_color.rgb,vs_color.a * alpha);
    //gl_FragColor = vs_color * alpha; //this works if alpha glending disabled

}
