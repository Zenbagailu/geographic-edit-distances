
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


//OES_standard_derivatives for use smoothstep, etc in GLSL ES 1.00
//WebGL supports it, but it needs to be anabled (Glew may take care of that if used)
#ifdef GL_OES_standard_derivatives
    #extension GL_OES_standard_derivatives : enable
#endif  

// input from the vertex buffer
precision mediump float;

varying vec4 vs_color;

void main(void)
{
	//gl_PointCoord is the coordinate of the fragment in relation to the a point
	// that is being drawn, from 0 to 1 

	vec2 cxy = 2.0 * gl_PointCoord - 1.0;
	float r=dot(cxy, cxy);
	if (r > 1.0) {
		discard;
	}

	//this compares the value of r with the pixels/fragments around 
	//http://stackoverflow.com/questions/16365385/explanation-of-dfdx#16368768
	//blend needs to be enabled...
	float delta = fwidth(r);
	
	float alpha = 1.0 - smoothstep(1.0 - delta, 1.0 + delta, r);
	//float alpha = 0.0;
	gl_FragColor = vec4(vs_color.xyz,vs_color.a * alpha);
	//gl_FragColor = vs_color;
}                              