
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

precision mediump float;
varying vec2 texcoord;
// //this corresponds to the values set in 
// //glActiveTexture();(0 if GL_TEXTURE0, 1 if  GL_TEXTURE1...etc
uniform sampler2D textureUnit; 
uniform vec4 color; //=vec4(0.0,0.0,0.0,1.0); //default value;

void main(void) {
    gl_FragColor = vec4(1, 1, 1, texture2D(textureUnit, texcoord).a) * color; //use the alpha
}


