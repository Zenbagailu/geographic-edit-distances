
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

//based on https://mattdesl.svbtle.com/drawing-lines-is-hard

attribute float thickness;
attribute float direction;
attribute vec3 position;
attribute vec3 otherPos;
attribute vec4  color;

// Matrices
uniform mat4 mv_matrix;
uniform mat4 proj_matrix;

uniform vec2 viewPort;  //(width,height)

//outputs
varying float edge;
varying float absThick;
varying vec4 vs_color;

//varying vec4 vs_color; //used now for debugging

const float outer = 2.5;  //check that it corresponds to the value used in the fragment shader!

void main() {

  float scThickness = thickness >= 0.0 ? thickness + outer : thickness - outer;
  //this considers that the alpha reductions (the outer interval) starts half-way the limit of thickness
  scThickness /= 2.0;

  float aspectRatio =  viewPort.x / viewPort.y;
  vec2 aspectVec = vec2(aspectRatio, 1.0);
  float pixelWidthRatio = 2.0 / viewPort.y;

  mat4 projView =  proj_matrix * mv_matrix;
  vec4 posProj  = projView * vec4(position,1.0);
  vec4 otherProj = projView * vec4(otherPos,1.0);


  vec3 posProjNDS  = posProj.xyz  / posProj.w ;
  vec3 otherProjNDS = otherProj.xyz / otherProj.w;

  //we use Normalised Device Space (NDS) coordinates instead, but we then need to to do the 
  //clipping ourselves. This is because calculating colour interpolation with clipping may (does) not work properly:
  //https://www.opengl.org/archives/resources/faq/technical/clipping.htm

  //THIS IS A HACK... IT SHOULD be DEALt WITH PROPERLY, INSTEAD
    if(posProj.w == 0.0){
      posProj.w = 0.00000001;
      posProjNDS  = posProj.xyz  / posProj.w ; 
    }

    if(otherProj.w == 0.0){
      otherProj.w = 0.0000001;
      otherProjNDS  = otherProj.xyz  / otherProj.w ; 
    }

  // cliping plane dimensions according to description from OpenGL SuperBible Sixth Edition, p. 38.
  // No idea why this works. It should be posProjNDS.z < 0.0 (behind view), but that does not work. 
  // This is independent of the hack above. 

  if(posProjNDS.z > 1.0){ //it needs clipping
    
    vec3 vdir= otherProjNDS - posProjNDS;
    float lambda = (0.0 - posProjNDS.z)/vdir.z; 
    posProjNDS += lambda * vdir;
  }

  //get 2D screen space with aspect correction
  vec2 posScreen  = aspectVec * posProjNDS.xy;
  vec2 nextScreen = aspectVec * otherProjNDS.xy;

  vec2 scdir = normalize(nextScreen - posScreen);

  if(otherProjNDS.z > 1.0 ){ //if it is outside the visible volume, flip.
    scdir= -scdir;
  }
 
  //if using NDS
  vec2 normal = vec2(-scdir.y, scdir.x);
  normal *= scThickness * pixelWidthRatio;
  normal.x /= aspectRatio;
  //also works fine with ortho
  posProjNDS.xy += normal.xy;
  gl_Position =  vec4(posProjNDS,1.0);

  edge = scThickness*direction;
  //absThick = abs(scThickness/2.0);
  absThick = abs(scThickness);

  vs_color = color;
}