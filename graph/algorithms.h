
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GRAPHALGORITHMS_H_
#define GRAPHALGORITHMS_H_ 

#include <map>
#include <boost/graph/breadth_first_search.hpp>
#include "allIn.h"

#include <fstream>
#include <boost/graph/graphml.hpp>
#include <boost/property_map/property_map.hpp>

#include <boost/graph/connected_components.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/copy.hpp>

#include <boost/graph/dijkstra_shortest_paths.hpp>


#include <functional>

#include <math.h>


//All these algorithms are implmented for mutable adjacency_list graphs, that is, boost::adjacency_lists
//that have boost::ListS as their container types. Immutable types (using vectors, for example) should have
//easier and more efficient algorithms. See the boost::graph examples, for those, which are used y default 
//in most cases.


//TODO:
//The algorithms should not use the GraphTraits. Rather, they should get ordinary maps for their output, 
//and create the Associative maps inside the algorithm.


namespace graph {
    //This is just a convenience traits for defining integer maps, etc. The renaming of the.
    //It allows also to easily define correct types for passing as arguments to the functions 
    
    template<class Graph> struct GraphTraits{ //just to define the containers, etc
        using vertex_descriptor             = typename boost::graph_traits<Graph>::vertex_descriptor;
        using edge_descriptor               = typename boost::graph_traits<Graph>::edge_descriptor;
        using vertices_size_type            = typename boost::graph_traits<Graph>::vertices_size_type;

        using VIdIntegerMap                 = std::map<vertex_descriptor, std::size_t> ;
        using VIdIntegerAssociativeMap      = boost::associative_property_map<VIdIntegerMap>;

        using EdgeIdFPtMap                 = std::map<GraphTraits<Graph>::edge_descriptor, double> ;
        using EdgeIdFPtAssociativeMap      = boost::associative_property_map<EdgeIdFPtMap>;

        //used in dijkstra
        using VIdFPtMap                     = std::map<vertex_descriptor, double> ;
        using VIdFPtAssociativeMap          = boost::associative_property_map<VIdFPtMap>;

        using VIdVIdMap                     = std::map<vertex_descriptor, vertex_descriptor> ;
        using VIdVIdAssociativeMap          = boost::associative_property_map<VIdVIdMap>;


    };

    // a simple convenience function...
    template<class Graph>
    typename GraphTraits<Graph>::vertex_descriptor
    getOtherVertex(typename GraphTraits<Graph>::edge_descriptor edgeId,
                   typename GraphTraits<Graph>::vertex_descriptor vertexId, 
                   Graph const& graph){
        auto otherId = boost::source(edgeId, graph);
        return otherId == vertexId ? boost::target(edgeId, graph): otherId;
    }

	template<class Graph> 
    void 
    breadthFirstSearchDistances(
                       Graph const& g,
                       typename GraphTraits<Graph>::vertex_descriptor vid,
                       typename GraphTraits<Graph>::VIdIntegerAssociativeMap & distanceMap){

        typename GraphTraits<Graph>::VIdIntegerMap vIntMap;
        typename GraphTraits<Graph>::VIdIntegerAssociativeMap vertexIndexMap(vIntMap);
        //set the association of the vertex_descriptor to indices in the map 
        //(needed if vecS is not used for vertices)
        std::size_t currentIndex=0;
        for(auto vid: allIn(boost::vertices(g))){
            vertexIndexMap[vid]=currentIndex++;
        }

        //for the way of using bgl_named_params<Param, Type, Rest>,  and its
        //strange syntax (argument type given as a function, and separated by
        //dots:
        //argumentType1(the_argument1).argumentType2(the_argument2).argumentType3(the_argument3)...
        //http://boost.sourceforge.net/libs/graph/doc/bgl_named_params.html
        boost::breadth_first_search(g, vid ,
                                    boost::visitor( //this indicates we are passing a visitor
                                                   boost::make_bfs_visitor( //creates a visitor with an on_tree_edge(), record distances
                                                                           boost::record_distances(distanceMap, boost::on_tree_edge())
                                                                     )
                                    ).vertex_index_map( //this we are passing an index_map
                                        vertexIndexMap
                                    )
                                    );
    }


    template<class Graph> 
    bool 
    saveAsGraphML(Graph& graph, std::string file){ 

       //it may be possible to use function property maps? (see type of property maps)
       // dp.property("x", get(&CartesianVertex::x, graph));  
       // dp.property("y", get(&CartesianVertex::y, graph));
       
       //here, instead of using a vertex type  with x and y, it considers a vertex type
       //that implements a [] operator. This, while a bit more complicated, allows to encapsulate
       //how coordinates are represented, for example through a topology type...
       
        using vertex_descriptor          = typename boost::graph_traits<Graph>::vertex_descriptor; 

         std::ofstream outfile(file);
        //check Bundled Properties: http://www.boost.org/doc/libs/1_54_0/libs/graph/doc/bundles.html
        //and dynamic_properties...

        boost::dynamic_properties dp;
        // dp.property("x", get(&CartesianVertex::x, graph)); 
        // dp.property("y", get(&CartesianVertex::y, graph));
        
        std::map<vertex_descriptor, double> xMap;
        boost::associative_property_map< std::map<vertex_descriptor, double> >  xCoords(xMap);

        std::map<vertex_descriptor, double> yMap;
        boost::associative_property_map< std::map<vertex_descriptor, double> >  yCoords(yMap);

        for(auto vId: graph::allIn(boost::vertices(graph))){
            auto& v = graph[vId];
            xMap[vId] = v[0];
            yMap[vId] = v[1];
        }

        dp.property("x", xCoords);  
        dp.property("y", yCoords);


        using IndexMap = std::map<vertex_descriptor, size_t>;
        IndexMap indexMap;
        boost::associative_property_map<IndexMap> indexAssociativeMap(indexMap);

        size_t ct=0;
        for(auto vId: graph::allIn(boost::vertices(graph))){
            indexMap[vId]=ct++;
        }
        
        boost::write_graphml(outfile, graph, indexAssociativeMap,dp, true);
        return true;
    }

    template<class Graph> Graph readFromGraphML(std::string file){
        
        using vertex_descriptor          = typename boost::graph_traits<Graph>::vertex_descriptor; 

        Graph graph;

        //load from file
        std::ifstream inFile;
        inFile.open(file, std::ifstream::in);

        boost::dynamic_properties dp;
    
        std::map<vertex_descriptor, double> xMap;
        boost::associative_property_map< std::map<vertex_descriptor, double> >  xCoords(xMap);

        std::map<vertex_descriptor, double> yMap;
        boost::associative_property_map< std::map<vertex_descriptor, double> >  yCoords(yMap);

        dp.property("x", xCoords);  
        dp.property("y", yCoords);
    
        try{
            boost::read_graphml(inFile, graph, dp);

        }catch(const std::exception& e){
            std::cout<<e.what()<<"\ncought in readFromGraphML"<<std::endl;
            return false;
        }
        catch(...){
            std::cout<<"unknown exception cought in readFromGraphML" <<std::endl;
            return false;
        }

        inFile.close();

        //now add data to the vertices     
        for(auto vId: graph::allIn(boost::vertices(graph))){
            auto& v = graph[vId];
            v[0] = xMap[vId];
            v[1] = yMap[vId];
        }

        return graph;
    }

    //the follwing function calculates the connected components in the graph
    //https://www.boost.org/doc/libs/1_67_0/libs/graph/doc/connected_components.html
    //and returns a filtered graph consisting of its largest connected component
    //(which could be the whole original graph)
    //https://www.boost.org/doc/libs/1_67_0/libs/graph/doc/filtered_graph.html
    //Since the filtered graph requires the graph it refers to to exist.
    
    template<class VIdMap>struct FilterIntEqual{
        FilterIntEqual() = default;
        FilterIntEqual(VIdMap const& _compMap, size_t _largestComponent):
        compMap_{_compMap}, //copy. in the case used below this will exist beyond the scope of the function.
        largestComponent_{ _largestComponent}{}
        template<class VertexDescriptor>
        bool 
        operator()(VertexDescriptor vId) const{ //it needs to be const to use it in bfs, and others
            auto elemIt = compMap_.find(vId);
            if (elemIt != compMap_.end()){
                return elemIt->second == largestComponent_;
            }
            else{
                throw (std::runtime_error("Trying to access non existent vertex in map, in FilterIntEqual."));
            }
        }
    private:
        VIdMap compMap_;
        size_t largestComponent_;
    };

    //This removes all but the largest connected component of the graph
    
    template<class Graph> 
    boost::filtered_graph<Graph,boost::keep_all,FilterIntEqual<typename GraphTraits<Graph>::VIdIntegerMap> >
    filterSmallerConnectedComponents(Graph& graph){

        
        using VIdIntegerMap             = typename GraphTraits<Graph>::VIdIntegerMap;
        using VIdIntegerAssociativeMap  = typename GraphTraits<Graph>::VIdIntegerAssociativeMap;
        //using VertexDescriptor          = typename GraphTraits<Graph>::vertex_descriptor;
        using FilterLargestComp         = FilterIntEqual<VIdIntegerMap>;
        using FilteredGraph             = boost::filtered_graph<Graph,boost::keep_all,FilterLargestComp>;

        VIdIntegerMap vIntMap;
        VIdIntegerAssociativeMap vertexIndexMap(vIntMap);
        //set the association of the vertex_descriptor to indices in the map 
        //(needed if vecS is not used for vertices)
        std::size_t currentIndex=0;
        for(auto vid: allIn(boost::vertices(graph))){
            vertexIndexMap[vid]=currentIndex++;
        } 

        VIdIntegerMap compMap;
        VIdIntegerAssociativeMap componentMap(compMap);    

        //as explained in:
        //https://stackoverflow.com/questions/15432104/how-to-create-a-propertymap-for-a-boost-graph-using-lists-as-vertex-container
        auto numOfComponents = boost::connected_components(graph, componentMap, boost::vertex_index_map(vertexIndexMap));    
        
        //find out which is largest.
        std::map<size_t,size_t> vertexCountMap;
        for (auto const& elem: compMap){
            vertexCountMap[elem.second]++; //elem.second is the integer "name" given to the component
            //std::cout<<elem.first<<": "<< elem.second<<std::endl;
        }

        size_t largestComponent;
        size_t largestComponentSize=0;
        for (auto const& componentSize: vertexCountMap){
            if(componentSize.second>largestComponentSize){
                largestComponentSize=componentSize.second;
                largestComponent=componentSize.first;
            }
        }

        //std::cout<<"The largest component is: "<< largestComponent<<" with a size of "<< largestComponentSize<<std::endl; 
        return FilteredGraph(graph, boost::keep_all(), FilterLargestComp(compMap,largestComponent));
        
    }

    //This copies a graph

    template<class Graph, class GraphCopy> 
    void 
    copyToGraph(Graph const& graph, GraphCopy& graphToCopyTo){

        using VIdIntegerMap             = typename GraphTraits<Graph>::VIdIntegerMap;
        using VIdIntegerAssociativeMap  = typename GraphTraits<Graph>::VIdIntegerAssociativeMap;

        VIdIntegerMap vertexIndexMap;
        VIdIntegerAssociativeMap vertexIndexAssociativeMap(vertexIndexMap);    
        //set the association of the vertex_descriptor to indices in the map 
        //(needed if vecS is not used for vertices)
        std::size_t currentIndex=0;
        for(auto vid: allIn(boost::vertices(graph))){
            vertexIndexMap[vid]=currentIndex++;
        } 

        boost::copy_graph(graph, graphToCopyTo, boost::vertex_index_map(vertexIndexAssociativeMap));

    }




}
#endif