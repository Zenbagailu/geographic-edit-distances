
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef TESTDIJKSTRA_H_
#define TESTDIJKSTRA_H_ 

//This is an implmntation of Dijkstra's shortest paths algorithm. 
//It is optimised so it may calculate only a portion of the graph 
//(to another point, or up to a distance, for example) by using 
//lazy initialisation of the necessary datastructures.  It also 
//keeps the size of all datastructures to the minimum necessary, 
//in order to reduce the complexity of the calculations. his is 
//faster than the boost grap Dijkstra algorithm, at least in 
//those cases. It also provides begin() end() functions that 
//return iterators over the vertex_descriptor types used, of all 
//vertices in the calculated shortest paths(which may be then a 
//subset of the vertices f the original graph).

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <queue>
#include <map>
#include <set>
#include <limits>
#include "allIn.h"


namespace graph{

    //An iterator wrapper over first elements (of an iterator that returns pairs,
    //such as the std::pair<key,value> used in maps)
    //based on:
    //https://stackoverflow.com/questions/1443793/iterate-keys-in-a-c-map/35262398#35262398
    
    template<class PairIt> 
    class FirstIterator : public PairIt{
    public:
        //assumes correspondance with std::iterator_traits    
        using FirstType = typename std::iterator_traits<PairIt>::value_type::first_type;

        FirstIterator():PairIt(){}
        FirstIterator(PairIt const& paitIt):PairIt(paitIt){}

        FirstType *operator -> ( ) { return ( FirstType * const ) &( PairIt::operator -> ( )->first ); }
        FirstType operator * ( ) { return PairIt::operator * ( ).first; }


    };

    //parametricised by a graph type and a distance type
    template<class GT, typename DT>class Dijkstra
    {
    public:
        using VertexId          = typename boost::graph_traits<GT>::vertex_descriptor;
        using EdgeId            = typename boost::graph_traits<GT>::edge_descriptor;

        using DistanceMap       = std::map<VertexId,DT>;
        using PredecessorMap    = std::map<VertexId,VertexId>;

        using KeyIterator       = FirstIterator<typename DistanceMap::iterator>;
        using KeyConstIterator  = FirstIterator<typename DistanceMap::const_iterator>;


        template<class DF, class SF> Dijkstra(
        GT const& graph, 
        DF const& edgeDisFunc, 
        VertexId source,
        SF const& stopFunc);

        // a version with a trivial default stop funtion, to calculate full graph.
        template<class DF> Dijkstra(
        GT const& graph, 
        DF const& edgeDisFunc, 
        VertexId source){Dijkstra{graph,edgeDisFunc,source,[](VertexId,DT){return false;}};}

        ~Dijkstra(){}
        //Used to perform lazy initialisation of the map, and to access it outside the class
        DT getDistance(VertexId vId); 
        //const veriosn without lazy intialisation
        DT getDistance(VertexId vId) const; 
        
        //Only used to access the map outside the class
        VertexId getPredecessor(VertexId vId)const;

        KeyIterator begin(){return KeyIterator(shortestDistances_.begin());}
        KeyIterator end(){return KeyIterator(shortestDistances_.end());}

        KeyConstIterator begin()const{return KeyConstIterator(shortestDistances_.begin());}
        KeyConstIterator end()const{return KeyConstIterator(shortestDistances_.end());}
        
    private:
        DistanceMap     shortestDistances_;
        PredecessorMap  predecessors_;

    };

    //A variation of Dijkstra using a min-priority queue:
    //https://en.wikipedia.org/wiki/Dijkstra's_algorithm
    //and implementing a lazy initialisation of the queue and set,
    //in case it is not required to calculate them for the whole graph
    //(if there is a stopping criteria, such as distance between 2 nodes
    //or the calculation of the min distance tree to a max distance)

    template<class GT, typename DT>
    template<class DF, class SF>
    Dijkstra<GT, DT>::Dijkstra(GT const& graph, 
                                DF const& edgeDisFunc, 
                                VertexId source,
                                SF const& stopFunc){

        //getDistance() //will return different values as shortestDistances_
        //gets updated. It will work because everytime a distance is updated the
        //corresponding element in the set will also be removed and inserted again in the
        //main loop in Dijkstra's algorithm below.
        auto compare = [this](VertexId vId,VertexId uId){
            return getDistance(vId) < getDistance(uId); 
        };

        //rather than a priority queue, use a set:
        //https://stackoverflow.com/questions/649640/how-to-do-an-efficient-priority-update-in-stl-priority-queue#14009760
        std::set<VertexId,decltype(compare)> unsettledNodes(compare);

        std::set<VertexId> settledNodes;

        //initialise
        shortestDistances_[source] = 0;
        unsettledNodes.insert(source);

        //main cycle:  
        // extract the node with the shortest distance
        while(!unsettledNodes.empty()){
            // VertexId vId = unsettledNodes.top();
            // unsettledNodes.pop();
            // 
            VertexId vId = *unsettledNodes.begin();
            unsettledNodes.erase(vId);
        
            //can be stopped from adding more nodes here,
            //with condition depending on vertex and/or distance.
            if(stopFunc(vId,getDistance(vId))){
                continue;
            }
            
            //for all edges (and neighbours of vId)
            for(auto eId: graph::allIn(boost::out_edges(vId,graph))){
                
                //get the neighbouring node in the edge
                auto uId = boost::source(eId, graph);
                uId =  uId == vId ? boost::target(eId, graph) : uId;

                // skip node if already settled
                if (settledNodes.find(uId) != settledNodes.end()){
                    continue;
                }
                
                float shortDist = getDistance(vId) + edgeDisFunc(eId);
                if (shortDist < getDistance(uId))
                {
                    // assign new shortest distance and mark unsettled
                    unsettledNodes.erase(uId); //it needs to be updated
                    shortestDistances_[uId] = shortDist; 
                    unsettledNodes.insert(uId); 
                    predecessors_[uId] = vId;
                }
            }     

            //when done with node add it to settledNodes
            settledNodes.insert(vId);
        }
    }

    //Lazy intialisation of shortestDistances_ map.
    template<class GT, typename DT>
    DT
    Dijkstra<GT, DT>::getDistance(VertexId vId){
        auto sDis= shortestDistances_.find(vId); 
        if(sDis == shortestDistances_.end()){
            auto& newShortDistance = shortestDistances_[vId];
            newShortDistance = std::numeric_limits<float>::max();
            return newShortDistance;
        }
        return sDis->second;
    }

    //const version does not have lazy initialisation 
    template<class GT, typename DT>
    DT
    Dijkstra<GT, DT>::getDistance(VertexId vId) const{
        auto sDis= shortestDistances_.find(vId); 
        return sDis->second;
    }


    template<class GT, typename DT>
    typename Dijkstra<GT, DT>::VertexId
    Dijkstra<GT, DT>::getPredecessor(VertexId vId)const{
        auto sDis= predecessors_.find(vId); 
        return sDis != predecessors_.end() ?  sDis->second : boost::graph_traits<GT>::null_vertex();
    }

}
#endif