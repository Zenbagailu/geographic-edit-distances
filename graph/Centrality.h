
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GRAPHCENTRALITY
#define GRAPHCENTRALITY 
#include "algorithms.h"
#include "allIn.h"
#include <boost/graph/betweenness_centrality.hpp>
namespace graph {

/**
 * because tihs class has been originally written for using with Kuhn-Munkres 
 * to calculate edit distances between nodes, it needs to return integers also in [] operator
 * Scale is used to scale the fractions then to an integer
 */

    template<class G, int Scale=1000> class Centrality 
    {
    public:
        using Graph                     = G; 
        using vertex_descriptor         = typename GraphTraits<G>::vertex_descriptor;
        using VIDIntMapIterator         = typename GraphTraits<G>::VIdIntegerMap::iterator;
        using VIDIntMapConstIterator    = typename GraphTraits<G>::VIdIntegerMap::const_iterator;

        using VIdValMap                 = std::map<vertex_descriptor, double>;
        using VIdValAssMap              = boost::associative_property_map<VIdValMap>;

        Centrality(Graph const& graph);
        Centrality( Centrality const&)=delete;
        Centrality& operator = (Centrality const&)=delete;

        //access
        //double operator double[](vertex_descriptor vid)const {return map_.at(vid);}
        /**
         * This is done so it can be used in calculating edit distance with Kunh-Munkres, which
         * in the implementation used can only take integers.
         */
        std::size_t operator[](vertex_descriptor vid)const {return static_cast<std::size_t>(Scale*map_.at(vid));}
        std::size_t size() const {return map_.size();}

        std::size_t getMax(){return max_;}
        std::size_t getMin(){return min_;}

        template<class F> void applyToAll(F&& func);

    private:
        VIdValMap map_; //regular map
        double min_;
        double max_;
    };

    template<class G, int Scale>
    Centrality<G,Scale>::Centrality(Graph const& g){
        
        min_ = std::numeric_limits<double>::max();
        max_ = -std::numeric_limits<double>::max();

        VIdValAssMap centralityVertexMap{map_};

        typename GraphTraits<Graph>::VIdIntegerMap vIntMap;
        typename GraphTraits<Graph>::VIdIntegerAssociativeMap vertexIndexMap(vIntMap);
        //set the association of the vertex_descriptor to indices in the map 
        //(needed if vecS is not used for vertices)
        std::size_t currentIndex=0;
        for(auto vid: allIn(boost::vertices(g))){
            vertexIndexMap[vid]=currentIndex++;
        }

        //using boost named parameters
        //http://boost.sourceforge.net/libs/graph/doc/bgl_named_params.html
        boost::brandes_betweenness_centrality(g,centrality_map(centralityVertexMap).vertex_index_map(vertexIndexMap)); 

        for(auto& pair : map_){
            auto cent=pair.second;
            max_= max_ < cent? cent : max_;
            min_= min_ > cent? cent : min_;
        }
    }


}
#endif          