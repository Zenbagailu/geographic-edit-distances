
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GRAPHRELATIVEASYMMETRY_H_
#define GRAPHRELATIVEASYMMETRY_H_ 
#include "algorithms.h"
#include "allIn.h"
#include <limits>
namespace graph {


    //The base graph it is associated with can be copied as long as the vertex_descriptors of the copy correspond 
    //to the ones of the original. Otherwise the vertex_descriptors in the PathGraphVertex and PathGraphEdge
    
	template<class G> class GlobalDepth
	{
	public:
		using Graph                     = G; 
        using vertex_descriptor         = typename GraphTraits<G>::vertex_descriptor;
        using VIDIntMapIterator         = typename GraphTraits<G>::VIdIntegerMap::iterator;
        using VIDIntMapConstIterator    = typename GraphTraits<G>::VIdIntegerMap::const_iterator;

		GlobalDepth(Graph const& graph);
		//GlobalDepth( GlobalDepth const&)=delete;
		//GlobalDepth& operator = (GlobalDepth const&)=delete;

		//access
		std::size_t operator[](vertex_descriptor vid)const {return map_.at(vid);}
        std::size_t at(vertex_descriptor vid)const {return map_.at(vid);}
		std::size_t	size() const {return map_.size();}

		std::size_t getMax()const{return max_;}
		std::size_t getMin()const{return min_;}

        template<class F> void applyToAll(F&& func);

        //for using standard algorithms
        // VIDIntMapIterator begin(){return map_.begin();}
        // VIDIntMapIterator end(){return map_.end();}

        // VIDIntMapConstIterator cbegin() const {return map_.begin();}
        // VIDIntMapConstIterator cend() const {return map_.end();}

	private:
		typename GraphTraits<G>::VIdIntegerMap map_; //regular map
		//just used for scaling representions (colours, etc)
		std::size_t min_;
		std::size_t max_;
	};

	template<class G>
	GlobalDepth<G>::GlobalDepth(Graph const& g){
		
		min_=std::numeric_limits<std::size_t>::max();
		max_=-std::numeric_limits<std::size_t>::max();

		//auto numNodes=boost::num_vertices(g);
		for(auto vid: allIn(boost::vertices(g))){
			std::size_t globalDepth=0;
			//the distanceMap could be cached instead,
			//if we are going to calculate shortests paths and distances

            typename GraphTraits<Graph>::VIdIntegerMap distanceMap;
			typename GraphTraits<Graph>::VIdIntegerAssociativeMap distanceAssociativeMap(distanceMap); 
            breadthFirstSearchDistances(g, vid, distanceAssociativeMap);
			for(auto p:distanceMap){
				globalDepth+=p.second;
			}
            
			max_=max_> globalDepth ? max_ : globalDepth;
			min_=min_< globalDepth ? min_ : globalDepth;

			//std::cout<<"globalDepth: "<< globalDepth << std::endl;
			map_[vid]=globalDepth; //this works
			//boost::put(map_,vid,globalDepth);
		}
	}

    template<class G>
    template<class F>
    void
    GlobalDepth<G>::applyToAll(F&& func){
        for(auto& pair : map_){
            func(pair,size());
        }
    }

    template<int Mult>struct MultipliedMeanDepth{
        template<class VIdIntMapEntry>void operator()(VIdIntMapEntry mapPair, size_t size){
            mapPair.second = static_cast<size_t>(Mult*static_cast<double>(mapPair.second)/(size-1)); 
        }
    };

    template<int Mult>struct MultipliedClosenessCentrality{
        template<class VIdIntMapEntry>void operator()(VIdIntMapEntry mapPair, size_t size){
            mapPair.second = static_cast<size_t>(Mult*((size-1)/static_cast<double>(mapPair.second))); 
        }
    };


    /**
     * These are really not used here, but are just place holders for the measures...
     */
	struct Calculation{
		virtual ~Calculation(){};
		virtual double operator()(std::size_t gD, std::size_t size)=0;
	};

	//as defined in "the Social Logic of Space"
	struct GetMeanDepth: public Calculation{
		virtual ~GetMeanDepth(){};
		virtual double operator()(std::size_t gD, std::size_t size){
			return static_cast<double>(gD)/(size-1); 
		}
	};

	//as defined in "the Social Logic of Space"
	struct GetReleativeAsymmetry: public GetMeanDepth {
		virtual double operator()(std::size_t gD, std::size_t size){
			return static_cast<double>(2*(GetMeanDepth::operator()(gD,size)-1) )/(size-2);
		}
	};


	// as defined in "The network analysis of urban streets: a primal approach" 
	// Sergio Porta, Paolo Crucitti, Vito Latora
	// and initially by 
	// Freeman, 1979:
	//"Centrality in social networks: conceptual clarification" 
	// Social Networks 1 215-239
	// Sabidussi G, 1966:
	// "The centrality index of a graph" Psychometrika 31 581-603

	//simply the inverse of mean depth

	struct GetClosenessCentrality: public Calculation{
		virtual ~GetClosenessCentrality(){};
		virtual double operator()(std::size_t gD, std::size_t size){
			return static_cast<double>(size-1)/(gD); 
		}
	};
	




}
#endif