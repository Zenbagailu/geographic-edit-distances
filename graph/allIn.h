
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef GRAPHALLIN_H_
#define GRAPHALLIN_H_ 
namespace graph {
	 //a helper for iterating graphs using C++11 for loops
    template<class P> class AllIn_t
    {
    public:
        using It = typename P::first_type; //this is for pairs describing iterators as used in boost::graph
        AllIn_t(P& p):p_{p}{}
        It begin(){return p_.first;}
        It end(){return p_.second;}
    private:
        P p_;
    };

    template<class P>
    AllIn_t<P>
    allIn(P p){
        return AllIn_t<P>{p};
    }
}
#endif