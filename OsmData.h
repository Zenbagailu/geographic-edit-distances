
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef TESTOSMDATA_H_
#define TESTOSMDATA_H_ 

#include <osm/UtmProjector.h>
#include <vector>
//to parse the json into a boost property tree
#include <boost/property_tree/ptree.hpp>
#include <utility> //for pairs
#include <string>
#include <boost/property_tree/json_parser.hpp>

namespace test {

    //for storing the attributes as pairs
    using AttributePair         = std::pair<std::string,std::string>;
    using AttributeContainer    = std::vector<AttributePair>;
    using PropertyTree          = boost::property_tree::ptree;

    AttributeContainer getAttributeContainerFromTags(PropertyTree const& tree);
    PropertyTree  getPtreeFromQuery(std::string  const& query); 


    template<class DataT>
    std::string 
    dataAsString(DataT const& data){
        std::ostringstream oss;
        
        for(auto const& attributePair: data.attributes){
            auto& tagName= attributePair.first;
            auto& tagValue= attributePair.second;
            oss <<tagName<<": "<< tagValue << "\n";
        }
        
        return oss.str();
    }

    //Only one attribute
    template<class DataT> 
    std::string 
    attributeValueAsString(DataT const& data, std::string const& attribute){
        std::ostringstream oss;
        for(auto const& attributePair: data.attributes){
            auto& tagName = attributePair.first;
            if (tagName.compare(attribute)==0){
                 auto& tagValue= attributePair.second;
                if(!tagValue.empty()){
                    oss <<tagName<<": "<< tagValue << "\n";
                }
            } 
        }  
        return oss.str();
    }

    //Attributes is a container of strings with the attributes wanted in the string that
    //implemts a find() that returns am iterator to the element (like std::set, or std::map for example).
    template<class DataT, class AttrSetT> //
    std::string 
    dataAsString(DataT const& data, AttrSetT const& attributes){
        std::ostringstream oss;
        
        for(auto const& attributePair: data.attributes){
            auto& tagName = attributePair.first;
            if(attributes.find(tagName)!= attributes.end()){
                auto& tagValue= attributePair.second;
                if(!tagValue.empty()){
                    oss <<tagName<<": "<< tagValue << "\n";
                } 
            }   
        }  
        return oss.str();
    }


    template<class Pt>struct Project{
        double centreLon;
        double centreLat;
        Pt centre;

        Project(double centreLongitude, double centreLatitude):
        centreLon{centreLongitude},
        centreLat{centreLatitude},
        centre{osm::lonLatToXY<Pt>(centreLon, centreLat, centreLon)}
        {}

        Pt operator ()(double lon, double lat) const{ //returns position relative to centre.
            auto xy=osm::lonLatToXY<Pt>(lon, lat,centreLon);
            xy[0]-=centre[0];
            xy[1]-=centre[1];
            return xy;
        }

        Pt inverse(double x, double y){
            return osm::xyToLonLat<Pt>(x+centre[0], y+centre[1], centreLon);
        }
    };

    //it should adjust to be more similar to other types of data
    struct NodeData{
        //using IdType = std::size_t; //it is 32 bits in emscripten
        using IdType = unsigned long long; 
  
        template<class Projector>NodeData(
        IdType id,
        double longitude, 
        double latitude, 
        Projector const& projector,
        PropertyTree const& tagTree);

        NodeData()=default; //it needs explicit default constructor, for the maps, for example

        ~NodeData(){}

        //getters
        IdType id()const {return id_;}
        double lon()const {return lon_;}
        double lat()const {return lat_;}
        double x()const {return x_;}
        double y()const {return y_;}
        double operator[] (IdType indx) const;
        //AttributeContainer const& data() const{return attributes;};
        //std::string dataAsString() const;
        
        //std::string const& data()const {return data_;}

        IdType id_;
        double lon_;
        double lat_;
        //std::string data_;
        AttributeContainer attributes;
        double x_;
        double y_;
    };

    template<class Projector>NodeData::NodeData(
    IdType id,
    double longitude, 
    double latitude, 
    Projector const& projector,
    PropertyTree const& tagTree):
    //std::string const& tags):
    id_{id},
    lon_{longitude},
    lat_{latitude},
    attributes{getAttributeContainerFromTags(tagTree)}{
    //data_{§}{
        auto xy=projector(lon_, lat_);
        x_=xy[0];
        y_=xy[1];
    }

    struct WayData{ 
        std::vector<NodeData::IdType> nodeIds;
        AttributeContainer attributes;
        //std::string data;
    };

    struct MultipolygonRelationData{ 
        std::vector<NodeData::IdType> outerwaysIds;
        std::vector<NodeData::IdType> innerwaysIds;
        AttributeContainer attributes;
        //std::string data;
    };   
}

#endif
