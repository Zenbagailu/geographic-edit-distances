
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef TESTGRAPHDENDOGRAM_H_
#define TESTGRAPHDENDOGRAM_H_ 

#include <string>
#include <vector>
#include <memory>


#include <graph/allIn.h>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <glm/gtc/matrix_transform.hpp>

//These are needed just by the threaded calculation.
//Otherwise they coculd be in the cpp file
//-----------------------------------------------
#include <editdistance/VertexMapContainer.h>
#include <editdistance/GraphEditDistance.h>
#include <editdistance/NodeDegreeCost.h>
#include <editdistance/NeighbourDegreeCost.h>
#include <editdistance/NodeMapCost.h>
#include <kuhnmunkres/KuhnMunkres.h>
#include <dendogram/treeDrawingFunctions.h>
//-----------------------------------------------

#include <mutex> //for multithreading

#include <thread> //for waiting thread
#include <chrono>

#include <dendogram/Dendogram.h>
#include <graph/GlobalDepth.h>


#include "straightPaths.h"
#include "largestConnectedComponent.h"
#include "straightGraphDrawingFunctions.h"
#include "dendogramDrawingFunctions.h"

#include "OsmDrawingTypes.h"
#include "OsmDataHolder.h"
#include "OsmDataSubset.h"
#include "OsmDrawQuery.h"

#include "OsmQueryFunctions.h"

// #include <random> //ONLY FOR TEST


namespace test {

    using EdgeData =  AttributeContainer;
    // Should this be instead a VertexListGraph? an EdgeListGraph?
    // (fast vertex iteration, fast edge iteration)
    using GraphT = boost::adjacency_list<           //  adjacency_list is a template depending on :
    //boost::listS,                                 
    boost::vecS,                                               
    //boost::listS,                                   
    boost::vecS,
    boost::undirectedS,                           //  directed or undirected edges ?.                      
    NodeData,                                       //  The type that describes a VertexProperties.(as Bundled properties)
    //SimpleNodeData,
    EdgeData                                        //  The type that describes an Edge. (as Bundled properties)
    > ;

    using SPGraph       = test::PathGraphDefinitions<test::GraphT>::PathGraphT;
    using SPGraphVId    = test::PathGraphDefinitions<test::GraphT>::PathGraphVertexId;
    using GlobalDepth   = graph::GlobalDepth<SPGraph>;

    //Because subsets refer to a data holder object through a pointer,
    //if the object is copied the pointer will be invalidated (if raw pointer).
    //smart pointers make sure that the object is not copied (only the pointer).
    using OsmDataPtr    = std::shared_ptr<test::OsmDataHolder>;
    using OsmSubset     = test::OsmDataSubset<OsmDataPtr>; 

    using SVertexIntMap = std::map<SPGraphVId, long long>;

    using ClusterMethod = dendogram::DistanceMatrix::Method;


    template< class OsmDataHolder,class IncFunc> 
    test::GraphT
    graphFromCentre(OsmDataHolder const& data, IncFunc const& isInFunction){
        using VertexId  = typename boost::graph_traits<test::GraphT>::vertex_descriptor;
        using EdgeId    = typename boost::graph_traits<test::GraphT>::edge_descriptor;
        std::map<typename OsmDataHolder::IdType, VertexId> osmToGraphVertex;
        test::GraphT graph;

        auto const& nodesMap = data.getNodeMap();

        for(auto const& way: data.getWays()){
            //take the 2 vertices of an edge
            auto currVerIdIter = way.nodeIds.begin();
            auto prevVerIdIter = currVerIdIter;
            ++currVerIdIter;

            auto prevIsIn = isInFunction(nodesMap.find(*prevVerIdIter)->second);
            for(; currVerIdIter != way.nodeIds.end(); ++currVerIdIter){
                auto currIsIn = isInFunction(nodesMap.find(*currVerIdIter)->second);

                if(currIsIn || prevIsIn){

                    if(osmToGraphVertex.find(*prevVerIdIter) == osmToGraphVertex.end()){
                        osmToGraphVertex[*prevVerIdIter] = boost::add_vertex(nodesMap.find(*prevVerIdIter)->second,graph);
                    }
                    
                    if(osmToGraphVertex.find(*currVerIdIter) == osmToGraphVertex.end()){
                        osmToGraphVertex[*currVerIdIter] = boost::add_vertex(nodesMap.find(*currVerIdIter)->second,graph);
                    }
                    
                    boost::add_edge(
                                    osmToGraphVertex[*prevVerIdIter],
                                    osmToGraphVertex[*currVerIdIter], 
                                    way.attributes,
                                    graph);
                }

                prevVerIdIter = currVerIdIter;
                prevIsIn = currIsIn;
            }
        }
        return graph;
    }

    struct StreetNetwork{
        test::GraphT graph;
        SPGraph pathGraph;
        GlobalDepth globalDepth;
        SVertexIntMap vertexValues;

        StreetNetwork(test::GraphT const& _graph, double angle=(test::PI/12.0)/test::PI):
        graph{_graph},
        pathGraph{test::calculateStraightPathGraph(graph,angle)},
        globalDepth{pathGraph}{

            //multiply global depth for length of path and store it in vertexValues.
            globalDepth.applyToAll([this](auto const& mapPair, size_t mapSize){//IsIn function, using polymorphic lambdas (C++14)

                auto  pVId = mapPair.first;

                double pathLength = 0;

                auto prevVIdIt = pathGraph[pVId].begin();
                auto vIdIt = prevVIdIt;
                ++vIdIt;

                while(vIdIt != pathGraph[pVId].end()){

                    auto const& prev = *prevVIdIt;
                    auto const& curr = *vIdIt;

                    auto difx =  graph[curr][0] - graph[prev][0];
                    auto dify =  graph[curr][1] - graph[prev][1];

                    auto len = std::sqrt(difx*difx + dify*dify);
                    pathLength += len;
                    prevVIdIt = vIdIt;
                    ++vIdIt;
                }   
                vertexValues[mapPair.first] = mapPair.second * pathLength;
            });  

        }

        StreetNetwork(StreetNetwork&& other): 
        graph(std::move(other.graph)),
        pathGraph(std::move(other.pathGraph)),
        globalDepth(std::move(other.globalDepth)),
        vertexValues(std::move(other.vertexValues)){}

        StreetNetwork& operator=(StreetNetwork&& other) {
            graph = std::move(other.graph);
            pathGraph = std::move(other.pathGraph); 
            globalDepth =  std::move(other.globalDepth),
            vertexValues = std::move(other.vertexValues);
            return *this;
        }
    };

    class DistanceMatrix{

    public:
        using IntType = long long;

        DistanceMatrix(size_t size=0):size_{size}{
            //using triangular number:
            distances_ = std::vector<IntType>(triangularNumber(size));  
        }

        void expand(size_t amount=1){
            size_+=amount;
            distances_.resize(triangularNumber(size_),0); //initialise to 0? 
        }

        size_t size()const{
            return size_;
        }

        IntType get(size_t a,size_t b)const{
            if (a == b){
                return 0;
            }
            else if (a<b){
                auto t=a;
                a=b;
                b=t;
            }
            return distances_[triangularNumber(a-1)+b];
        }

        void  set(size_t a,size_t b, IntType distance){
            if (a<b){
                auto t=a;
                a=b;
                b=t;
            }
            distances_[triangularNumber(a-1)+b]=distance;
        }

    private:
        //https://en.wikipedia.org/wiki/Triangular_number
        constexpr size_t triangularNumber(size_t n)const{
            return (n*(n+1)/2);
        }

    private:
        std::vector<IntType> distances_;
        size_t size_;

    };

    //This is temporary. Possibly they should be read from file, possibly as an argument in main.
    AttributeDrawingSettings createDrawingAttributes();

    class GraphDendogram
    {
    public:

        GraphDendogram(
            AttributeDrawingSettings attrdrawing,
            bool loading = true, 
            ClusterMethod currentClusteringMethod = ClusterMethod::WEIGHTED_AVERAGE):
        attributeDrawing_{attrdrawing},
        loading_{loading},
        currentClusteringMethod_{currentClusteringMethod}{}

         GraphDendogram(
            bool loading = true, 
            ClusterMethod currentClusteringMethod = ClusterMethod::WEIGHTED_AVERAGE):
        loading_{loading},
        currentClusteringMethod_{currentClusteringMethod},
        attributeDrawing_{createDrawingAttributes()}{} 
         
        virtual ~GraphDendogram(){};

        GraphDendogram( GraphDendogram const&)=delete;
        GraphDendogram& operator = (GraphDendogram const&)=delete;


        template <class Renderer> 
        double 
        renderDistanceClusters(
            Renderer& renderer,
            Renderer& textRenderer, 
            int faceSizeA,
            int faceSizeB,
            double distance,
            double dendogramScale = 0.1);

        template <class Renderer> 
        double
        renderSizedClusters(
            Renderer& renderer, 
            Renderer& textRenderer, 
            int faceSizeA,
            int faceSizeB,
            size_t clusterSize,
            double dendogramScale = 0.1);

        //Main processing functions

        //Initialisation functions
        void setCentreFromQuery(std::string const& entityQuery);
        void prepocessSubgraphs( std::string const& entityQuery, double clusteringRadius);
        //to be able to do it dynamically (as new graphs are loaded or processed).
        void addRowToDistanceMatrix();
        void calculateDendogram();

        //if necessary, it would need to use mutex while accessin dendogram_
        // double getRange(){return dendogram_.getData(dendogram_.getRoot());}

        template <class UpdateFunc>
        void
        loadAndCalculate(std::string const& graphQuery,double graphBoundarySize,double maxDegrees,UpdateFunc updateFunction);

        //for drawing.
        // size_t numberOfLeaves()const{return streetNetworks_.size();}
        //like this it returns the ones processed so far if using multithreading
        // see implementation 
        size_t numberOfLeaves();

        //the only time when size is modified is at the end of the processing, and it is the only time there 
        //may be problems with concurrency. In any case, this does not modify the size, so it may only produce 
        //problems in the display. There are none the way it is being used when first programmed.
        size_t numberOfSubsets(){return subsets_.size();}


        //Because these will called only after the corresponding dendogram exist, the indices won't be modified.
        //Otherwise a mutex would be needed also for the subsets
        OsmSubset const& getSubset(size_t index)const {return subsets_.at(index);}
        //But a mutex is needed when drawing the individual street networks, unless streetNetworks_ is not resized in "pushback,"
        //and this iterators invalidated
        StreetNetwork const& getStreetNetwork(size_t index)const {return streetNetworks_.at(index);}
        // void graphDendogram_.lockStreetNetwork(bool state){state? streetNetworkMutex_.lock() :  streetNetworkMutex_.unlock()};


        AttributeDrawingSettings const& getAttributeDrawingSettings() const{return attributeDrawing_;}
        double getGraphBoundarySize() const {return graphBoundarySize_;}
        double getMaxDistance();

        void setLoading(bool state){loading_=state;}
        bool getLoading() const{return loading_;}
       
        //it is only set here, and even if read at the same time, it should be an atomic operation so 
        //no mutex should be needed.
        void setMethod(ClusterMethod method){
            // methodMutex_.lock();
            currentClusteringMethod_=method;
            // methodMutex_.unlock();
        }


    private:
         void generateSubgraphs(std::string const& entityQuery,double clusteringRadius);

    private:
        double graphBoundarySize_;
        OsmDataPtr entityDataPtr_; //when copying, subsets need to refer to the same pointer 
        std::vector<OsmSubset> subsets_;
        std::vector<StreetNetwork> streetNetworks_;
        AttributeDrawingSettings attributeDrawing_;
        DistanceMatrix distanceMatrix_;
        dendogram::Dendogram<int> dendogram_;  

        ClusterMethod currentClusteringMethod_;

        //for multithreading 
        std::mutex matrixMutex_;
        std::mutex dendogramMutex_;

        //used for making queries in different methods
        double centreLon_;
        double referenceLat_;

        //used for controlling loading process from outside, it makes sense if
        //loading is threaded.
        bool loading_;
    };

   //Leaf drawing in a functor, so it can be reused it in different forms of rendering the dendogram.
    template<class Renderer> class LeafDrawing
    {
    public:
        LeafDrawing(GraphDendogram const& graphDendogram, 
                    Renderer& textRenderer, //this is clumsy, needs to be changed annd made consistent
                    int faceSizeA, 
                    int faceSizeB):
        graphDendogram_{graphDendogram},
        textRenderer_{textRenderer},
        faceSizeA_{faceSizeA},
        faceSizeB_{faceSizeB}{}

        virtual ~LeafDrawing(){};
        LeafDrawing( LeafDrawing const&)=delete;
        LeafDrawing& operator = (LeafDrawing const&)=delete;
        void operator()(size_t leafIndex, Renderer& renderer, double posXLeaf,double posYS)const;


    private:
        GraphDendogram const& graphDendogram_;
        Renderer& textRenderer_;
        int faceSizeA_;
        int faceSizeB_;
    };

    template<class Renderer>
    void 
    LeafDrawing<Renderer>::operator()(size_t leafIndex, Renderer& renderer, double posXLeaf,double posYS)const{

        // subsetMutex_.lock();

        auto const& subset = graphDendogram_.getSubset(leafIndex);
        auto const& centroid =subset.getCentroid();

        auto translationMatrix=glm::translate(glm::mat4(1.0), //as 2019 default constructor is 0 matrix
          {posXLeaf-centroid[0]+graphDendogram_.getGraphBoundarySize()*1.2,posYS-centroid[1],-100}); //we move everything a bit to the back...
        
        renderer.setTransformation(translationMatrix);
        textRenderer_.setTransformation(translationMatrix);

        drawData(subset,faceSizeA_, renderer, textRenderer_, graphDendogram_.getAttributeDrawingSettings(),false);

        //This is just to get string Data
        //It should really be made into a separate function that runs on creating the subset
        // std::ostringstream oss;
        std::string text;
        auto searchSt="name";

        //it prevents duplicate data from different entitites
        
        auto const& relations = subset.getRelations();
        for(auto const& relation: relations){
            auto st = test::attributeValueAsString(relation, searchSt);
            if (text.find(st) == text.npos){
                text.append(st);
            } 
        }

        auto const& ways = subset.getWays();
        for(auto const& way: ways){
            auto st = test::attributeValueAsString(way, searchSt);
             if (text.find(st) == text.npos){
                text.append(st);
            } 
        }

        auto const& nodes = subset.getNodes();
        for(auto const& node: nodes){
            auto st = test::attributeValueAsString(node, searchSt);
             if (text.find(st) == text.npos){
                text.append(st);
            } 
        }

        // subsetMutex_.unlock();
        std::istringstream input;
        // input.str(oss.str());
        input.str(text);

        std::string line;
        int ct=0;
        while( std::getline(input, line)){
            textRenderer_.renderTextInPos(centroid[0] - graphDendogram_.getGraphBoundarySize()*1.2,
             centroid[1] - graphDendogram_.getGraphBoundarySize()*1.2,
             0,
             line,
             0,
             ct*faceSizeB_);
            ++ct;
        }

        renderer.setColor(0.1,0.1,0.1,1.0);
        // graphDendogram_.lockStreetNetwork(true);
        auto const& network = graphDendogram_.getStreetNetwork(leafIndex);
        //we draw them a bit "higher" so the graphs are on the foreground and draw over areas, etc.
        test::drawPathGraphDepth(network.graph,network.pathGraph,network.globalDepth,renderer,  10.0);
        // graphDendogram_.lockStreetNetwork(false);

        //reset translations
        renderer.setTransformation(glm::mat4(1.0)); //as 2019 default constructor of mat4 produces null matrix
        textRenderer_.setTransformation(glm::mat4(1.0));

    }

    template <class Renderer> 
    double 
    GraphDendogram::renderDistanceClusters(Renderer& renderer, Renderer& textRenderer, int faceSizeA, int faceSizeB, double distance, double dendogramScale){
        LeafDrawing<Renderer> leafDrawing(*this, textRenderer, faceSizeA, faceSizeB);
        double span=0;
        dendogramMutex_.lock();
        if(dendogram_.ready()){
            span = test::drawDendogramClustersByDistance(dendogram_, distance, renderer, leafDrawing, graphBoundarySize_*2, dendogramScale);
        }
        dendogramMutex_.unlock();
        return span;
    }

    template <class Renderer> 
    double
    GraphDendogram::renderSizedClusters(Renderer& renderer, Renderer& textRenderer, int faceSizeA, int faceSizeB, size_t clusterSize, double dendogramScale){
        LeafDrawing<Renderer> leafDrawing(*this, textRenderer, faceSizeA, faceSizeB);
        // auto leafDrawing = [](size_t,Renderer&, double, double){}; //dummy function, for tests.
        double span=0;
        dendogramMutex_.lock();
        if(dendogram_.ready()){
            span = test::drawDendogramClustersBySize(dendogram_, clusterSize, renderer, leafDrawing, graphBoundarySize_*2, dendogramScale);
        }
        dendogramMutex_.unlock();
        return span;
    }

     //it can be used in a thread with an update function
    template <class UpdateFunc> 
    void 
    GraphDendogram::loadAndCalculate(
        std::string const& graphQuery,
        double graphBoundarySize,
        double maxDegrees,
        UpdateFunc updateFunction){

        try { //because it will run in a n independent thread it needs specifc try catch distinctive from main thread

            graphBoundarySize_ = graphBoundarySize; //used for placing and calculating size of drawings
            //With map
            editdistance::NodeMapCost costCalculator;

            //-----------------------------------------------------------------------------------------------------
            //This goes through each subgraph, creates a query for its area, retrieves and calculates the graph, and
            //checks its validity. If invalid, it removes the subgraph from the list in an effiecent way. If valid, 
            //it adds the graph to streetNetworks_, and calculate the distances.
            //-----------------------------------------------------------------------------------------------------

            auto graphQueryBase = removeBoundingBoxFromBoundingBoxQuery(graphQuery);
            Project<std::array<double,3>>project{centreLon_, referenceLat_};


            //This is very important. Otherwise streetNetworks_.push_back() may invalidate exsiting iterators to it from other threads,
            //and make it crash.
            streetNetworks_.reserve(subsets_.size()); //the masimum size it could have, if all subsets are valid


            //AN EFFICIENT METHOD, BASED ON ERASE MOVE IDIOM: https://en.wikipedia.org/wiki/Erase–remove_idiom
            //It makes the request for the new graph and if valid, it adds and stores 

            auto currentIt = subsets_.begin();
            auto tailIt = subsets_.end();
            size_t count=0;
            while(currentIt != tailIt) {

                if(loading_){ //simple way of setting it active or inactive from outside, like another thread.
                    
                    auto const& subset = *currentIt;
                    auto const& subSetCentre = subset.getCentroid();

                    //Build bounding box for query specific for the area in question.
                    //First it needs to get the referenceLat_ and centreLon_ translatated to xy (mercator projection)
                    //this is done in OsmDataHolder with a Project functor. We do same thing here:
         
                    auto south =  project.inverse(subSetCentre[0]                      , subSetCentre[1] - graphBoundarySize_ )[1];
                    auto west  =  project.inverse(subSetCentre[0] - graphBoundarySize_ , subSetCentre[1]                      )[0];
                    auto north =  project.inverse(subSetCentre[0]                      , subSetCentre[1] + graphBoundarySize_ )[1];
                    auto east  =  project.inverse(subSetCentre[0] + graphBoundarySize_ , subSetCentre[1]                      )[0];

                    auto boundedGraphQuery = addBoundingBoxToQuery(graphQueryBase, south, west, north, east);   
                    test::OsmDataHolder graphData = test::OsmDataHolder(boundedGraphQuery, referenceLat_, centreLon_);

                    auto graph = graphFromCentre(graphData,
                                                     [&subSetCentre, this](auto const& pt){//IsIn function, using polymorphic lambdas (C++14)
                                                        //a circle
                                                        float radius = graphBoundarySize_;
                                                        auto dx = pt[0] - subSetCentre[0];
                                                        auto dy = pt[1] - subSetCentre[1];
                                                        auto disSq = dx*dx + dy*dy;
                                                        return disSq <= radius * radius;
                                                    }                               
                    ); 


                    //the cost of these calculations for invalid (very small) graphs will be neglectable, anyway.
                    auto largestCGraph = test::createLargestComponentGraph(graph);
                    StreetNetwork stNet(std::move(largestCGraph), (test::PI*maxDegrees/180)/test::PI); //the cost is given in decimals, 0->1.

                    if(boost::num_vertices(stNet.pathGraph) <= 1 || boost::num_edges(stNet.pathGraph) <=1) { //delete subgraph
                        --tailIt;
                        std::swap(*currentIt, *tailIt);   
                    }
                    else{ //add graph to street networks, 
                        // streetNetworkMutex_.lock();
                        streetNetworks_.push_back(std::move(stNet));  //this may invalidate any iterator to streetNetworks_
                        // streetNetworkMutex_.unlock();

                        //CALCULATE DISTANCES HERE!!!
                        addRowToDistanceMatrix();
                        calculateDendogram();
                        // updateFunction(count);
                        //it should really use a mutex to access cout, just in case it is used by another thread
                        std::cout<<"costs calculated for: " << streetNetworks_.size()-1 <<std::endl;

                        ++currentIt;
                    }
                    updateFunction(count); //called every time, even when no updated dendogram (it will just force a redraw)

                    count++;//just ot keep track of preocessed subgraphs
                }
                else { //if loading paused:
                    std::this_thread::sleep_for(std::chrono::milliseconds(200));
                }
            }

            subsets_.erase(tailIt,subsets_.end());


        }catch (const std::exception& e){
            std::cerr << e.what() << std::endl;
        }
    }
    
}
#endif