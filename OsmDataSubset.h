
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef TESTOSMDATASUBSET
#define TESTOSMDATASUBSET 

#include "OsmData.h" //for OsmDataSubset

#include <unordered_map>
#include <vector>
#include <array>

//These hold subsets of data from a OsmDataHolder (accessed through a pointer)
namespace test{

    template<class DataPtr> class OsmDataSubset
    {
    public:
        using IdType = typename DataPtr::element_type::IdType;

        OsmDataSubset(DataPtr dataPtr): dataPtr_{dataPtr}{};
        virtual ~OsmDataSubset(){};

        //Same interface as OsmDataHolder
        std::unordered_map<IdType,NodeData> const& getNodeMap()const{return dataPtr_->getNodeMap();}
        std::unordered_map<IdType,WayData> const& getWayMap()const{return dataPtr_->getWayMap();}

        std::vector<MultipolygonRelationData> const& getRelations()const{return relations_;}
        std::vector<WayData> const& getWays() const{return ways_;}
        std::vector<NodeData> const& getNodes() const{return nodes_;}   

        void addRelation(MultipolygonRelationData const& relation){relations_.push_back(relation);}
        void addWay(WayData const& way){ways_.push_back(way);}
        void addNode(NodeData const& node){nodes_.push_back(node);}     

        template<class RelIt>
        void 
        addRelations(RelIt begin, RelIt end){std::copy(begin,end,std::back_inserter(relations_));}

        template<class WayIt>
        void 
        addWays(WayIt begin, WayIt end){std::copy(begin,end,std::back_inserter(ways_));}

        template<class NodeIt>
        void 
        addNodes(NodeIt begin, NodeIt end){std::copy(begin,end,std::back_inserter(nodes_));}

        std::array<float,3>  const& getCentroid() const {return centroid_;}

        void calculateCentroid(); 

    private:
        DataPtr dataPtr_;
        std::array<float,3> centroid_;
        std::vector<MultipolygonRelationData> relations_; //since it is highest order, it does not need a map
        std::vector<NodeData> nodes_;
        std::vector<WayData> ways_; 
    };

    template<class NodeIds, class NodeMap>
    std::pair<std::array<float,3>,float>  //return centroid and area
    calculateWayAreaCentroid(NodeIds const& nodeIds, NodeMap const& nodeMap){
        std::array<float,3> wayCentre={0,0,0};
        float signedArea = 0.0f;
        auto prevNodeId = nodeIds.back();
        for(auto nodeId: nodeIds){
            auto const& prNode = nodeMap.at(prevNodeId);
            auto const& acNode = nodeMap.at(nodeId);
            auto a = prNode[0]*acNode[1] - prNode[1]*acNode[0];
            signedArea += a;
            wayCentre[0] += (prNode[0] + acNode[0])*a;
            wayCentre[1] += (prNode[1] + acNode[1])*a;
            prevNodeId=nodeId;
        }    
        signedArea /= 2;
        wayCentre[0] /= (6.0f*signedArea);
        wayCentre[1] /= (6.0f*signedArea);

        return {wayCentre,signedArea};
    }

    template<class DataPtr> 
    void 
    OsmDataSubset<DataPtr>::calculateCentroid(){
        centroid_={0,0,0};


        //calculate centroid for relations (for outer ways).
        auto const& wayMap=dataPtr_->getWayMap();
        auto const& nodeMap = dataPtr_->getNodeMap();
        std::array<float,3> relationsCentre={0,0,0};
        float relationsArea = 0;

        for(auto const& relation: relations_){

            std::array<float,3> waysCentre={0,0,0};
            float relationArea = 0;
            for(auto id : relation.outerwaysIds){
                auto const& way=wayMap.at(id);
                auto wayCentre = calculateWayAreaCentroid(way.nodeIds, nodeMap);
                auto wayArea = wayCentre.second > 0 ? wayCentre.second: -wayCentre.second; 
                waysCentre[0] += wayCentre.first[0] * wayArea;  //add them multiplied by area
                waysCentre[1] += wayCentre.first[1] * wayArea;
                relationArea += wayArea; 
            }
            relationsArea += relationArea;
            relationsCentre[0] += waysCentre[0]; //add them multiplied by area
            relationsCentre[1] += waysCentre[1];
            
        }


        //calculate centroid for ways
        std::array<float,3> waysCentre={0,0,0};
        float waysArea = 0;
        for(auto const& way: ways_){
            auto wayCentre = calculateWayAreaCentroid(way.nodeIds, nodeMap);
            auto wayArea = wayCentre.second > 0 ? wayCentre.second: -wayCentre.second; 
            waysCentre[0] += wayCentre.first[0] * wayArea;  //add them multiplied by area
            waysCentre[1] += wayCentre.first[1] * wayArea;
            waysArea += wayArea; 
        }

        //calculate centroid for points.
        std::array<float,3> nodesCentre={0,0,0};
        for(auto const& node: nodes_){
            nodesCentre[0] += node[0];
            nodesCentre[1] += node[1];
        } 
        

        //it possibly should be weighted. The ways and relations are easy to weight
        //with the areas, points should be given some area equivalent
        int ct=0;
        if(relationsArea>0){
            relationsCentre[0] /= relationsArea; 
            relationsCentre[1] /= relationsArea;

            centroid_[0] += relationsCentre[0];
            centroid_[1] += relationsCentre[1];
            ++ct;
        }

        if(waysArea>0){
            waysCentre[0] /= waysArea;
            waysCentre[1] /= waysArea;

            centroid_[0] += waysCentre[0];
            centroid_[1] += waysCentre[1];
            ++ct;
        }
       
        if(nodes_.size() > 0){
            nodesCentre[0] /= nodes_.size();
            nodesCentre[1] /= nodes_.size();

            centroid_[0] += nodesCentre[0];
            centroid_[1] += nodesCentre[1];
            ++ct;
        }

        if(ct>0){
            centroid_[0] /= ct;
            centroid_[1] /= ct;
        }    
    }

    template<class DataPtr> 
    std::vector<OsmDataSubset<DataPtr>> 
    makeSubsetsFromDatataHolder(DataPtr dataPtr){
        using OsmSubset = OsmDataSubset<DataPtr>;
        std::vector<OsmSubset> subsets;

        for (auto const& relation: dataPtr->getRelations()){
            OsmSubset subSet(dataPtr);
            subSet.addRelation(relation);
            subsets.push_back(subSet);
        }

        for (auto const& way: dataPtr->getWays()){
            OsmSubset subSet(dataPtr);
            subSet.addWay(way);
            subsets.push_back(subSet);
        }

        for (auto const& node: dataPtr->getNodes()){
            OsmSubset subSet(dataPtr);
            subSet.addNode(node);
            subsets.push_back(subSet);    
        }

        //now calculate centroids
        for(auto& subset: subsets){
            subset.calculateCentroid();
        }

        return subsets;   
    }

    template<class Subset> 
    Subset 
    unionSubset(Subset const& subsetA, Subset const& subsetB){
        Subset united = subsetA;
        auto const& relationsToCopy = subsetB.getRelations();
        united.addRelations(relationsToCopy.begin(),relationsToCopy.end());
        auto const& waysToCopy = subsetB.getWays();
        united.addWays(waysToCopy.begin(),waysToCopy.end());
        auto const& nodesToCopy = subsetB.getNodes();
        united.addNodes(nodesToCopy.begin(),nodesToCopy.end());
        return united;
    }
}

#endif