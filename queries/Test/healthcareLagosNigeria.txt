//Get amenity (amenity = arts_centre, cinema, community_centre,planetarium, public_bookcase, social_centre,theatre,conference_centre,internet_cafe,place_of_worship, library)
//Medical:(amenity = clinic, dentist, doctors, hospital, nursing_home)
 
[out:json] 
[bbox:6.3747,2.9993,6.9080,3.7477];  //Lagos (Nigeria) S,W,N,E 
//Get all buildings inside ways and relations that are gas stations (by making them into areas through map_to_area).
//The problem with this is that for making the area (finding the corresponding area, really) the ways need to have
//a name. Without name, the areas won't be considered

(
    //all ways that are that are marked as:
    way[amenity~"clinic|doctors|hospital"][!"building"];
    rel[amenity~"clinic|doctors|hospital"][!"building"]; 
);

map_to_area->.areas; 
(
    rel(area.areas)[building];>;
    way(area.areas)[building];>;
 );
out;

(
 //all ways that are marked as:
    way[amenity~"clinic|doctors|hospital"];>;
    rel[amenity~"clinic|doctors|hospital"];>;    
 );
out;

//Get all buildings (areas) that contain points marked as:
(
    node["amenity"~"clinic|doctors|hospital"];        
) -> .amNodes;

.amNodes out; //just include them, anyway.

//This only works if the building has a name (and then is an area).
.amNodes is_in;
area._[building];
(
  way(pivot);>;
  rel(pivot);>;
);
out;

//this gets all buildings 10 meters around the node (which includes wrong buildings)
(
  way["building"](around.amNodes:10);>;
  relation["building"](around.amNodes:10);>;

);
out;


