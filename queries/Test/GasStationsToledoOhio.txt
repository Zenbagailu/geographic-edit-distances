
//Get Gas stations (amenity = fuel)
[out:json]
[bbox:41.4144,-84.0248,41.8158,-83.2764];   //Toledo S,W,N,E 

//Get all buildings inside ways and relations that are gas stations (by making them into areas through map_to_area).
//The problem with this is that for making the area (finding the corresponding area, really) the ways need to have
//a name. Without name, the areas won't be considered

(
    //all ways that are that are marked as fuel that are not buildings
    way[amenity~"fuel"][!"building"];
    rel[amenity~"fuel"][!"building"];>; 
);

map_to_area->.areas; 
(
    rel(area.areas)[building];>;
    way(area.areas)[building];>;
 );
out;

 //all ways that are that are marked as fuel
(
    way[amenity~"fuel"];>;
    rel[amenity~"fuel"];>;    
);
out;

//Get all buildings (areas) that contain points marked as fuel
//Get the nodes.
(
    node["amenity"~"fuel"];        
) -> .fuelNodes;

.fuelNodes out; //Include them also.

//This only works if the building has a name (and then is an area).
.fuelNodes is_in;
area._[building];
(
  way(pivot);>;
  rel(pivot);>;
);
out;

//this gets all buildings 10 meters around the node (which includes wrong buildings)
(
  way["building"](around.fuelNodes:10);>;
  relation["building"](around.fuelNodes:10);>;
);
out;
