
//Get amenity (amenity = arts_centre, cinema, community_centre,planetarium, public_bookcase, social_centre, theatre, conference_centre, internet_cafe, place_of_worship, library)
//Medical:(amenity = clinic, dentist, doctors, hospital, nursing_home)

[out:json]  
[bbox:41.7931,12.3284,41.9929,12.7026];  //Rome S,W,N,E 

//Get all buildings inside ways and relations that are gas stations (by making them into areas through map_to_area).
//The problem with this is that for making the area (finding the corresponding area, really) the ways need to have
//a name. Without name, the areas won't be considered

(
    //all ways that are that are marked as fuel
    way[amenity~"place_of_worship"][!"building"];
    rel[amenity~"place_of_worship"][!"building"]; 
);

map_to_area->.areas; 
(
    rel(area.areas)[building];>;
    way(area.areas)[building];>;
 );
out;

(
 //all ways that are marked as fuel
    way[amenity~"place_of_worship"];>;
    rel[amenity~"place_of_worship"];>;    
 );
out;

//Get all buildings (areas) that contain points marked as fuel
(
    node["amenity"~"place_of_worship"];        
) -> .amNodes;

.amNodes out; //just include them, anyway.

//This only works if the building has a name (and then is an area).
.amNodes is_in;
area._[building];
(
  way(pivot);>;
  rel(pivot);>;
);
out;

//this gets all buildings 10 meters around the node (which includes wrong buildings)
(
  way["building"](around.amNodes:10);>;
  relation["building"](around.amNodes:10);>;

);
out;


