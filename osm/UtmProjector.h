
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


// Transform from WGS84 to UTM (Universal Trasnverse Mercator) projection. UTM reduces distorsion of projection
// SWEREF 99, used in Sweden, is a modified, more precise version of UTM.
//Based on javascript code from: http://home.hiwaay.net/~taylorc/toolbox/geography/geoutm.html
#ifndef OSMUTMPROJECTOR_H_
#define OSMUTMPROJECTOR_H_ 

#include <cmath>
#include <stdexcept>
#include <iostream>

namespace osm{

    constexpr double PI = 3.14159265358979;

    /* Ellipsoid model constants (actual values here are for WGS84) */
    const double sm_a = 6378137.0;                  //Ellipsoid model major axis.
    const double sm_b = 6356752.314;                //Ellipsoid model minor axis.
    const double sm_EccSquared = 6.69437999013e-03;
    const double UTMScaleFactor = 0.9996;

    constexpr double degToRad (double deg){
        return (deg / 180.0 * PI);
    }

    constexpr double radToDeg (double rad){
        return (rad / PI * 180.0);
    }

    double arcLengthOfMeridian (double phi);
    double utmCentralMeridian (int zone);
    double footpointLatitude (double iY);

    /*
    * mapLatLonToXY
    *
    * Converts a latitude/longitude pair to x and y coordinates in the
    * Transverse Mercator projection.  Note that Transverse Mercator is not
    * the same as UTM; a scale factor is required to convert between them.
    *
    * Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
    * GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
    *
    * Inputs:
    *    phi - Latitude of the point, in radians.
    *    lambda - Longitude of the point, in radians.
    *    lambda0 - Longitude of the central meridian to be used, in radians.
    *
    * Returns:
    *    A PtT (which should be able to take an intialisation list) containing 
    *    the x and y coordinates of the computed point.
    *    
    */
   
    template<class PtT>
    PtT mapLatLonToXY (double phi, double lambda, double lambda0){
  
        double ep2 = (std::pow (sm_a, 2.0) - std::pow (sm_b, 2.0)) / std::pow (sm_b, 2.0);
        double nu2 = ep2 * std::pow (std::cos (phi), 2.0);
        double N = std::pow (sm_a, 2.0) / (sm_b * std::sqrt (1 + nu2));
        double t = std::tan (phi);
        double t2 = t * t;
        //double tmp = (t2 * t2 * t2) - std::pow (t, 6.0);
        double l = lambda - lambda0;

        // Precalculate coefficients for l**n  l**1 and l**2 have coefficients of 1.0 
        double l3coef = 1.0 - t2 + nu2;

        double l4coef = 5.0 - t2 + 9 * nu2 + 4.0 * (nu2 * nu2);

        double l5coef = 5.0 - 18.0 * t2 + (t2 * t2) + 14.0 * nu2
            - 58.0 * t2 * nu2;

        double l6coef = 61.0 - 58.0 * t2 + (t2 * t2) + 270.0 * nu2
            - 330.0 * t2 * nu2;

        double l7coef = 61.0 - 479.0 * t2 + 179.0 * (t2 * t2) - (t2 * t2 * t2);

        double l8coef = 1385.0 - 3111.0 * t2 + 543.0 * (t2 * t2) - (t2 * t2 * t2);

        // Calculate easting (x) 
        double x = N * std::cos (phi) * l
            + (N / 6.0 * std::pow (std::cos (phi), 3.0) * l3coef * std::pow (l, 3.0))
            + (N / 120.0 * std::pow (std::cos (phi), 5.0) * l5coef * std::pow (l, 5.0))
            + (N / 5040.0 * std::pow (std::cos (phi), 7.0) * l7coef * std::pow (l, 7.0));

        // Calculate northing (y) 
        double y = arcLengthOfMeridian (phi)
            + (t / 2.0 * N * std::pow (std::cos (phi), 2.0) * std::pow (l, 2.0))
            + (t / 24.0 * N * std::pow (std::cos (phi), 4.0) * l4coef * std::pow (l, 4.0))
            + (t / 720.0 * N * std::pow (std::cos (phi), 6.0) * l6coef * std::pow (l, 6.0))
            + (t / 40320.0 * N * std::pow (std::cos (phi), 8.0) * l8coef * std::pow (l, 8.0));

        return {x,y};
    }



    /*
    * mapXYToLatLon
    *
    * Converts x and y coordinates in the Transverse Mercator projection to
    * a latitude/longitude pair.  Note that Transverse Mercator is not
    * the same as UTM; a scale factor is required to convert between them.
    *
    * Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
    *   GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
    *
    * Inputs:
    *   x - The easting of the point, in meters.
    *   y - The northing of the point, in meters.
    *   lambda0 - Longitude of the central meridian to be used, in radians.
    *
    * Returns:
    *    xy - A PtT (which should be able to take an intialisation list) containing 
    *    the latitude and longitude in radians.
    *
    * Remarks:
    *   The local variables nf, nuf2, tf, and tf2 serve the same purpose as
    *   n, nu2, t, and t2 in mapLatLonToXY, but they are computed with respect
    *   to the footpoint latitude phif.
    *
    *   x1frac, x2frac, x2poly, x3poly, etc. are to enhance readability and
    *   to optimize computations.
    *
    */
   
    template<class PtT>
    PtT mapXYToLatLon (double x, double y, double lambda0){
    
        // Get the value of phif, the footpoint latitude.
        double phif = footpointLatitude (y);      
    
        double ep2 = (std::pow (sm_a, 2.0) - std::pow (sm_b, 2.0))
              / std::pow (sm_b, 2.0);     
 
        double cf = std::cos (phif);
            
        double nuf2 = ep2 * std::pow (cf, 2.0);
            
        double nf= std::pow (sm_a, 2.0) / (sm_b * std::sqrt (1 + nuf2));
        double nfPow = nf; //initialise
            
        double tf = std::tan (phif);
        double tf2 = tf * tf;
        double tf4 = tf2 * tf2;
        
        // fractional coefficients for x**n in the equations
        double x1frac = 1.0 / (nfPow * cf);
        
        nfPow *= nf;   // now equals nf**2)
        double x2frac = tf / (2.0 * nfPow);
        
        nfPow *= nf;   // now equals nf**3)
        double x3frac = 1.0 / (6.0 * nfPow * cf);
       
        nfPow *= nf;   // now equals nf**4)
        double x4frac = tf / (24.0 * nfPow);

        nfPow *= nf;   // now equals nf**5)
        double x5frac = 1.0 / (120.0 * nfPow * cf);
        
        nfPow *= nf;   // now equals nf**6)
        double x6frac = tf / (720.0 * nfPow);
        
        nfPow *= nf;   // now equals nf**7)
        double x7frac = 1.0 / (5040.0 * nfPow * cf);
        
        nfPow *= nf;   // now equals nf**8)
        double x8frac = tf / (40320.0 * nfPow);
        
        // polynomial coefficients for x**n. x**1 does not have a polynomial coefficient.
        double x2poly = -1.0 - nuf2;
        
        double x3poly = -1.0 - 2 * tf2 - nuf2;
        
        double x4poly = 5.0 + 3.0 * tf2 + 6.0 * nuf2 - 6.0 * tf2 * nuf2
            - 3.0 * (nuf2 *nuf2) - 9.0 * tf2 * (nuf2 * nuf2);
        
        double x5poly = 5.0 + 28.0 * tf2 + 24.0 * tf4 + 6.0 * nuf2 + 8.0 * tf2 * nuf2;
        
        double x6poly = -61.0 - 90.0 * tf2 - 45.0 * tf4 - 107.0 * nuf2
            + 162.0 * tf2 * nuf2;
        
        double x7poly = -61.0 - 662.0 * tf2 - 1320.0 * tf4 - 720.0 * (tf4 * tf2);
        
        double x8poly = 1385.0 + 3633.0 * tf2 + 4095.0 * tf4 + 1575 * (tf4 * tf2);
            
        // Calculate latitude
        double phiLambdaX = phif + x2frac * x2poly * (x * x)
            + x4frac * x4poly * std::pow (x, 4.0)
            + x6frac * x6poly * std::pow (x, 6.0)
            + x8frac * x8poly * std::pow (x, 8.0);
            
        // Calculate longitude
        double phiLambdaY = lambda0 + x1frac * x
            + x3frac * x3poly * std::pow (x, 3.0)
            + x5frac * x5poly * std::pow (x, 5.0)
            + x7frac * x7poly * std::pow (x, 7.0);
            
        return{phiLambdaX, phiLambdaY};
    }

    /**
    * latLonToUTMXY
    *
    * Converts a latitude/longitude pair to x and y coordinates in the
    * Universal Transverse Mercator projection.
    *
    * Inputs:
    *   lat - Latitude of the point, in radians.
    *   lon - Longitude of the point, in radians.
    *   zone - UTM zone to be used for calculating values for x and y.
    *          If zone is less than 1 or greater than 60, the routine
    *          will determine the appropriate zone from the value of lon.
    *
    * Returns:
    *   xy - the UTM x and y values .
    *
    */
    
    template<class PtT> PtT latLonToUTMXY (double lat, double lon, int zone)
    {
        auto xy = mapLatLonToXY<PtT> (lat, lon, utmCentralMeridian(zone));

        /* Adjust easting and northing for UTM system. */
        xy[0] = xy[0] * UTMScaleFactor + 500000.0;
        xy[1] = xy[1] * UTMScaleFactor;
        if (xy[1] < 0.0)
            xy[1] = xy[1] + 10000000.0;

        return xy;
    }

    /**
    * utmXYToLatLon
    *
    * Converts x and y coordinates in the Universal Transverse Mercator
    * projection to a latitude/longitude pair.
    *
    * Inputs:
    *   x - The easting of the point, in meters.
    *   y - The northing of the point, in meters.
    *   zone - The UTM zone in which the point lies.
    *   southhemi - True if the point is in the southern hemisphere;
    *               false otherwise.
    *
    * Returns:
    *   latlon - A 2-element array containing the latitude and
    *            longitude of the point, in radians.
    *
    */
   
    template<class PtT> PtT utmXYToLatLon (double x, double y, int zone, bool southhemi)
    {   
        x -= 500000.0;
        x /= UTMScaleFactor;
            
        /* If in southern hemisphere, adjust y accordingly. */
        if (southhemi){
            y -= 10000000.0;
        }
                
        y /= UTMScaleFactor;
        
        double cmeridian = utmCentralMeridian (zone);
        return mapXYToLatLon<PtT> (x, y,cmeridian);
    }


    template<class PtT> PtT lonLatToUTM(double lon, double lat)
    {
       
        if ((lon < -180.0) || (180.0 <= lon)) {
            throw(std::invalid_argument("The longitud is out of range (-180, 180)"));
        }

        if ((lat < -90.0) || (90.0 < lat)) {
            throw(std::invalid_argument("The latitude is out of range (-90, 90)"));
        }

        // Compute the UTM zone.
        double zone = std::floor ((lon + 180.0) / 6) + 1;
       
        auto xy = latLonToUTMXY<PtT> (degToRad(lat), degToRad(lon), zone);
        if (lat < 0){
            //Southern Hemisphere
        }    
        else{
            //Northern Hemisphere
        }
           
        return xy;
    }

    /**
     * This is added to transform using a central meridian as reference
     * this is in order to avoid problems with areas between two zones
     */


    template<class PtT> PtT lonLatToXY(double lon, double lat, double centralMeridian)
    {
       
        if ((lon < -180.0) || (180.0 <= lon)) {
            throw(std::invalid_argument("The longitud is out of range (-180, 180)"));
        }

        if ((lat < -90.0) || (90.0 < lat)) {
            throw(std::invalid_argument("The latitude is out of range (-90, 90)"));
        }

        if ((centralMeridian < -180.0) || (180.0 < centralMeridian)) {
            throw(std::invalid_argument("The centralMeridian is out of range (-180, 180)"));
        }

       
        auto xy = mapLatLonToXY<PtT>(degToRad(lat), degToRad(lon), degToRad(centralMeridian));
        if (lat < 0){
            //Southern Hemisphere
        }    
        else{
            //Northern Hemisphere
        }
           
        return xy;
    }

    template<class PtT> PtT utmToGeographic (double x, double y, int zone, bool southhemi)
    {                                  
        
        if ((zone < 1) || (60 < zone)) {
            throw(std::invalid_argument("The UTM zone is out of range (1, 60)"));
        }
        
        auto geog = utmXYToLatLon<PtT>(x, y, zone, southhemi);

        geog[0] = radToDeg(geog[0]);
        geog[1] = radToDeg(geog[1]);

        return geog;
    }

    //it hides rads conversions since convention is in degrees, and inverts lon lat.
    template<class PtT> PtT xyToLonLat(double x, double y, double centralMeridian)
    {
        auto result = osm::mapXYToLatLon<PtT>(x, y, degToRad(centralMeridian));
        return {radToDeg(result[1]),radToDeg(result[0])}; //invert lon lat!
    }
}


#endif



