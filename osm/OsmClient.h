
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


//Based on sync_client.cpp example from boost::asio, by  Christopher M. Kohlhoff

#ifndef OSMOSMCLIENT_H_
#define OSMOSMCLIENT_H_ 

#include <string>


namespace osm{

    class OsmClient
    {
    public:
        //Known overpass servers (october 2017) from 
        //https://blog-en.openalfa.com/how-to-query-openstreetmap-using-the-overpass-api
        //
        //http://overpass.osm.rambler.ru
        //http://www.overpass-api.de
        //http://api.openstreetmap.fr
        
        OsmClient(std::string server="overpass-api.de");
        //development server with some extra features, not working
        //OsmClient(std::string server="http://dev.overpass-api.de/api_mmd/"); 

        virtual ~OsmClient(){}
        OsmClient( OsmClient const&)=delete;
        OsmClient& operator = (OsmClient const&)=delete;
        std::string processQuery(std::string querySt);
    private:
        std::string server_;
    };

}

#endif