
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "UtmProjector.h"


namespace osm{

    /**
     * arcLengthOfMeridian
     *
     * Computes the ellipsoidal distance from the equator to a point at a
     * given latitude.
     *
     * Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
     * GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
     *
     * Inputs:
     *     phi - Latitude of the point, in radians.
     *
     * Constants:
     *     sm_a - Ellipsoid model major axis.
     *     sm_b - Ellipsoid model minor axis.
     *
     * Returns:
     *     The ellipsoidal distance of the point from the equator, in meters.
     *
     */
    
     double arcLengthOfMeridian (double phi){
         double n = (sm_a - sm_b) / (sm_a + sm_b);

         double alpha = ((sm_a + sm_b) / 2.0)
            * (1.0 + (std::pow (n, 2.0) / 4.0) + (std::pow (n, 4.0) / 64.0));

         double beta = (-3.0 * n / 2.0) + (9.0 * std::pow (n, 3.0) / 16.0)
            + (-3.0 * std::pow (n, 5.0) / 32.0);

         double gamma = (15.0 * std::pow (n, 2.0) / 16.0)
             + (-15.0 * std::pow (n, 4.0) / 32.0);

         double delta = (-35.0 * std::pow (n, 3.0) / 48.0)
             + (105.0 * std::pow (n, 5.0) / 256.0);

         double epsilon = (315.0 * std::pow (n, 4.0) / 512.0);

         double result = alpha
             * (phi + (beta * std::sin (2.0 * phi))
                 + (gamma * std::sin (4.0 * phi))
                 + (delta * std::sin (6.0 * phi))
                 + (epsilon * std::sin (8.0 * phi)));
         return result;
     }


     //This should be changed in oder to use SWEREF 99? it uses more divisions than the UTM
     //www.lantmateriet.se/en/Maps-and-geographic-information/GPS-and-geodetic-surveys/Reference-systems/Two-dimensional-systems/SWEREF-99-projections/

     /**
     * utmCentralMeridian
     *
     * Determines the central meridian for the given UTM zone.
     *
     * Inputs:
     *     zone - An integer value designating the UTM zone, range [1,60].
     *
     * Returns:
     *   The central meridian for the given UTM zone, in radians, or zero
     *   if the UTM zone parameter is outside the range [1,60].
     *   Range of the central meridian is the radian equivalent of [-177,+177].
     *
     */
    
     double utmCentralMeridian (int zone){
         double cmeridian  = degToRad (-183.0 + (zone * 6.0));
         return cmeridian;
     }

     /*
     * footpointLatitude
     *
     * Computes the footpoint latitude for use in converting transverse
     * Mercator coordinates to ellipsoidal coordinates.
     *
     * Reference: Hoffmann-Wellenhof, B., Lichtenegger, H., and Collins, J.,
     *   GPS: Theory and Practice, 3rd ed.  New York: Springer-Verlag Wien, 1994.
     *
     * Inputs:
     *   y - The UTM northing coordinate, in meters.
     *
     * Returns:
     *   The footpoint latitude, in radians.
     *
     */
    
     double footpointLatitude (double iY){
         // (Eq. 10.18) 
         double n = (sm_a - sm_b) / (sm_a + sm_b);    
         // (Eq. 10.22), (Same as alpha in Eq. 10.17) 
         double alpha = ((sm_a + sm_b) / 2.0)
             * (1 + (std::pow (n, 2.0) / 4) + (std::pow (n, 4.0) / 64));
         // (Eq. 10.23) 
         double y = iY / alpha;
         // (Eq. 10.22)
         double beta = (3.0 * n / 2.0) + (-27.0 * std::pow (n, 3.0) / 32.0)
             + (269.0 * std::pow (n, 5.0) / 512.0);
         //(Eq. 10.22)
         double gamma = (21.0 * std::pow (n, 2.0) / 16.0)
             + (-55.0 * std::pow (n, 4.0) / 32.0);    
         //(Eq. 10.22)
         double delta = (151.0 * std::pow (n, 3.0) / 96.0)
             + (-417.0 * std::pow (n, 5.0) / 128.0);       
         //(Eq. 10.22)
         double epsilon = (1097.0 * std::pow (n, 4.0) / 512.0);      
         // sum of the series (Eq. 10.21)
         double result = y + (beta * std::sin (2.0 * y))
             + (gamma * std::sin (4.0 * y))
             + (delta * std::sin (6.0 * y))
             + (epsilon * std::sin (8.0 * y));
         return result;
     }

}