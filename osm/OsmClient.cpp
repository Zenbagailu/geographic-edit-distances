
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#include "OsmClient.h"

#include <iostream>
#include <string>

#include <stdexcept>
#include <sstream>
#include <emscripten.h>

#include <thread> //for sleep
#include <chrono>

namespace osm{

    //Javascript function 
    //this could probably be done using emscripten_wget() instead? 
    EM_JS(const char*, doRequest, (const char* request), {
        var jsRequest = UTF8ToString(request);
        var xhr = new XMLHttpRequest();
        var response ="Could not process request: " + jsRequest; // default result, if not overriden by processRequest

        function processRequest(e) {
            if (xhr.readyState == 4){ //4 means that it is done,
                if(xhr.status == 200) { //200 that it was succesful
                    response = xhr.responseText;
                }
                else{ //not succesful
                    response = "Request: " + jsRequest + " failed with code: " + xhr.status.toString() + ": " + xhr.statusText;
                }
            }
        }

        xhr.addEventListener("readystatechange", processRequest, false);
        //construct request
        xhr.open('GET', jsRequest , false); //true means asynchronous

        //this is for catching errors connecting from the javascript (browser) side
        try{
            xhr.send();
        }catch(err) {
            response = "Request: " + jsRequest + " failed to send.";
        }
        
        
        var lengthBytes = lengthBytesUTF8(response)+1;
        var stringOnWasmHeap = _malloc(lengthBytes);
        stringToUTF8(response, stringOnWasmHeap, lengthBytes);
        return stringOnWasmHeap;

    });

    OsmClient::OsmClient(std::string server):server_{server}
    {}



    std::string OsmClient::processQuery(std::string querySt){
        static const std::string apiStr="/api/interpreter?data=";
        std::ostringstream stringStream;
        stringStream << "https://" <<server_ << apiStr << querySt;
        auto result = std::string(doRequest(stringStream.str().c_str()));

        while (result.rfind("Request: ", 0) == 0 && result.find("failed with code: 429", 9) != std::string::npos){
            std::cout<<"Too many requests, trying again." <<std::endl;

            std::this_thread::sleep_for(std::chrono::milliseconds(2000));
            result = std::string(doRequest(stringStream.str().c_str()));
        }

        while (result.rfind("Request: ", 0) == 0 && result.find("failed with code: 504", 9) != std::string::npos){
            std::cout<<"Gateway error, trying again."   <<std::endl;

            std::this_thread::sleep_for(std::chrono::milliseconds(2000));
            result = std::string(doRequest(stringStream.str().c_str()));
        }

        //process failed requests
        while (result.rfind("Request: ", 0) == 0 && result.find("failed to send", 9) != std::string::npos){
            std::cout<<"Failed to send request, trying again."   <<std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(2000));
            result = std::string(doRequest(stringStream.str().c_str()));
        }

        //begins with:
        //https://stackoverflow.com/questions/1878001/how-do-i-check-if-a-c-stdstring-starts-with-a-certain-string-and-convert-a
        if (result.rfind("Request: ", 0) == 0) { 
            throw std::runtime_error(result);
        }
        else if (result.rfind("Could not process request: ",0) == 0){
            throw std::runtime_error(result);
        }

        return result;
    }
}
