
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef TESTOSMDRAWQUERY_H_
#define TESTOSMDRAWQUERY_H_ 

#include <osm/UtmProjector.h>

//to parse the json into a boost property tree
#include <boost/property_tree/ptree.hpp>

#include <string>
#include <vector>
#include <array>
#include <unordered_map>
#include <set>
#include <exception>

#include <glaux/tesselate.h>

#include "OsmData.h"
#include "OsmDrawingTypes.h"

#include "OsmDataHolder.h"

namespace test {

    template<class Projector>NodeData getNode(boost::property_tree::ptree const& tree, Projector projector){

        boost::property_tree::ptree const& id = tree.get_child("id");
        auto idName=id.get_value<std::size_t>();
        boost::property_tree::ptree const& lat = tree.get_child("lat");
        auto latData=lat.get_value<double>();
        boost::property_tree::ptree const& lon = tree.get_child("lon");
        auto lonData=lon.get_value<double>();
        return{idName,lonData,latData, projector,tree};  
    }


    template<class Renderer, class Nodes> void drawNodes(
    Nodes const& nodes,//a contaier of nodes...(or a class/struct that implements the same type elements)
    Renderer& renderer){ 
        renderer.setPointSize(5);
        renderer.begin(Renderer::DrawingMode::POINTS);
        for(auto& node:nodes){
            renderer.vertex(node.x(),node.y());
        }
        renderer.end();
    }


    template<class Renderer, class Nodes> void drawNodesData(
    Nodes const& nodes,  //a contaier of nodes...(or a class/struct that implements the same  type elements)
    Renderer& textRenderer,
    int faceSize){
        for(auto& node:nodes){
            //for rendering with lines:
            std::stringstream data;
            data.str(dataAsString(node));
            int lnCt=0;
            for (std::string line; std::getline(data, line); ) {
                textRenderer.renderTextInPos(node.x(),node.y(), 30, line, 30, - (faceSize + lnCt*faceSize));
                ++lnCt;
            }
        }
    }


    template<class Renderer, class Way, class Nodes> void drawWay(
    Way const& way, 
    Nodes const& nodes,//a contaier of nodes...(or a class/struct that implements the same elements as Node)
    Renderer& renderer){  
        renderer.begin(Renderer::DrawingMode::LINE_STRIP);  
        for(auto nodeId: way.nodeIds){
            renderer.vertex(nodes.at(nodeId));
        }
        renderer.end();  
    }
  
    template<class WaysIndices, class Ways, class Nodes> 
    std::vector<typename Ways::mapped_type>
    joinInClosedPolygons(
    WaysIndices const& waysIds,     // A container with a set of ids of the ways to consider.
    Ways const& ways,               // A map type from ids to Ways.
    Nodes const& nodes){            // A map type from ids to Nodes.

        //not very nice... should use some type of Trait type for this.
        using wayType           = typename Ways::mapped_type;
        std::vector<wayType> newWays;


        std::vector<size_t> brokenWays; //in cases of small types more efficient than lists
        for(auto id : waysIds){
            auto const& way=ways.at(id);
            if(way.nodeIds.front() != way.nodeIds.back()){
                brokenWays.push_back(id);
            }
        }

        //process all open ways
        while(brokenWays.size() > 0){
            auto currentWayId=brokenWays.back();
            brokenWays.pop_back();

            wayType newWay;
            newWay.nodeIds = ways.at(currentWayId).nodeIds;

            //as long as it is not closed
            while(newWay.nodeIds.front() != newWay.nodeIds.back()){

                //Find the one that links to its end.
                //one could also check the beginning, but it won't matter if it is closed (it may be faster
                //but a bit more complex).
                bool continues=false;
                std::vector<size_t>::iterator itToRemove;
                //for(auto const& wayId: brokenWays){
                //for(int i=0;i<brokenWays.size();++i){
                for (auto it = brokenWays.begin(); it != brokenWays.end(); ++it){
                
                    auto wayToProcessId = *it;
                    auto const& wayToProcess=ways.at(wayToProcessId);

                    if(     newWay.nodeIds.back() == wayToProcess.nodeIds.front()){
                        newWay.nodeIds.insert(newWay.nodeIds.end(), 
                                              wayToProcess.nodeIds.begin(),
                                              wayToProcess.nodeIds.end());
                        continues=true;
                        itToRemove=it;
                        break;

                    }
                    else if (newWay.nodeIds.back() == wayToProcess.nodeIds.back()){
                        //copy reversed
                        newWay.nodeIds.insert(newWay.nodeIds.end(), 
                                              wayToProcess.nodeIds.rbegin(),
                                              wayToProcess.nodeIds.rend());
                        continues=true;
                        itToRemove=it;
                        break;
                    }
                }
                if(!continues){ //If it could not find any way of continuing or closing: 
                    break; //exit the checking for closeness
                }else{
                    brokenWays.erase(itToRemove);
                }
            }

            //if it was closed
            if(newWay.nodeIds.front() == newWay.nodeIds.back()){
                newWays.push_back(newWay);
            }

        }

        return newWays;
    }
    
     


    template<class Renderer, class Relation, class Ways, class Nodes> void fillRelation(
    Relation const& relation,
    Ways const& ways,  
    Nodes const& nodes,//a contaier of nodes...(or a class/struct that implements the same elements as Node)
    Renderer& renderer){

        //create tesselator.
        //It should be better not to expose any detail.
        static glaux::Tesselator<Renderer> tesselator;

        tesselator.beginPolygon(renderer);

       // std::vector<typename Nodes::mapped_type> polygon;

        for(auto id : relation.outerwaysIds){
            auto const& way=ways.at(id);
            if(way.nodeIds.front() == way.nodeIds.back()){

                tesselator.beginContour();
                for(auto nodeId: way.nodeIds){
                    tesselator.vertex(nodes.at(nodeId));
                }
                tesselator.endContour();   
            }
        }

        //Now process eventual incorrect polygons:
        auto correctedOuterWays=joinInClosedPolygons(relation.outerwaysIds,ways,nodes);
        for(auto const& cway: correctedOuterWays){
            if(cway.nodeIds.front() == cway.nodeIds.back()){ //check just in case
                tesselator.beginContour();
                for(auto nodeId: cway.nodeIds){
                    tesselator.vertex(nodes.at(nodeId));
                }
                tesselator.endContour();
            }
        }

        for(auto id : relation.innerwaysIds){
            auto& way=ways.at(id);
            if(way.nodeIds.front() == way.nodeIds.back()){
                tesselator.beginContour();
                for(auto nodeId: way.nodeIds){
                    tesselator.vertex(nodes.at(nodeId));
                }
                tesselator.endContour();
            }
        }

        //Now process eventual incorrect polygons:
        auto correctedInnerWays=joinInClosedPolygons(relation.innerwaysIds,ways,nodes);
        for(auto const& cway: correctedInnerWays){
            if(cway.nodeIds.front() == cway.nodeIds.back()){ //check just in case
                tesselator.beginContour();
                for(auto nodeId: cway.nodeIds){
                    tesselator.vertex(nodes.at(nodeId));
                }
                tesselator.endContour();
            }
        }

        tesselator.endPolygon();
    }


    //This should really be a part of renderer...
    template<class Renderer, class Way, class Nodes> void fillWay(
    Way const& way, 
    Nodes const& nodes,//a contaier of nodes...(or a class/struct that implements the same elements as Node)
    Renderer& renderer){

        if(way.nodeIds.front() == way.nodeIds.back()){
            
            std::vector<typename Nodes::mapped_type> polygon;
            //this is a bit unnecesary, perhaps a better interface to tesselatePolygon could solve this...
            for(auto nodeId: way.nodeIds){
                polygon.push_back(nodes.at(nodeId));
         
            }
            //renderer.begin(Renderer::DrawingMode::TRIANGLES); 
            glaux::tesselatePolygon(renderer,polygon);
            //renderer.end();   
        }
    }

    template<class Renderer, class Way, class Nodes> void drawWayData(
    Way const& way, //a way 
    Nodes const& nodes,  //a contaier of nodes...(or a class/struct that implements the same  type elements)
    Renderer& textRenderer,
    int faceSize){
     
        //get the middle node of the way, to use to display data
        //auto textNodeId=way.nodeIds[way.nodeIds.size()/2];

        //rightmost node:
        double rightmost = -std::numeric_limits<double>::max();
        size_t textNodeId;
        for(auto nodeId: way.nodeIds){
            // polygon.push_back(nodes.at(nodeId));
            auto const& node = nodes.at(nodeId);
            if(node.x() > rightmost){
                rightmost   = node.x();
                textNodeId = nodeId;
            }
        }

        std::stringstream data;
        data.str(dataAsString(way));
        int lnCt=0;
        for (std::string line; std::getline(data, line); ) {
            textRenderer.renderTextInPos(nodes.at(textNodeId).x(),
                                         nodes.at(textNodeId).y(),
                                         30, 
                                         line, 
                                         30, - (faceSize + lnCt*faceSize));
            ++lnCt;
        }
    }



    template<class Renderer, class Relation, class Ways, class Nodes> void drawRelationData(
    Relation const& relation,
    Ways const& ways,  
    Nodes const& nodes,
    Renderer& textRenderer,
    int faceSize){

        //get the middle node of the way, to use to display data
        //auto textNodeId=way.nodeIds[way.nodeIds.size()/2];

        //rightmost node:
        double rightmost = -std::numeric_limits<double>::max();
        size_t textNodeId;

        //no need to do inner ways (since there will always be an outer way with nodes that are more "outside")

        for(auto id : relation.outerwaysIds){
            auto const& way=ways.at(id);
            for(auto nodeId: way.nodeIds){
                auto const& node = nodes.at(nodeId);
                if(node.x() > rightmost){
                    rightmost   = node.x();
                    textNodeId = nodeId;
                }
            }    
        }

        std::stringstream data;
        data.str(dataAsString(relation));
        int lnCt=0;
        for (std::string line; std::getline(data, line); ) {
            textRenderer.renderTextInPos(nodes.at(textNodeId).x(),
                                         nodes.at(textNodeId).y(),
                                         30, 
                                         line, 
                                         30, - (faceSize + lnCt*faceSize));
            ++lnCt;
        }


    }

    template<class ET> DrawingSettings
    calculateDrawingSettings(
    ET const& entity, //the entity
    std::string entityType, //the type of entity
    AttributeDrawingSettings const& attributeDrawing){ //all AttributeDrawingSettings

        // DrawingSettings drawingSettings{true,false,0.5,{1.0,1.0,1.0,1.0}};
        DrawingSettings drawingSettings{true,false,0.5,{0.5,0.5,0.5,1.0}}; //default

        for(auto const& pair:attributeDrawing){

            auto const& filter = pair.first;
            if(filter.queryType != entityType){ //find the drawing settings for the entitytype
                continue;
            }

            size_t matchedTags=0;

            for(auto const& tag: entity.attributes){ //see that it matches all in the description 
                auto const& tagKey=tag.first;
                auto const& settingTag=filter.tags.find(tagKey);
                //This is actually the way it should be....
                //if(settingTag != filter.tags.end() && settingTag->second == tag.second){
                if(settingTag != filter.tags.end()){
                    ++matchedTags;
                }
            }

            if(matchedTags==filter.tags.size()){
                //std::cout<<"settings applied"<<std::endl;
                drawingSettings =  pair.second; //if match, assign settings
                break; 
            }
        }

        return drawingSettings;

    }

    template<class Renderer, class OsmData> void 
    drawData(
             OsmData const& data,
             int faceSize,
             Renderer& renderer,
             Renderer& textRenderer,
             AttributeDrawingSettings const& attributeDrawing, 
             bool drawNodesText=true){ //This is a placeholder until a general method for processing node attributes is developed

        //This is a bit dodgy...
        auto const& ways = data.getWayMap();
        auto const& nodes = data.getNodeMap();

        for(auto const& relation:data.getRelations()){

            DrawingSettings drawingSettings = calculateDrawingSettings(relation,"relation",attributeDrawing);

            renderer.setLineThickness(drawingSettings.thickness);
            renderer.setColor(drawingSettings.color);

            if(drawingSettings.fill){
                fillRelation(relation,ways,nodes,renderer);
            }

            if(drawingSettings.drawText){
                drawRelationData(relation,ways,nodes,textRenderer,faceSize);
            }

            for(auto id : relation.outerwaysIds){
                auto const& way = ways.find(id);
                if (way !=ways.end()){
                    drawWay(way->second,nodes,renderer);
                }
            }

            for(auto id : relation.innerwaysIds){
                auto const& way = ways.find(id);
                if (way !=ways.end()){
                    drawWay(way->second,nodes,renderer);
                }
            }
        }

        //Now draw ways that don't belong to relations.
        for(auto const& way: data.getWays()){
            DrawingSettings drawingSettings = calculateDrawingSettings(way,"way",attributeDrawing);
            renderer.setLineThickness(drawingSettings.thickness);
            renderer.setColor(drawingSettings.color);

            if(drawingSettings.fill){
                fillWay(way,nodes,renderer);
            }
            
            drawWay(way,nodes,renderer);

            if(drawingSettings.drawText){
                drawWayData(way,nodes,textRenderer,faceSize);
            }
        }
        //TODO: Drawing settings for nodes not implemented.
        //A drawing each node one by one (with different settings depending on their types), would 
        //slow down the rendering. They should be sorted depending on their different settings, and then
        //draw all with same settings at once. 

        drawNodes(data.getNodes(), renderer);
        if(drawNodesText){
            drawNodesData(data.getNodes(), textRenderer, faceSize);
        }
        
    }     
}
#endif