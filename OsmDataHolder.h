
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef TESTOSMDATAHOLDER_H_
#define TESTOSMDATAHOLDER_H_

#include <unordered_map>
#include <exception>
#include <array>
#include "OsmData.h"

namespace test {
    class OsmDataHolder
    {
    public:
        using IdType = NodeData::IdType;
        OsmDataHolder(std::string  const& query, double centreLat, double centreLon);
        OsmDataHolder():projector_{0,0}{}
        virtual ~OsmDataHolder(){}
        void addData(std::string  const& query);
        
        //These getters are only temporary... they are used for drawing the data and divide it into subsets
        //possibly it can be solved with more concrete friend functions and other techniques.
        std::unordered_map<IdType,NodeData> const& getNodeMap()const{return dependentNodes_;}
        std::unordered_map<IdType,WayData> const& getWayMap()const{return dependentWays_;}

        std::vector<MultipolygonRelationData> const& getRelations()const{return relations_;}
        std::vector<WayData> const& getWays() const{return ways_;}
        std::vector<NodeData> const& getNodes() const{return nodes_;}      
    
    private:
        //for storing the nodes and ways with their ID
        std::unordered_map<IdType,NodeData> dependentNodes_; 
        std::unordered_map<IdType,WayData> dependentWays_; 

        //these are the elements that are independent (all relations, ways that are not part of relations
        //and nodes that are not part of ways)
        std::vector<MultipolygonRelationData> relations_; //since it is highest order, it does not need a map
        std::vector<NodeData> nodes_;
        std::vector<WayData> ways_; 

        Project<std::array<double,2>> projector_;
    };
    

}

#endif