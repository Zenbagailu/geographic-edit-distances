
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef DISJOINTSETSDISJOINTSETS_H_
#define DISJOINTSETSDISJOINTSETS_H_

#include <map>

namespace disjointsets {
    
    /*
     From pseudocode from wikipedia.
     http://en.wikipedia.org/wiki/Disjoint-set_data_structure
     It uses Union Ranking and Path Compresion
     */
    
    /*
     The implementation uses copies of objects directly, so it will not be a good idea
     For big objects. Otherwise handles, smart pointers (or small objects) should work fine? (not tested...)
     */
    
    //Elem needs to implement Id
    template< typename Elem, class Compare = std::less<Elem> > class DisjointSets{
        
    public:
        
        //Types
        typedef std::pair<Elem, int>            Node; //first is parent, second is rank
        typedef std::map<Elem, Node, Compare>   Map;
       
        //Function Members
        DisjointSets(){};
        void clear(); //reset
        void makeSet(Elem const& e);
        void join(Elem const& x, Elem const& y);//Union, with union ranking
        bool areSameSet(Elem const& x, Elem const& y);
        
        //this adds functionality, so we can also use the DisjointSets as a set (the set of all the elements in the map)
        //it is not part of the essential functionality of a DisjointSets data structure
        bool contain(Elem const& e) const;
        
        //A more explicit form of find, to be used to put sets into other containers and
        // other algorithms... it would be nice to have a better solution
        //Elem const& getRepresentativeElement(Elem const& elem){return find(elem);}
        
    private:
        //Find with Path Compresion
        Elem const& find(Elem const& elem);

        
    private:
        // map of keys and nodes
        Map map_;
        
        //prevent copying
        DisjointSets( DisjointSets const& cs);
        DisjointSets& operator=(DisjointSets const& cs );
        
               
    };
     
    template< typename Elem, class Compare>
    void
    DisjointSets<Elem,Compare>::
    clear(){ //reset
        map_.clear();
    }
    
    template< typename Elem, class Compare>
    bool
    DisjointSets<Elem,Compare>::
    contain(Elem const& e) const{
        return map_.find(e) != map_.end();
    }
    
    template< typename Elem, class Compare>
    void
    DisjointSets<Elem,Compare>::
    makeSet(Elem const& e){
        map_.insert(std::make_pair(e,Node(e,0) ) ); //map insert does not do it if the key (e) already exists
    }
    

    //Union, with union ranking
    template< typename Elem, class Compare>
    void
    DisjointSets<Elem,Compare>::
    join(Elem const& x, Elem const& y){
        
        //add them if they don't exist.
        //this is needed, or otherwise a test that returns without joining...
        if(!contain(x)){makeSet(x);}
        if(!contain(y)){makeSet(y);}
        
        Node & xRootNode=map_[find(x)];
        Node & yRootNode=map_[find(y)];
        
        if (xRootNode.second > yRootNode.second){
            yRootNode.first = xRootNode.first;
        }else if (xRootNode.second < yRootNode.second){
            xRootNode.first = yRootNode.first;
        }else if (xRootNode.first != yRootNode.first){//if they are the same, they are already joined...
            yRootNode.first = xRootNode.first;
            xRootNode.second++;
        }
    }
    
    template< typename Elem, class Compare>
    bool
    DisjointSets<Elem,Compare>::
    areSameSet(Elem const& x, Elem const& y){
        if(!contain(x) || !contain(y)){
            return false;
        }
        return map_.find(find(x)) == map_.find(find(y)); //this compares the iteator to the same element
    }

    
    //Find with Path Compresion
    template< typename Elem, class Compare>
    Elem const&
    DisjointSets<Elem,Compare>::
    find(Elem const& elem){
        Node & node=map_[elem];
        if(node.first == elem){ //if it is the top, that is, if its parent is itself
            return elem;
        }
        //else:
        node.first=find(node.first); //search and flatten
        return node.first;
    }
    
    
    
}

#endif
