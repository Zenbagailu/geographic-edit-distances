
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef VIEWSORTHO_H_
#define VIEWSORTHO_H_ 

#include <glm/glm.hpp>
#include "View.h"


namespace views {
	class Ortho: public View
	{
	public:
		Ortho(float zNear=-1000, float zFar=1000);
		virtual ~Ortho();

		void rotate(glm::ivec2 rDir);
		void resize(int w, int h);
		void zoom(float zoom);
		void setZoom(float zoom);
		void setCentre(float x, float y, float z);
		void pan(glm::ivec2 posA, glm::ivec2 posB);

	private:
		void update();

	private:
		
        float angleAxisX_;
        float angleAxisZ_;

        glm::vec3 centre_;

        float zNear_, zFar_;
        float zoom_;
	};
}
#endif