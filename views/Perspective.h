
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef VIEWSPERSPECTIVE_H_
#define VIEWSPERSPECTIVE_H_ 

#include <glm/glm.hpp>
#include "View.h"

namespace views {
	
	class Perspective: public View
	{
	public:
		Perspective(float zNear=0.1, float zFar=1000);
		virtual ~Perspective();

		void rotate(glm::ivec2 rDir);
		void resize(int w, int h);
		void zoom(float zoom);
		void pan(glm::ivec2 posA, glm::ivec2 posB);
		void setClippingPlanes(float zNear,float zFar);
		void setDistance(float distance);

	private:
		void update();

	private:
		
        float angleAxisX_;
        float angleAxisY_;

        glm::vec3 centre_;

        float zNear_, zFar_;
		float distance_;
		float fovY_;

	};
}
#endif