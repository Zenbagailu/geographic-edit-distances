
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "Perspective.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>

//debug
// #include <iostream>
// #include <glm/gtx/string_cast.hpp>



namespace views{

	Perspective::Perspective(float zNear, float zFar):
	angleAxisX_{0},
    angleAxisY_{0},
	centre_{0,0,0},
	zNear_{zNear},
	zFar_{zFar},
	distance_{1.00}, 
	fovY_(30.0){
		update();

	}
	
	Perspective::~Perspective(){

	}

	void Perspective::update(){
		modelMatrix_=glm::mat4(1.0f);
		modelMatrix_=glm::translate(modelMatrix_,{0.0f, 0.0f, -distance_});
		modelMatrix_=glm::rotate(modelMatrix_, angleAxisX_, {1.0f, 0.0f, 0.0f});
		modelMatrix_=glm::rotate(modelMatrix_, angleAxisY_, {0.0f, 1.0f, 0.0f});

		//this is to make the Z axis point up (standard drawing convention)
		modelMatrix_=glm::rotate(modelMatrix_, glm::half_pi<float>(), {1.0f, 0.0f, 0.0f});

		modelMatrix_=glm::translate(modelMatrix_, -centre_);
	}


	void Perspective::resize(int w, int h){
		w_=w;
		h_=h;
		projMatrix_=glm::perspective(fovY_,w_/h_,zNear_,zFar_);

	}

	void Perspective::rotate(glm::ivec2 rDir) { 
		angleAxisX_ -= glm::pi<float>()	* static_cast<double>(rDir.y)/ h_;
		angleAxisY_ -= glm::pi<float>() * static_cast<float>(rDir.x)/ w_;
		float vLim = glm::half_pi<float>()-0.001;
		angleAxisX_ = angleAxisX_ >= vLim ? vLim : angleAxisX_ < -vLim ? -vLim: angleAxisX_;

		update();
	}

	void Perspective::zoom(float zoom) {
		
		//distance_ += zoom > 0 ? 0.001: -0.001;
		distance_ *= (1 + 2 * zoom / h_);
		//zFar_=zFar_>distance_*2?distance_*2:zFar_;
		distance_ = distance_ <= zNear_? zNear_ : distance_;


		update();
	}

	//it assumes pixel (openGL) coordinates
	void Perspective::pan(glm::ivec2 posA, glm::ivec2 posB){
		update(); //needs all transformations applied...

		auto viewport=glm::vec4(0, 0, w_, h_);

		auto projA=glm::unProject(glm::vec3{posA,0},modelMatrix_,projMatrix_,viewport);
		auto dirA=glm::unProject(glm::vec3{posA,1},modelMatrix_,projMatrix_,viewport);
		dirA-=projA;

		auto projB=glm::unProject(glm::vec3{posB,0},modelMatrix_,projMatrix_,viewport);
		auto dirB=glm::unProject(glm::vec3{posB,1},modelMatrix_,projMatrix_,viewport);
		dirB-=projB;

		//this defines in model space the planes parallel to the viewing plane
		glm::vec2 viewCentre{w_/2.f,h_/2.f}; //centre of screen
		auto projC=glm::unProject(glm::vec3{viewCentre,0},modelMatrix_,projMatrix_,viewport);
		auto dirC=glm::unProject(glm::vec3{viewCentre,1},modelMatrix_,projMatrix_,viewport);
		dirC-=projC;

		auto planeNormal=dirC;

		//now we calculate intersections(from solving line-plane intersection equations)
		//for a plane parallel to the view plane through centre_, and the rays from the two screen
		//positions perpendicular to the viewing plane

		float pd=glm::dot(planeNormal, centre_);

		float f=glm::dot(planeNormal, projA);
		float g=glm::dot(planeNormal, dirA);
		float gp=(pd-f)/g;

		auto pptA=gp*dirA+projA;

		f=glm::dot(planeNormal, projB);
		g=glm::dot(planeNormal, dirB);
		float gn=(pd-f)/g;

		auto pptB=gn*dirB+projB;

		// //because the translation is in the opposite direction than the center, it needs to be subtracted
		centre_-=pptB-pptA;

		update();
	}

	void Perspective::setClippingPlanes(float zNear,float zFar){
		zNear_=zNear;
		zFar_=zFar;
		projMatrix_=glm::perspective(fovY_,w_/h_,zNear_,zFar_);
	}

	void Perspective::setDistance(float distance){
		distance_=distance;
	}
}
