
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "Ortho.h"

#include <glm/gtc/matrix_transform.hpp>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>

#include <limits>

namespace views{

	Ortho::Ortho(float zNear, float zFar):
	angleAxisX_{0},
    angleAxisZ_{0},
	centre_{0,0,0},
	zNear_{zNear},
	zFar_{zFar},
	zoom_{100}{

	}

	Ortho::~Ortho(){

	}

	void Ortho::update(){
		modelMatrix_=glm::mat4(1.0f);
		modelMatrix_=glm::rotate(modelMatrix_, angleAxisX_, {1.0f, 0.0f, 0.0f});
		modelMatrix_=glm::rotate(modelMatrix_, angleAxisZ_, {0.0f, 0.0f, 1.0f});
		modelMatrix_=glm::translate(modelMatrix_, -centre_);
	}

	void Ortho::resize(int w, int h){
		w_=w;
		h_=h;
		projMatrix_=glm::ortho(-w_/(2.f*zoom_),w_/(2.f*zoom_),-h_/(2.f*zoom_),h_/(2.f*zoom_),zNear_,zFar_);
	}

	void Ortho::rotate(glm::ivec2 rDir) { 
		angleAxisX_ -= glm::pi<float>()	* static_cast<double>(rDir.y)/ h_;
		angleAxisZ_ -= glm::pi<float>() * static_cast<float>(rDir.x)/ w_;
		update();
	}

	
	void Ortho::zoom(float zoom) {
		
		//This is a bit of a hack... but it works.
		auto nzoom = zoom_ * (1 + zoom/h_);
		//in emscripten
		if(nzoom > 0 && nzoom <= 283700){ //the limit has been experimentally determined.
			zoom_ = nzoom;
		}

		projMatrix_=glm::ortho(-w_/(2.f*zoom_),w_/(2.f*zoom_),-h_/(2.f*zoom_),h_/(2.f*zoom_),zNear_,zFar_);	
	}


	void Ortho::setZoom(float zoom){

		if(zoom > 0 && zoom <= 283700){ //the limit has been experimentally determined.
			zoom_ = zoom;
		}

		projMatrix_=glm::ortho(-w_/(2.f*zoom_),w_/(2.f*zoom_),-h_/(2.f*zoom_),h_/(2.f*zoom_),zNear_,zFar_);	
	}

	void Ortho::setCentre(float x, float y, float z){
		centre_[0]=x;centre_[1]=y;centre_[2]=z; update();
	}

	//it assumes pixel (openGL) coordinates
	void Ortho::pan(glm::ivec2 posA, glm::ivec2 posB){
		update(); //needs transfromations applied

		auto ppoint= glm::unProject(
	       glm::vec3{posA,0},
	       modelMatrix_,
	       projMatrix_,
	       glm::vec4{0,0,w_,h_});

		auto npoint= glm::unProject(
	       glm::vec3{posB,0},
	       modelMatrix_,
	       projMatrix_,
	       glm::vec4{0,0,w_,h_});

		centre_ -= npoint-ppoint;

		update();

	}

}
