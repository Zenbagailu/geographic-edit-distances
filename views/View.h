
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef VIEWSVIEW_H_
#define VIEWSVIEW_H_ 

#include <glm/glm.hpp>

namespace views {
	
	//it could also include the resize(w,h) function,
	//and perhaps an update()
	class View
	{
	public:
		View();
		virtual ~View();

		float getWidth()const{return w_;}
		float getHeight()const{return h_;}

		float const* getProjectionPtr()const;
		float const* getModelPtr() const;

		glm::mat4 const& getModelMatrix()const;
		glm::mat4 const& getProjectionMatrix()const;

		void setModelMatrix(glm::mat4&& modelMatrix); //to be able to perform model transformations
		void setModelMatrix(glm::mat4 const& modelMatrix);

		void setProjectionMatrix(glm::mat4&& modelMatrix); //to be able to adjust to screen, if used not in derived from
		void setProjectionMatrix(glm::mat4 const& modelMatrix);

	protected: //they need to be accessed frequently by inheriting classes...
		glm::mat4 projMatrix_;
        glm::mat4 modelMatrix_;
        float w_,h_; 

	};
}
#endif