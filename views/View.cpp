
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "View.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>

#include <iostream>



namespace views{

	View::View():
	projMatrix_{1.0}, //default unit matrices
    modelMatrix_{1.0}
    {
    	
	}
	

	View::~View(){

	}


	float const* View::getProjectionPtr()const {
		return glm::value_ptr(projMatrix_);
	}

	float const* View::getModelPtr() const{
		//update(); 
		return glm::value_ptr(modelMatrix_);
	}

	glm::mat4 const& View::getModelMatrix() const{
		//update();
		return modelMatrix_;
	}

	glm::mat4 const& View::getProjectionMatrix() const{
		//update();
		return projMatrix_;
	}

	void View::setModelMatrix(glm::mat4&& modelMatrix){
		modelMatrix_=std::move(modelMatrix);
	}

	void View::setModelMatrix(glm::mat4 const& modelMatrix){
		modelMatrix_=modelMatrix;
	}

	void View::setProjectionMatrix(glm::mat4&& projectionMatrix){
		projMatrix_=std::move(projectionMatrix);
	}

	void View::setProjectionMatrix(glm::mat4 const& projectionMatrix){
		projMatrix_=projectionMatrix;
	}

}
