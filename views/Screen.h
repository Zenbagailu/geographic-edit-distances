
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef VIEWSSCREEN_H_
#define VIEWSSCREEN_H_ 


#include "View.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

//This is a simple view for passing to functions that require a view type object
//(for getting the projection and model matrix, screen size, etc) but render on
//screen coordinates (without any other transformation). The only method it needs to 
//have (besides those inherited) is a resize().
namespace views {
    class Screen: public View
    {
    public:
        virtual ~Screen(){};
        void resize(int w, int h){
            w_=w;
            h_=h;
            projMatrix_=glm::ortho(0.0f,w_,0.0f,h_);
        };
    };
}
#endif