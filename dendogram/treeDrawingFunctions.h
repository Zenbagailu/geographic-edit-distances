
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef DENDOGRAMTREEDRAWINGFUNCTIONS_H_
#define DENDOGRAMTREEDRAWINGFUNCTIONS_H_ 


//for printing 
#include <ostream>

//#include <iostream>
#include <sstream>


namespace dendogram {

    //These functions are abstracted for drawing generic binary trees, as long
    //as they implement the same basic interface.

    template< class Tree>
    size_t
    maxDepth(Tree const& tree, 
             typename Tree::node_type const& node, 
             int depth){
        ++depth;
        if(tree.isNodeLeaf(node)){
            return depth;
        }else{
            auto depthA = maxDepth(tree,tree.getChildA(node),depth);
            auto depthB = maxDepth(tree,tree.getChildB(node),depth);
            //return depthA > depthB ? (depthA > depth ? depthA :  depth) : (depthB > depth ? depthB :  depth);
            return depthA > depthB ? depthA : depthB;
        } 
    }

    template< class Tree>
    size_t
    maxDepth(Tree const& tree){
        return maxDepth(tree, tree.getRoot(),0);
    }

    template< class Tree>
    size_t
    numOfLeaves(Tree const& tree, 
                typename Tree::node_type const& node, 
                int numLeaves){
        
        if(tree.isNodeLeaf(node)){
            return ++numLeaves;
        }else{
            auto numLeavesA = numOfLeaves(tree,tree.getChildA(node),numLeaves);
            auto numLeavesB = numOfLeaves(tree,tree.getChildB(node),numLeaves);
            //return depthA > depthB ? (depthA > depth ? depthA :  depth) : (depthB > depth ? depthB :  depth);
            return numLeavesA + numLeavesB;
        } 
    }


    template< class Tree>
    size_t
    numOfLeaves(Tree const& tree){
        return numOfLeaves(tree,tree.getRoot(),0);
    }

    //depending of when they are visited, this can be used for laying them out. One can think that the first leaf
    //visited is the leftmost, and then add a value to posX (and return it). In reality posX should not be passed using 
    //the stack/recursion, it is really a variable that is independent of the function (it simply should exist at a scope
    //where the recursive functions are called, perhaps through a class).


    //This is an implmentation as a functor. It is longer than a single function, but clearer in terms of
    //variables that need to be passed recursively and those that don't.
    template< class LeafF, class NodeF,class EdgeF> 
    class LayoutByLevel{
    public:

        LayoutByLevel(
            LeafF const& leafFunction,
            NodeF const& nodeFunction,
            EdgeF const& edgeFunction):
        leafFunction_{leafFunction},
        nodeFunction_{nodeFunction},
        edgeFunction_{edgeFunction}
        {}

        template<class Tree> 
        void 
        operator()(Tree const& tree);

        template<class Tree> 
        double 
        operator()(Tree const& tree, typename Tree::node_type const& node, size_t level);


    private:
        LeafF const& leafFunction_;
        NodeF const& nodeFunction_;
        EdgeF const& edgeFunction_;
        size_t furthest_; 
    };

    
    template< class LeafF, class NodeF,class EdgeF> 
    template<class Tree> 
    void
    LayoutByLevel<LeafF,NodeF,EdgeF>::operator()(Tree const& tree){
        furthest_= 0; //reset
        (*this)(tree,tree.getRoot(),0); //recursive call
    }


    template< class LeafF, class NodeF,class EdgeF> 
    template<class Tree> 
    double
    LayoutByLevel<LeafF,NodeF,EdgeF>::operator()(Tree const& tree, typename Tree::node_type const& node, size_t level){
        double nodePos;
        if(tree.isNodeLeaf(node)){
            ++furthest_; //increase the furthes everytime it reaches a leave.
            nodePos = furthest_;
            leafFunction(node, furthest_, level); 
        }
        else{
            //this imposes an arbitrary ordering of left and right
            auto aPos = (*this)(tree, tree.getChildA(node), level+1);
            auto bPos = (*this)(tree, tree.getChildB(node), level+1);

            //just a way of calculating the pos
            nodePos = (aPos + bPos)/2.0;

            //do all drawing
            nodeFunction(node,nodePos,level);
            edgeFunction(nodePos, aPos, level);
            edgeFunction(nodePos, bPos, level);
        }
        return nodePos;   
    }


    template< class Tree, class LeafF, class NodeF,class EdgeF>
    void
    layoutByLevel(
        Tree const& tree,
        LeafF const& leafFunction,
        NodeF const& nodeFunction,
        EdgeF const& edgeFunction){

        LayoutByLevel< LeafF,NodeF,EdgeF> layoutFunctor;
        layoutFunctor(tree);

    }

    //<< operator for leaf type must be implemented
    template< class Tree>
    std::ostream& 
    write(Tree const& tree, std::ostream& os){

        using Node = typename Tree::node_type;

        int elemWidth = 4;
        int elemHeight = 2;


        auto height=maxDepth(tree);
        os<<"height: "<<height<<std::endl;
        auto vizWidth= elemWidth * numOfLeaves(tree) + 10;  //plus some padding for printing 
        auto vizHeight = elemHeight * height;
        std::string viz(vizWidth*vizHeight,' ');


        layoutNodeLevel(tree,tree.getRoot(), 0, 0, 
                     [&](Node const& node, size_t posX, size_t level){
                        auto posY = level * elemHeight;
                        viz.at(vizWidth*posY + posX*elemWidth) = '|';  
                        //write leaf
                        std::ostringstream stringStream;
                        stringStream << tree.getLeaf(node);
                        //stringStream << tree.getData(node); //<< operator for leaf type must be implemented
                       //stringStream << leaves_[node.getLeaf()]; //<< operator for leaf type must be implemented
                        //stringStream <<  posX*elemWidth; //for debugging
                        auto leafData = stringStream.str();
                        viz.replace(vizWidth*(posY+1) + posX*elemWidth, leafData.size(), leafData); 
                     },
                     [&](Node const& node, double posX, size_t level){
                        auto posY = level * elemHeight;
                        std::ostringstream stringStream;
                        stringStream << tree.getData(node);
                        //stringStream <<  posX*elemWidth; //for debugging
                        auto distanceString = stringStream.str();
                        auto pos = posX*elemWidth;
                        viz.replace(vizWidth*posY     + pos, distanceString.size(), distanceString); 
                        viz.at(vizWidth*(posY+1) + pos) = '|';
                     },
                     [&](double posX,  double posOther, size_t level){
                        auto posY = level * elemHeight;
                        auto pos =  static_cast<int>(posX*elemWidth);    
                        auto other =  static_cast<int>(posOther*elemWidth);  
                        if(pos>other){ //to left
                            auto size = pos - other;
                            std::string hLine(size-1,'_');
                            viz.replace(vizWidth*(posY+1) + pos-hLine.size(), hLine.size(), hLine); 
                        }else{
                            auto size = other -pos;
                            std::string hLine(size-1,'_');
                            viz.replace(vizWidth*(posY+1) + pos + 1         , hLine.size(), hLine); 
                        }
                     }

                     );

        for(size_t i = 0; i< vizHeight; ++i){
            auto line = viz.substr(vizWidth*i, vizWidth);
            //remove trailing spaces (not necessary, but it looks better in debug console):
            line.erase(line.find_last_not_of(" ") + 1);
            os<<line<<std::endl;
        }
        return os;
    }


}
#endif