
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef DENDOGRAMDENDOGRAM_H_
#define DENDOGRAMDENDOGRAM_H_ 
#include <vector>
#include "DistanceMatrix.h"
#include <algorithm>
#include <stdexcept>
#include <iterator> //for iterator_traits
#include <queue>


namespace dendogram {

    class Node{
    public:
        Node(
             double distance, 
             size_t childA,
             size_t childB,
             size_t sum):distance_{distance},childA_{childA},childB_{childB},hasLeaf_{false},leaf_{0}, sum_{sum}{}

        Node(size_t leaf): distance_{0},childA_{0},childB_{0},hasLeaf_{true},leaf_{leaf}, sum_{1}{}
        // Node(): distance_{0},childA_{-1},childB_{-1},hasLeaf_{false},leaf_{-1}, sum_{0}{} //this creates an invalid node

        ~Node(){}

        //a bit silly. it should be dealt with const correctness, really 
        // bool isValid()const{return leaf_ != -1;}
        bool isLeaf()const{return hasLeaf_;} 
        size_t getChildA()const{return childA_;}
        size_t getChildB()const{return childB_;}
        size_t getLeaf()const{return leaf_;}
        double getDistance()const{return distance_;}
        size_t getSum()const{return sum_;}

        
    private:
        double distance_; //the distance between the children
        size_t childA_;
        size_t childB_;
        bool hasLeaf_;
        size_t leaf_; //index in a random access container of the element
        size_t sum_; //keeps tract of how many leafs cluster in this node. It is used for calculating weighted distances   
        bool ready_;
    };

    template<class T> class Dendogram{
    public:

        Dendogram();

        template< class TConstIt> 
        Dendogram(TConstIt elementsBegin,TConstIt elementsEnd);
        virtual ~Dendogram(){};
        //Dendogram( Dendogram const&)=delete; //it deletes also the implicit move constructor
        //Dendogram& operator = (Dendogram const&)=delete;

        void pushBack(T const& leaf);

        template< class TConstIt>
        void 
        append(TConstIt elementsBegin,TConstIt elementsEnd);

        template< class DF>
        void 
        calculate(DistanceMatrix::Method collapseType, DF distanceFunction);

        template< class LeafFunc>
        void 
        getClusters(double thresehold, LeafFunc const& leafFunc);

        template< class LeafFunc>
        int 
        cluster(Node const& node,  double thresehold, int clusterCount, LeafFunc const& leafFunc)const;

        //Interface. This allows to implement other algorithms on the Dendogram (depth first or breadth first searches, 
        //drawing and layout routines...) without having to know the details of implmentation (the Node class, etc).
        //General algorithms for other binary trees can also be adapted more easily.
        using node_type = Node;
        using leaf_type = T;
        
        bool ready()const{return ready_;} 
        Node const& getRoot()const{return nodes_[rootIndex_];} 
        Node const& getChildA(Node const& node) const {return nodes_[node.getChildA()];}
        Node const& getChildB(Node const& node) const {return nodes_[node.getChildB()];}
        bool isNodeLeaf(Node const& node)const{return node.isLeaf();}
        // double getData(Node const& node)const{return node.getDistance();} //a bit of a mistmatch between required generality and specificity
        //This returns the distance normalised by the furthest distance (the one of root) * 100
        double getData(Node const& node)const{return ready()? 100 * node.getDistance()/getRoot().getDistance() : 0;}
        T const& getLeaf(Node const& node) const{return leaves_[node.getLeaf()];}



    private:
        size_t rootIndex_;
        std::vector<Node> nodes_;// indexes used for the relation with the distance matrix
        std::vector<T> leaves_;
        bool ready_;
            //DistanceMatrix distanceMatrix_;
    };

    template< class T>
    Dendogram<T>::Dendogram():ready_{false}{}

    //Copies the elements iterated through. If this is not desired pointers should be used.
    //Both constructor version and append version.
    //A more flexible vesion could allow to build a dendogram on an existing random access sequential
    //container. For example, it could have a function that, given a container, it returns a dendogram for it.
    template< class T>
    template< class TConstIt>
    Dendogram<T>::Dendogram(TConstIt elementsBegin,TConstIt elementsEnd):ready_{false}{
        std::copy(elementsBegin,elementsEnd, std::back_inserter(leaves_)); 
    }
    template< class T>
    template< class TConstIt>
    void 
    Dendogram<T>::append(TConstIt elementsBegin,TConstIt elementsEnd){
        std::copy(elementsBegin,elementsEnd, std::back_inserter(leaves_));
    }

    template< class T>
    void 
    Dendogram<T>::pushBack(T const& leaf){
        leaves_.pushBack(leaf);
    }

    template< class T>
    template< class DF>
    void 
    Dendogram<T>::calculate(DistanceMatrix::Method collapseType, DF distanceFunction){

       rootIndex_ = -1;
       //nodes_.clear();
       // leaves_.clear();

       if (leaves_.size() == 0){
        throw (std::runtime_error("Trying to calculate empty dendogram."));
        }

        //Initialise nodes_:
        for (size_t i = 0; i < leaves_.size(); i++){
            nodes_.push_back({i}); //the index of the leaf
        }

        DistanceMatrix distanceMatrix{nodes_.size()};

        for (size_t i = 0; i < nodes_.size(); i++){
            auto const& elA=leaves_[i];
            for (size_t j = 0; j < i; j++){
                auto const& elB=leaves_[j];
                distanceMatrix.setDistance(i,j,distanceFunction(elA,elB));
            }
        }

        //The orphan nodes, to establish correspondence between the ordering in the distance matrix
        //and the one in nodes_
        std::vector<size_t>orphanNodesIndices(nodes_.size(),0);
        for (size_t i = 0; i < nodes_.size(); i++){
            orphanNodesIndices[i]=i; //the indices of the corresponding nodes
        }

        struct CollapseEvent{
            double distance;
            size_t orphanA, orphanB; //index in orphanNodesIndices
            int versionA,versionB; 
        };

        auto compare = [](CollapseEvent const& eA,CollapseEvent const& eB){
            return eA.distance > eB.distance; 
        };

        std::priority_queue<CollapseEvent,
        std::vector<CollapseEvent>,
        decltype(compare)> collapseEvents(compare);


        for (size_t i = 0; i < distanceMatrix.size(); ++i){
            for (size_t j = 0; j < i; ++j){
                auto dis = distanceMatrix.getDistance(i, j);
                collapseEvents.push({dis,i,j,0,0}); 
            }
        }

        std::vector<int> versions(distanceMatrix.size(),0);


        while(orphanNodesIndices.size()>1){

            auto event = collapseEvents.top();
            collapseEvents.pop();
            
            //for the event to still be current, the nodes involved need not to have been affected by any change
            //
            //if they correspond to deleted orphan nodes 
            if(event.orphanA >= orphanNodesIndices.size() || event.orphanB >= orphanNodesIndices.size()){
                continue;
            }
            if (event.versionA != versions[event.orphanA]  || event.versionB != versions[event.orphanB]){
            //the event is outdated and does not correspond to actual state of nodes
                continue;
            }

            auto indexOfParentNode=nodes_.size(); //it will be added to the end
            //get the indices of the nodes for orphan A and B
            auto nA = orphanNodesIndices[event.orphanA];
            auto nB = orphanNodesIndices[event.orphanB];
            
            //This sets and ordering on the nodes depending on distance.
            //It can be useful in drawing or visualising the tree, but has no effect on 
            //the algorithm! 
            if(nodes_[nA].getDistance()<nodes_[nB].getDistance()){ //Swap
                size_t t = nA;
                nA = nB;
                nB = t;
            }

            nodes_.push_back({event.distance,nA,nB,nodes_[nA].getSum() + nodes_[nB].getSum()});


            // Update distance matrix.
            distanceMatrix.collapseDistancesToFirst(event.orphanA, event.orphanB, 
                                                    collapseType,
                                                    nodes_[nA].getSum(), 
                                                    nodes_[nB].getSum());


            distanceMatrix.moveLastRowTo(event.orphanB);

            //and the corresponding orphanNodesIndices
            orphanNodesIndices[event.orphanA] = indexOfParentNode; //the new node
            orphanNodesIndices[event.orphanB] = orphanNodesIndices.back();// the last one, which has been moved.

            //now reduce the size of orphanNodesIndices to match the one of distanceMatrix

            //Update versions for the changes
            versions[event.orphanA] ++;
            versions[event.orphanB] ++; //we only use ot for storage, but...

            //update the events

            //update events of A, if it has been moved (if it was the last orphan node),
            //it will have been copied to B, so no need to calculate (it would through 
            //an out of range error in DistanceMatrix).
            if(event.orphanA < distanceMatrix.size() ){
                for (size_t i = 0; i < distanceMatrix.size(); ++i){
                    if(event.orphanA!=i){ //don't add 0 distances to queue 
                        auto dis = distanceMatrix.getDistance(event.orphanA, i);
                        collapseEvents.push({dis,event.orphanA,i,versions[event.orphanA],versions[i]});
                    }
                }    
            }

            //update events of B (since we have copied last orphan node here).
            //If it has been deleted (if it was the last orphan node) it is
            //obviously not necessary. It will through an out of range error 
            //in DistanceMatrix anyway, as there is no corresponding row.
            if(event.orphanB < distanceMatrix.size() ){
                for (size_t i = 0; i < distanceMatrix.size(); ++i){
                    if(event.orphanB!=i){ //don't add 0 distances to queue 
                        auto dis = distanceMatrix.getDistance(event.orphanB, i);
                        collapseEvents.push({dis,event.orphanB,i,versions[event.orphanB],versions[i]});
                    }
                }    
            }
            
            orphanNodesIndices.pop_back();
            versions.pop_back(); //not really necessary, but just to be consistent

            //only for debugging
            if(orphanNodesIndices.size() != distanceMatrix.size()){
                throw std::runtime_error("Error updating lenght of matrix and orphanNodesIndices.");
            }

        }

        rootIndex_ = orphanNodesIndices[0];
        ready_ = true;

    }

    //Recursive  depth first search of leaves and calculation of clusterCount
    //In each call to LeafF, the clusterCount will be either the same as the 
    //previous call or it may have increased, if the next leaf to consider is in
    //a different cluster.
    template< class T>
    template< class LeafFunc>
    int 
    Dendogram<T>::cluster(Node const& node,  double thresehold, int clusterCount, LeafFunc const& leafFunc)const{

        if(!node.isLeaf()){  
            auto const& childA = nodes_[node.getChildA()];
            clusterCount = cluster(childA,thresehold, clusterCount,leafFunc);
            //the branching to childA defined a cluster, so increase clusterCount if it is 
            //there is a transition through the threshold defining the clustering limit.
            if(node.getDistance() > thresehold &&   childA.getDistance()<= thresehold){
               ++clusterCount;
            }

            auto const& childB = nodes_[node.getChildB()];
            //the branching to childB defined a cluster, so increase clusterCount if it is 
            //there is a transition through the threshold defining the clustering limit.
            clusterCount = cluster(childB,thresehold, clusterCount,leafFunc);  
            if(node.getDistance() > thresehold &&   childB.getDistance()<= thresehold){
               ++clusterCount;
            }    
        }
        else{//call leaf function  with its cluster count, so it for example can process it
            leafFunc(leaves_[node.getLeaf()], clusterCount); //leaves will always be in a cluster, even if they are the single element        
        } 
        return clusterCount;
    }


    //First call to recursive function from root and with clusterCount 0.
    template< class T>
    template< class LeafFunc>
    void 
    Dendogram<T>::getClusters(double thresehold, LeafFunc const& leafFunc){
        cluster(nodes_[rootIndex_],thresehold,0,leafFunc);
    }


    template< class TConstIt>
    Dendogram<typename std::iterator_traits<TConstIt>::value_type> create(TConstIt begin, TConstIt end){
        return Dendogram<typename std::iterator_traits<TConstIt>::value_type>(begin, end);
    }


}
#endif