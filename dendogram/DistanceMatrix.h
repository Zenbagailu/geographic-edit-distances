
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef DENDOGRAM_DISTANCEMATRIX_H_
#define DENDOGRAM_DISTANCEMATRIX_H_ 
#include <vector>
namespace dendogram {
    class DistanceMatrix
    {
    public:
        enum Method{ 
            AVERAGE, //averaging the two distances. This does not consider how big the clusters are...
            WEIGHTED_AVERAGE, //the right way of averaging, using centroids
            SHORTEST, //take always the shortests
            LONGEST }; //or the longest

        DistanceMatrix(size_t size);
        virtual ~DistanceMatrix(){};
        DistanceMatrix( DistanceMatrix const&)=delete;
        DistanceMatrix& operator = (DistanceMatrix const&)=delete;


        void setDistance(size_t a, size_t b, double dis);
        double getDistance(size_t a, size_t b);

        //only used for debugging
        size_t size(){return size_;};
             
        void collapseDistancesToFirst(size_t a, size_t b, 
                               Method method, 
                               size_t weighta, size_t weightb);

        void moveLastRowTo(size_t to);


    private:
        //void copyRow(size_t to, size_t from);
  
        // The relations of distances between pairs of elements make a symmetric
        // matrix. For brevity, these can be represented in a lower triangular matrix
        // any distance between two elements with index i and j will then be returned
        // by checking this matrix. Since the matrix is represented with a one 
        // dimensional vector,the triangular number is used to calculate the equivalent 
        // one dimensional index.
        
        //this is the result of applying the formula of triangular number: 
        //https://en.wikipedia.org/wiki/Triangular_number
        //to calculate the elements of the lower triangular matrix:
        //n(n+1)/2
        
        inline size_t index(size_t i,size_t j){
            if(j>i) std::swap(i,j);
            return j + (i*(i+1))/2; 
        }

    private:
        size_t size_; //size of the matrix.
        //a lower triangular matrix
        std::vector<double> matrix_; //the amount of elements will be size_*(size_+1)/2
    
    };
}
#endif