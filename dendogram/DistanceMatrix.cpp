
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "DistanceMatrix.h"

namespace dendogram {

    DistanceMatrix::DistanceMatrix(size_t size):
    size_{size},
    //applying triangular numer https://en.wikipedia.org/wiki/Triangular_number
    //it actually needs to be calculated with size + 1, since the triangular number
    //considers the number of elements in each row, and row 0 will actually have 1 
    //element, and row 9, for example, 10.
    matrix_((size*(size+1))/2){ }

    void DistanceMatrix::setDistance(size_t a, size_t b, double dis){
        if(a >= size_ || b >= size_){
            throw std::out_of_range("out of range pair in DistanceMatrix::setDistance");
        }

        matrix_[index(a,b)] = dis;
    }

    double DistanceMatrix::getDistance(size_t a, size_t b){
        if(a >= size_ || b >= size_){
            throw std::out_of_range("out of range pair in DistanceMatrix::getDistance");
        }
        return matrix_[index(a,b)];
    }


    void DistanceMatrix::collapseDistancesToFirst(
                                           size_t a, 
                                           size_t b, 
                                           Method method,
                                           size_t weighta, 
                                           size_t weightb){
        if(a >= size_ || b >= size_){
            throw std::out_of_range("out of range pair in DistanceMatrix::collapseDistancesToFirst");
        }

        for(size_t i=0; i< size_; i++){
            double result;
            double va= matrix_[index(a,i)];
            double vb= matrix_[index(b,i)];
            switch(method)
            {
            case AVERAGE:           result = (va + vb) * 0.5f;
            break;
            case WEIGHTED_AVERAGE:  result = (va*weighta + vb*weightb) / (weighta+weightb);
            break;
            case LONGEST:           result = va > vb ? va : vb;
            break;
            case SHORTEST:          result = va < vb ? va : vb;
            break;
            }
            matrix_[index(a,i)]=result;
        }
    }

    void DistanceMatrix::moveLastRowTo(size_t to){
        if(to >= size_){
            throw std::out_of_range("out of range index in DistanceMatrix::moveLastRowTo");
        }

        for(size_t i=0; i< size_; i++){
            matrix_[index(to,i)] = matrix_[index(size_-1,i)];
        }
        --size_;

    }

}