
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#include "OsmData.h"
#include <osm/OsmClient.h>
//#include <boost/property_tree/info_parser.hpp> //only for debug


namespace test {
    double NodeData::operator[](IdType indx)const{

        switch(indx){
            case 0:
            return x_;
            case 1:
            return y_;
            case 2:
            return 0;
            default:
            throw std::invalid_argument("the index "+ std::to_string(indx) + " is invalid.");
        }
    }

    AttributeContainer getAttributeContainerFromTags(PropertyTree  const& tree){
        AttributeContainer attributes;
        auto tags = tree.get_child_optional("tags");
       
        if(tags){
            for (boost::property_tree::ptree::value_type const& tag : *tags ){
                auto& tagName= tag.first;
                auto& tagValue= tag.second.data();
                attributes.push_back({tagName,tagValue});
            }
        }
        else{

        }

        return attributes;
    }


    boost::property_tree::ptree getPtreeFromQuery(std::string  const& query){
        osm::OsmClient osmClient;
        auto result= osmClient.processQuery(query); 

        //boost::property_tree used to parse the JSON file:
        std::stringstream resultStream(result); // put string into stream, which it is std::basic_istream
        boost::property_tree::ptree tree;
        boost::property_tree::read_json(resultStream, tree);

        //to debug the tree, print as INFO, just to test:    
        // std::ostringstream oss;
        // boost::property_tree::info_parser::write_info(oss, tree);
        // std::cout<<oss.str()<<std::endl;
        return tree;

    }


}