
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef OSMDRAWINGTYPES
#define OSMDRAWINGTYPES 
#include <string>
#include <map>
#include <array>
#include <utility> //for std::pair
namespace test {

    struct AttributeFilter{
        std::string queryType;
        std::map<std::string,std::string> tags;  
    };

    struct DrawingSettings{
        bool fill;            //It makes no sense for points, as they are drawn now. 
        bool drawText;
        double thickness;     //Both for lines and points. 
        std::array<double, 4> color;
    };

    //Ideally it could be a map, but it would need to test if exisiting attributes match all the requirements in the filter
    //(but may also have extra attributes that don't match)
    using AttributeDrawingSettings = std::vector<std::pair<AttributeFilter,DrawingSettings>> ;


}
#endif