
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/


#ifndef glfwMOUSE3DNAVIGATION_H_
#define glfwMOUSE3DNAVIGATION_H_ 

#include "MouseInfo.h"


namespace ui {
template<class V> class mouse3DNavigation{
public:
    typedef V View_t;
    mouse3DNavigation(View_t& view);
   // mouse3DNavigation( mouse3DNavigation const&)=delete;
    mouse3DNavigation& operator = (mouse3DNavigation const&)=delete;

    void mouseButtonFunction(ui::MouseInfo const& info);
    void mouseMotionFunction(ui::MouseInfo const& info);
    void scrollFunction(int xOffset, int yOffset);

private:
    ui::MouseInfo lastMouseEvent;
    View_t& viewRef; //it can be tested if it is null?
};



template<class V> 
mouse3DNavigation<V>::mouse3DNavigation(mouse3DNavigation<V>::View_t& view):viewRef(view){
}
template<class V>
void 
mouse3DNavigation<V>::mouseButtonFunction(ui::MouseInfo const& info){
    if(info.action == ui::MouseActionType::PRESSED){
        lastMouseEvent=info;
    }else if(info.action == ui::MouseActionType::RELEASED){
        lastMouseEvent=ui::MouseInfo(); //reset all stored values
    }
}

template<class V>
void
mouse3DNavigation<V>::mouseMotionFunction(ui::MouseInfo const& info){
   
    if(lastMouseEvent.button==ui::MouseButtonType::BUTTON01){

        viewRef.rotate({info.x-lastMouseEvent.x,info.y-lastMouseEvent.y });
        lastMouseEvent=info;
        lastMouseEvent.button= ui::MouseButtonType::BUTTON01;
    }  

    if(lastMouseEvent.button==ui::MouseButtonType::BUTTON02){

        viewRef.pan({lastMouseEvent.x,lastMouseEvent.y},{info.x,info.y});
        lastMouseEvent=info;
        lastMouseEvent.button= ui::MouseButtonType::BUTTON02;
    }  
}

template<class V>
void
mouse3DNavigation<V>::scrollFunction(int xOffset, int yOffset){
    viewRef.zoom(yOffset);
}  


template<class V>
mouse3DNavigation<V>
create3DNavigation(V& view){
    //return mouse3DNavigation<V>(view);
    return{view};
}  
}
#endif
