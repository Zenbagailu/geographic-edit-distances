
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef UIMOUSEINFO_H_
#define UIMOUSEINFO_H_ 

#include "common.h"
namespace ui {


	enum class MouseButtonType{BUTTON01,BUTTON02,BUTTON03,NONE,UNKNOWN};
	enum class MouseActionType{RELEASED,PRESSED,MOVED,NONE};

	struct MouseInfo
	{	
		MouseInfo():x{0},y{0},button{MouseButtonType::NONE},action{MouseActionType::NONE},modifier{ModifierType::NONE}{}
		MouseInfo(int _x,int _y, MouseButtonType _button,MouseActionType action_, ModifierType modifier_=ModifierType::NONE):
		x{_x},y{_y},
		button{_button},action{action_},modifier{modifier_}{};

		int x,y;
		MouseButtonType button;
		MouseActionType action;
		ModifierType modifier;
	};
}
#endif