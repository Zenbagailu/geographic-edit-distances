
/*
================================================================================
Copyright (c) 2020 Pablo Miranda Carranza

This file is part of Geographic Edit Distances.
Geographic Edit Distances is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

Geographic Edit Distances is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Geographic Edit Distances.  If not, see <https://www.gnu.org/licenses/>.

================================================================================
*/

#ifndef UICOMMON_H_
#define UICOMMON_H_ 
namespace ui {
	enum ModifierType{NONE=0x00,SHIFT=0x01, CONTROL=0x02, ALT=0x04, SUPER=0x08};
	constexpr int operator|( ModifierType mm1,  ModifierType mm2) { return  ModifierType(int(mm1)|int(mm2)); }
}
#endif
